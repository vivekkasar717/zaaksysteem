#! /bin/bash
# This script takes the mini BAG dump used for testing, and loads it into
# an actual Elasticsearch instance.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

paste -d'\n' \
    <( jq -M -c '{ "create": {"_id": .woonplaats.openbareruimte.nummeraanduiding.id, "_type":"_doc", "_index":"bag_nummeraanduiding" } }' < ${DIR}/share/bag/nummeraanduiding.txt  ) \
    ${DIR}/share/bag/nummeraanduiding.txt \
    | curl \
        -XPOST \
        -H "Content-Type: application/json" \
        --data-binary "@-" \
        "${ES_HOST_PORT:-localhost:9200}/_bulk?pretty=true"

paste -d'\n' \
    <( jq -M -c '{ "create": {"_id": .woonplaats.openbareruimte.id, "_type":"_doc", "_index":"bag_openbareruimte" } }' < ${DIR}/share/bag/openbareruimte.txt  ) \
    ${DIR}/share/bag/openbareruimte.txt \
    | curl \
        -XPOST \
        -H "Content-Type: application/json" \
        --data-binary "@-" \
        "${ES_HOST_PORT:-localhost:9200}/_bulk?pretty=true"
