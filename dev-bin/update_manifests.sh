#!/bin/bash

DIR=$(readlink -m $(dirname $0)/../);
rm -f MANIFEST MANIFEST.bak ; /usr/bin/perl "-MExtUtils::Manifest=mkmanifest" -e mkmanifest
egrep "\.(pod|pm)" MANIFEST | egrep -v '^(drafts|MANIFEST.pod)' > MANIFEST.pod
