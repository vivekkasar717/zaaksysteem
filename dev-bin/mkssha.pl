#! /usr/bin/env perl

use strict;
use warnings;
use v5.10;

use Digest::SHA1;
use File::Which;
use MIME::Base64;
use String::Random qw/random_string/;

sub generate_password {
    my $length = $_[0] // 20;
    
    if (which("pwgen")) {
        chomp(my $password = `pwgen -1 -n $length`);

        return $password;
    }

    say STDERR '`pwgen` not found, falling back to more crude methods';

    return random_string('.' x $length)
}

#
# This is what I'd put in sub MAIN... If there was one!
#

my $ctx = Digest::SHA1->new;
my $plaintext = $ARGV[0] // generate_password;
my $salt = random_string('.' x 4);

$ctx->add($plaintext);
$ctx->add($salt);

my $hashed = '{SSHA}' . encode_base64($ctx->digest . $salt, '');

say "$plaintext\t$hashed";

__END__

=head1 NAME

mkssha - Make a Secure SHA hashed password
    
=head2 SYNOPSIS

mkssha [password]

=over
    
=item B<password> - The password to hash. If left out, one will be randomly generated

=back
    
=head1 CONTRIBUTORS
    
=over
    
=item Patrick Spek <patrick@mintlab.nl>

=back

=head1 COPYRIGHT
    
Copyright (c) 2018, Mintlab B.V. and all the persons listen in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

=head1 LICENSE
    
This program is distributed under the terms of the L<EUPL license|Zaaksysteem::LICENSE>.
