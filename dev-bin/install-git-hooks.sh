#!/bin/bash

REPO_PATH="$(cd "$(dirname "$0")/.."; pwd -P)"

if [ ! -e "${REPO_PATH}/dev-bin/git-hooks/post-checkout" ]; then
    exit 0
fi

cp -v ${REPO_PATH}/dev-bin/git-hooks/* ${REPO_PATH}/.git/hooks
