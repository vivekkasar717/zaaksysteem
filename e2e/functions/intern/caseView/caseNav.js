export const openTab = tab => {
    $(`.case-navigation-list [data-name="${tab}"]`).click();
};

export const openPhaseActions = () => {
    $('.phase-sidebar .phase-sidebar-header-inner li:nth-child(1)').click();
};

export const openChecklist = () => {
    $('.phase-sidebar .phase-sidebar-header-inner li:nth-child(2)').click();
};

export const openPhase = phase => {
    $(`zs-case-phase-nav .phase-list-container div:nth-child(${phase})`).click();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
