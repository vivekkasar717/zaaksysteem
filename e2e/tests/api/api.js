import { performApiCall } from './../../functions/common/auth/xhrRequests';

const getSpecificCase = (res, caseNumber) =>
    res.result.instance.rows
        .find(row =>
            row.instance.number == caseNumber
        );

const isAccessAllowed = (res, expected) => {
    expect(res !== 'Authorization required.').toEqual(expected);
};

const confirmExactCases = (res, expected) => {
    res.result.instance.rows.forEach((row, key) => {
        expect(row.instance.number).toEqual(expected[key]);
    });
};

const checkCaseAttributes = (res, expected) => {
    Object.keys(expected).forEach(caseNumber => {
        const caseExpectations = expected[caseNumber];
        const caseInfo = getSpecificCase(res, caseNumber);

        Object.keys(caseExpectations).forEach(caseExpectation => {
            expect(caseExpectations[caseExpectation]).toEqual(caseInfo.instance.attributes[caseExpectation]);
        });
    });
};

// case 178: casetype 1, api_keuze  'ja', not-destroyed
// case 179: casetype 1, api_keuze 'nee', not-destroyed
// case 180: casetype 2, api_keuze  'ja', not-destroyed
// case 181: casetype 2, api_keuze 'nee', not-destroyed
// case 182: casetype 1, api_keuze  'ja', destroyed
// case 183: casetype 1, api_keuze 'nee', destroyed
// case 184: casetype 2, api_keuze  'ja', destroyed
// case 185: casetype 2, api_keuze 'nee', destroyed

// API-user is authenticated to 'casetype 1'
// Saved search 'API' filters cases by api_keuze == 'ja'
// API 17 is limited to the saved search 'API'
// API 18 is not limited by saved searches

const scenarios = [
    // confirms that API can not be accessed without proper key
    {
        call: 'cases',
        id: 17,
        key: 'wrong',
        results: [
            {
                checker: isAccessAllowed,
                expected: false
            }
        ]
    },
    // confirms that cases can only be found when
    // - user is authenticated to the case
    // - case is not destroyed
    // - case is in the saved search
    {
        call: 'cases',
        id: 17,
        key: 'met',
        results: [
            {
                checker: confirmExactCases,
                expected: [178]
            },
            {
                checker: checkCaseAttributes,
                expected: {
                    178: {
                        api_keuze: ['ja'],
                        omschrijving: ['Zaak met rechten met keuze ja']
                    }
                }
            }
        ]
    },
    // confirms that cases can only be found when
    // - user is authenticated to the case
    // - case is not destroyed
    // and are not limited by any saved search
    {
        call: 'cases',
        id: 18,
        key: 'zonder',
        results: [
            {
                checker: confirmExactCases,
                expected: [178, 179]
            },
            {
                checker: checkCaseAttributes,
                expected: {
                    178: {
                        api_keuze: ['ja'],
                        omschrijving: ['Zaak met rechten met keuze ja']
                    },
                    179: {
                        api_keuze: ['nee'],
                        omschrijving: ['Zaak met rechten met keuze nee']
                    }
                }
            }
        ]
    }
];

scenarios.map(scenario => {
    const { call, id, key, results } = scenario;

    describe(`when calling '${call}' on API ${id} with the key '${key}'`, () => {
        let apiResult;

        beforeAll(() => {
            apiResult = performApiCall(call, id, key);
        });

        results.map(result => {
            const { checker, expected } = result;

            it(`it should have the correct results for ${checker.name}`, () => {
                apiResult.then(res => {
                    checker(res, expected);
                });
            });
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
