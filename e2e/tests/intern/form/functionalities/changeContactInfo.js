import {
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    getRequestorEmailAddress,
    getRequestorPhoneNumber,
    getRequestorMobileNumber,
    changeRequestorEmailAddress,
    changeRequestorPhoneNumber,
    changeRequestorMobileNumber
} from './../../../../functions/common/form';
import waitForElement from './../../../../functions/common/waitForElement';
import getContactUrl from './../../../../functions/common/getValue/getContactUrl';
import {
    openContactInfo,
    getEmailAddress,
    getPhoneNumber,
    getMobileNumber
} from './../../../../functions/intern/contactView';

const contacts = [
    {
        type: 'add',
        requestorType: 'citizen',
        requestorId: '15',
        oldEmail: '',
        oldPhoneNumber: '',
        oldMobileNumber: '',
        newEmail: 'changed@info.com',
        newPhoneNumber: '0612345678',
        newMobileNumber: '0687654321'
    },
    {
        type: 'chang',
        requestorType: 'citizen',
        requestorId: '16',
        oldEmail: 'unchanged@email.com',
        oldPhoneNumber: '0611111111',
        oldMobileNumber: '0622222222',
        newEmail: 'changed@info.com',
        newPhoneNumber: '0612345678',
        newMobileNumber: '0687654321'
    },
    {
        type: 'delet',
        requestorType: 'citizen',
        requestorId: '17',
        oldEmail: 'unremoved@email.com',
        oldPhoneNumber: '0611111111',
        oldMobileNumber: '0622222222',
        newEmail: '',
        newPhoneNumber: '',
        newMobileNumber: ''
    },
    {
        type: 'add',
        requestorType: 'organisation',
        requestorId: '8',
        oldEmail: '',
        oldPhoneNumber: '',
        oldMobileNumber: '',
        newEmail: 'changed@info.com',
        newPhoneNumber: '0612345678',
        newMobileNumber: '0687654321'
    },
    {
        type: 'chang',
        requestorType: 'organisation',
        requestorId: '9',
        oldEmail: 'unchanged@email.com',
        oldPhoneNumber: '0611111111',
        oldMobileNumber: '0622222222',
        newEmail: 'changed@info.com',
        newPhoneNumber: '0612345678',
        newMobileNumber: '0687654321'
    },
    {
        type: 'delet',
        requestorType: 'organisation',
        requestorId: '10',
        oldEmail: 'unremoved@email.com',
        oldPhoneNumber: '0611111111',
        oldMobileNumber: '0622222222',
        newEmail: '',
        newPhoneNumber: '',
        newMobileNumber: ''
    }
];

contacts.forEach(contact => {
    const { type, requestorType, requestorId, oldEmail, oldPhoneNumber, oldMobileNumber, newEmail, newPhoneNumber, newMobileNumber } = contact;

    describe(`when starting a registration form for ${type}ing the contact info of a ${requestorType}`, () => {
        beforeAll(() => {
            const data = {
                casetype: 'Contactgegevens aanpassen bij registratie',
                requestorType: requestorType,
                requestorId: requestorId,
                channelOfContact: 'behandelaar'
            };

            openPageAs();
            startForm(data);
            goNext();
        });

        it('the email address should be correct', () => {
            expect(getRequestorEmailAddress()).toEqual(oldEmail);
        });

        it('the phone number should be correct', () => {
            expect(getRequestorPhoneNumber()).toEqual(oldPhoneNumber);
        });

        it('the mobile number should be correct', () => {
            expect(getRequestorMobileNumber()).toEqual(oldMobileNumber);
        });

        describe(`and when ${type}ing the info and registering the case`, () => {
            beforeAll(() => {
        
                changeRequestorEmailAddress(newEmail);
                changeRequestorPhoneNumber(newPhoneNumber);
                changeRequestorMobileNumber(newMobileNumber);

                goNext();
                waitForElement('.case-view');
                browser.sleep(5000);
                browser.ignoreSynchronization = true;
                browser.get(getContactUrl(requestorType, requestorId));
                waitForElement('[id="ui-id-12"]');
                openContactInfo();
                waitForElement('[name="npc-email"]');
            });
        
            it(`the email address should have been ${type}ed`, () => {
                expect(getEmailAddress()).toEqual(newEmail);
            });

            it(`the phone number should have been ${type}ed`, () => {
                expect(getPhoneNumber()).toEqual(newPhoneNumber);
            });

            it(`the mobile number should have been ${type}ed`, () => {
                expect(getMobileNumber()).toEqual(newMobileNumber);
            });

            afterAll(() => {
                browser.get('/intern/');
                browser.ignoreSynchronization = false;
            });
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
