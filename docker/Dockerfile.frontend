#
# Welcome to the Dockerfile.frontend, were we use layers liberaly to
# speed up build times. You will probably find some anti-patterns, but
# they work for us in a spiffy kind of way.
#
# This is how the Dockerfile is setup. And why!
#
# frontend-shared) Build dependencies for shared code so we don't need
# to copy over individual node_modules and/or the complete node_modules
# directory from client. This should be a cache only action, changing
# deps will be unlikely.
#
# client- and frontend-package) Build the dependencies of both the
# client and the frontend code. Should hit the cache more often than
# not.
#
# client-build) Build the client src code. This requires a huge amount
# of layers due to the intertwinedness of frontend code into client
# code. But this makes sure that most things come from the cache and are
# speedy. We consume some layers as a trade-off.
#
# frontend-build) Build the frontend src code. Again, a huge amount of layers, we
# test the boundries of the max amount of docker layers with this thing.
#
# production) Build the production deployment, this is the trivial bit.
#
# development) Build a development layer for perl developers. You don't
# get to have all the things of the production layer plus a self-signed
# ssl certificate
#
# npm-development) Build a development layer for js developers, you get
# to have all the build artifacts, npm and more.
#

# This is a copy of what is needed for a frontend build without client deps
FROM node:boron as frontend-shared
WORKDIR /tmp/shared
COPY shared-fe/package.json .
RUN npm set progress=false \
    && npm install --production --no-optional

# Install all the client/package.json deps
FROM node:boron as client-package
WORKDIR /tmp/client
COPY client/package.json .
RUN npm set progress=false \
    && npm install --production --no-optional

# Install all the frontend/package.json deps
FROM node:boron as frontend-package
WORKDIR /tmp/frontend
COPY frontend/package.json /tmp/frontend/
COPY --from=client-package /tmp/client /tmp/client
RUN  npm set progress=false \
    && npm install --production --no-optional --silent

# Actually install the client src code
FROM node:boron as client-build
RUN mkdir -p /opt/zaaksysteem/frontend/zaaksysteem/src/js /opt/zaaksysteem/root
WORKDIR /opt/zaaksysteem/client

# The following adds layers and layers, but increases the build speed.
# A change in frontend code DOES not trigger a rebuild of client \o/
# Feel free to rewrite the client code to include these files
#
COPY frontend/zaaksysteem/src/css/partials/_angular-autocomplete.scss ../frontend/zaaksysteem/src/css/partials/_angular-autocomplete.scss
COPY frontend/zaaksysteem/src/js/_namespace.js ../frontend/zaaksysteem/src/js/_namespace.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/core/data ../frontend/zaaksysteem/src/js/nl/mintlab/core/data
COPY frontend/zaaksysteem/src/js/nl/mintlab/core/form/_form.js ../frontend/zaaksysteem/src/js/nl/mintlab/core/form/_form.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/core/form/formValidatorService.js ../frontend/zaaksysteem/src/js/nl/mintlab/core/form/formValidatorService.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/core/user ../frontend/zaaksysteem/src/js/nl/mintlab/core/user
COPY frontend/zaaksysteem/src/js/nl/mintlab/dom ../frontend/zaaksysteem/src/js/nl/mintlab/dom
COPY frontend/zaaksysteem/src/js/nl/mintlab/events ../frontend/zaaksysteem/src/js/nl/mintlab/events
COPY frontend/zaaksysteem/src/js/nl/mintlab/locale ../frontend/zaaksysteem/src/js/nl/mintlab/locale
COPY frontend/zaaksysteem/src/js/nl/mintlab/message ../frontend/zaaksysteem/src/js/nl/mintlab/message
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/_directives.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/_directives.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngDroppable.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngDroppable.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngEditable.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngEditable.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngUpload.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngUpload.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsAutogrow.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsAutogrow.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsConfirm.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsConfirm.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsContextmenu.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsContextmenu.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDatePickerModel.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDatePickerModel.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDateRangePicker.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDateRangePicker.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDropdownMenu.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsDropdownMenu.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsHoverMenu.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsHoverMenu.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsInfiniteScroll.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsInfiniteScroll.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsModal.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsModal.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsModelTransformers.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsModelTransformers.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsPlaceholder.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsPlaceholder.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsSelect.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsSelect.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsSpotEnlighter.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsSpotEnlighter.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsStyle.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsStyle.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsTemplate.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsTemplate.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsTitle.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsTitle.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/contains.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/contains.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/fromGlobalToLocal.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/fromGlobalToLocal.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/fromLocalToGlobal.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/fromLocalToGlobal.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getDocumentPosition.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getDocumentPosition.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getMousePosition.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getMousePosition.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getViewportPosition.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getViewportPosition.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getViewportSize.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getViewportSize.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getWindowHeight.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/getWindowHeight.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/intersects.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/intersects.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/setMouseEnabled.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/dom/setMouseEnabled.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/events/addEventListener.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/events/addEventListener.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/events/cancelEvent.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/events/cancelEvent.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/events/removeEventListener.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/events/removeEventListener.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/_filters.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/_filters.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/capitalize.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/capitalize.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/generateUid.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/generateUid.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/generateUuid.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/generateUuid.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/object/clone.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/object/clone.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/object/inherit.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/object/inherit.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/safeApply.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/safeApply.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/shims/indexOf.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/shims/indexOf.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/shims/trim.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/shims/trim.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/net ../frontend/zaaksysteem/src/js/nl/mintlab/net
COPY frontend/zaaksysteem/src/js/nl/mintlab/timeline ../frontend/zaaksysteem/src/js/nl/mintlab/timeline
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngDraggable.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/ngDraggable.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsPopup.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/directives/zsPopup.js
COPY frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/size.js ../frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/size.js
COPY frontend/zaaksysteem/src/html/timeline/timeline.swig ../frontend/zaaksysteem/src/html/timeline/timeline.swig
COPY frontend/zaaksysteem/src/js/nl/mintlab/docs ../frontend/zaaksysteem/src/js/nl/mintlab/docs
COPY frontend/zaaksysteem/src/html/docs/docs.swig ../frontend/zaaksysteem/src/html/docs/docs.swig

# End the massive import of frontend code

COPY --from=client-package /tmp/client/ .
COPY root/ ../root
COPY client/ .
RUN npm run build-apps --silent

# Build the frontend src code
FROM node:boron as frontend-build
RUN mkdir -p /opt/zaaksysteem/frontend /opt/zaaksysteem/client/src /opt/zaaksysteem/root
WORKDIR /opt/zaaksysteem/frontend
COPY --from=frontend-shared /tmp/shared/node_modules ../client/node_modules

# The following adds layers and layers, but increases the build speed.
# A change in client code DOES not trigger a rebuild of frontend \o/
# Feel free to rewrite the frontend code to include these files
#
COPY client/src/intern/views/case/zsCaseDocumentView/_webodf.scss ../client/src/intern/views/case/zsCaseDocumentView/_webodf.scss
COPY client/src/intern/views/root/createContactMoment ../client/src/intern/views/root/createContactMoment
COPY client/src/shared/case/caseStatusLabelFilter ../client/src/shared/case/caseStatusLabelFilter
COPY client/src/shared/case/zsCaseStatusIcon ../client/src/shared/case/zsCaseStatusIcon
COPY client/src/shared/navigation/zsBackdropService ../client/src/shared/navigation/zsBackdropService
COPY client/src/shared/object/mock ../client/src/shared/object/mock
COPY client/src/shared/ui/uiViewTransition ../client/src/shared/ui/uiViewTransition
COPY client/src/shared/ui/zsClickOutside ../client/src/shared/ui/zsClickOutside
COPY client/src/shared/ui/zsIcon ../client/src/shared/ui/zsIcon
COPY client/src/shared/ui/zsNotificationCounter ../client/src/shared/ui/zsNotificationCounter
COPY client/src/shared/ui/zsSpinner ../client/src/shared/ui/zsSpinner
COPY client/src/shared/user/sessionService ../client/src/shared/user/sessionService
COPY client/src/shared/user/userSettings ../client/src/shared/user/userSettings
COPY client/src/shared/util/appUnload ../client/src/shared/util/appUnload
COPY client/src/shared/util/capabilities ../client/src/shared/util/capabilities
COPY client/src/shared/util/listenerFn ../client/src/shared/util/listenerFn
COPY client/src/shared/util/route/auxiliaryRoute ../client/src/shared/util/route/auxiliaryRoute
COPY client/src/shared/util/route/getActiveStates ../client/src/shared/util/route/getActiveStates
COPY client/src/shared/util/route/getViewName ../client/src/shared/util/route/getViewName
COPY client/src/shared/util/route/stateRegistrar ../client/src/shared/util/route/stateRegistrar
COPY client/src/shared/util/route/viewRegistrar ../client/src/shared/util/route/viewRegistrar
COPY client/src/shared/vorm/types/select ../client/src/shared/vorm/types/select
COPY client/src/shared/vorm/types/textarea ../client/src/shared/vorm/types/textarea
COPY client/src/shared/vorm/util/isEmpty.js ../client/src/shared/vorm/util/isEmpty.js
COPY client/src/shared/vorm/util/isEqual.js ../client/src/shared/vorm/util/isEqual.js
COPY client/src/shared/vorm/vormForm ../client/src/shared/vorm/vormForm
COPY client/src/shared/vorm/vormInvoke ../client/src/shared/vorm/vormInvoke
COPY client/src/shared/zs/vorm/convertValidationException ../client/src/shared/zs/vorm/convertValidationException
COPY client/src/shared/object/zql/zqlEscapeFilter ../client/src/shared/object/zql/zqlEscapeFilter
COPY client/src/shared/ui/zsConfirm ../client/src/shared/ui/zsConfirm
COPY client/src/shared/ui/zsKeyboardNavigableList ../client/src/shared/ui/zsKeyboardNavigableList
COPY client/src/shared/ui/zsNProgress ../client/src/shared/ui/zsNProgress
COPY client/src/shared/ui/zsPagination ../client/src/shared/ui/zsPagination
COPY client/src/shared/ui/zsDisabled ../client/src/shared/ui/zsDisabled
COPY client/src/shared/ui/zsMoveFocusTo ../client/src/shared/ui/zsMoveFocusTo
COPY client/src/shared/ui/zsTrapKeyboardFocus ../client/src/shared/ui/zsTrapKeyboardFocus
COPY client/src/shared/ui/zsEmailPreview ../client/src/shared/ui/zsEmailPreview
COPY client/src/shared/ui/zsPositionFixed ../client/src/shared/ui/zsPositionFixed
COPY client/src/shared/ui/zsTable ../client/src/shared/ui/zsTable
COPY client/src/shared/ui/zsTooltip ../client/src/shared/ui/zsTooltip
COPY client/src/shared/ui/zsTruncate ../client/src/shared/ui/zsTruncate
COPY client/src/shared/util/cachedCollection ../client/src/shared/util/cachedCollection
COPY client/src/shared/util/date/fromIsoCalendarDate.js ../client/src/shared/util/date/fromIsoCalendarDate.js
COPY client/src/shared/util/date/getYearMonthNumber.js ../client/src/shared/util/date/getYearMonthNumber.js
COPY client/src/shared/util/date/utc.js ../client/src/shared/util/date/utc.js
COPY client/src/shared/util/oneWayBind ../client/src/shared/util/oneWayBind
COPY client/src/shared/util/parseSassVariables ../client/src/shared/util/parseSassVariables
COPY client/src/shared/util/propCheck ../client/src/shared/util/propCheck
COPY client/src/shared/util/searchHash.js ../client/src/shared/util/searchHash.js
COPY client/src/shared/util/zsStorage ../client/src/shared/util/zsStorage
COPY client/src/shared/case/createCaseRegistration ../client/src/shared/case/createCaseRegistration
COPY client/src/shared/object/vormObjectSuggest ../client/src/shared/object/vormObjectSuggest
COPY client/src/shared/ui/zsContextualActionMenu ../client/src/shared/ui/zsContextualActionMenu
COPY client/src/shared/util/address/format.js ../client/src/shared/util/address/format.js
COPY client/src/shared/util/number ../client/src/shared/util/number
COPY client/src/shared/vorm/types/radio ../client/src/shared/vorm/types/radio
COPY client/src/shared/util/rwdService ../client/src/shared/util/rwdService
COPY client/src/shared/vorm/util/vormValidator ../client/src/shared/vorm/util/vormValidator
COPY client/src/shared/vorm/vormTemplateService ../client/src/shared/vorm/vormTemplateService
COPY client/src/shared/ui/zsSnackbar ../client/src/shared/ui/zsSnackbar
COPY client/src/shared/util/route/viewTitle ../client/src/shared/util/route/viewTitle
COPY client/src/shared/vorm/vormField ../client/src/shared/vorm/vormField
COPY client/src/shared/ui/zsDatePicker ../client/src/shared/ui/zsDatePicker
COPY client/src/shared/ui/zsModal ../client/src/shared/ui/zsModal
COPY client/src/shared/ui/zsSuggestionList ../client/src/shared/ui/zsSuggestionList
COPY client/src/intern/zsIntern ../client/src/intern/zsIntern
COPY client/src/shared/ui/zsDropdownMenu ../client/src/shared/ui/zsDropdownMenu
COPY client/src/shared/vorm/vormFieldset ../client/src/shared/vorm/vormFieldset
COPY client/src/shared/zs/vorm/zsVormTemplateModifier ../client/src/shared/zs/vorm/zsVormTemplateModifier
COPY client/src/shared/api ../client/src/shared/api
COPY client/src/shared/navigation/zsSideMenu ../client/src/shared/navigation/zsSideMenu
COPY client/src/intern/views/case/zsCaseTimelineView ../client/src/intern/views/case/zsCaseTimelineView
COPY client/src/shared/styles/ ../client/src/shared/styles/
COPY client/src/shared/vorm/types/input ../client/src/shared/vorm/types/input
COPY client/src/shared/contact/createContact ../client/src/shared/contact/createContact
COPY client/src/shared/navigation/zsTopBar ../client/src/shared/navigation/zsTopBar
COPY client/src/shared/object/zsObjectSuggest ../client/src/shared/object/zsObjectSuggest
COPY client/src/shared/ui/zsSpotEnlighter ../client/src/shared/ui/zsSpotEnlighter

#COPY client/src/intern/zsIntern/zsActiveSubject ../client/src/intern/zsIntern/zsActiveSubject
#COPY client/src/intern/zsIntern/zsViewlessIntern ../client/src/intern/zsIntern/zsViewlessIntern

#COPY client/src/shared/api/resource ../client/src/shared/api/resource
#COPY client/src/shared/api/resource/composedReducer ../client/src/shared/api/resource/composedReducer
#COPY client/src/shared/api/resource/mutationService ../client/src/shared/api/resource/mutationService

#COPY client/src/shared/navigation/zsTopBar/zsContextualSettingMenu ../client/src/shared/navigation/zsTopBar/zsContextualSettingMenu
#COPY client/src/shared/navigation/zsTopBar/zsNotificationList ../client/src/shared/navigation/zsTopBar/zsNotificationList

#COPY client/src/shared/object/vormObjectSuggest/vormObjectSuggestDisplay ../client/src/shared/object/vormObjectSuggest/vormObjectSuggestDisplay
#COPY client/src/shared/object/vormObjectSuggest/vormObjectSuggestModel ../client/src/shared/object/vormObjectSuggest/vormObjectSuggestModel

#COPY client/src/shared/object/zsObjectSuggest/objectSuggestionService ../client/src/shared/object/zsObjectSuggest/objectSuggestionService
#COPY client/src/shared/object/zsObjectSuggest/zsObjectSuggestAdvancedSearch ../client/src/shared/object/zsObjectSuggest/zsObjectSuggestAdvancedSearch
#COPY client/src/shared/object/zsObjectSuggest/zsObjectSuggestAdvancedSearch/zsAdvancedSearchResultList ../client/src/shared/object/zsObjectSuggest/zsObjectSuggestAdvancedSearch/zsAdvancedSearchResultList
#COPY client/src/shared/object/zsObjectSuggest/zsObjectSuggestAdvancedSearch/zsSuggestionSubjectList ../client/src/shared/object/zsObjectSuggest/zsObjectSuggestAdvancedSearch/zsSuggestionSubjectList

#COPY client/src/shared/ui/zsContextualActionMenu/contextualActionService ../client/src/shared/ui/zsContextualActionMenu/contextualActionService

#COPY client/src/shared/ui/zsNProgress/zsUiViewProgress ../client/src/shared/ui/zsNProgress/zsUiViewProgress

#COPY client/src/shared/ui/zsPositionFixed/controller.js ../client/src/shared/ui/zsPositionFixed/controller.js

#COPY client/src/shared/ui/zsSnackbar/snackbarService ../client/src/shared/ui/zsSnackbar/snackbarService

#COPY client/src/shared/ui/zsSpotEnlighter/externalSearchService ../client/src/shared/ui/zsSpotEnlighter/externalSearchService
#COPY client/src/shared/ui/zsSpotEnlighter/savedSearchesService ../client/src/shared/ui/zsSpotEnlighter/savedSearchesService
#COPY client/src/shared/ui/zsSpotEnlighter/zsSpotEnlighterSuggestionList ../client/src/shared/ui/zsSpotEnlighter/zsSpotEnlighterSuggestionList

# End the massive import of client code

COPY --from=frontend-package /tmp/frontend .
COPY --from=client-build /opt/zaaksysteem/root ../root
COPY frontend/ .
RUN npm run fullbuild --silent

# Production layer build
FROM nginx:stable as production
USER root
WORKDIR /etc/nginx
COPY docker/inc/frontend/entrypoint.sh /entrypoint.sh
COPY docker/inc/frontend/etc/nginx/ /etc/nginx/
COPY share/apidocs /opt/zaaksysteem/apidocs
COPY --from=frontend-build /opt/zaaksysteem/root/ /opt/zaaksysteem/root/

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

# Backend developement layer build, does not install npm and such
FROM nginx:stable as development
USER root
WORKDIR /etc/nginx
ENV DEBIAN_FRONTEND=noninteractive
COPY dev-bin/generate_dev_certs.sh /tmp/
RUN apt-get update \
    && apt-get install --no-install-recommends -y openssl \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /etc/nginx/ssl
COPY docker/inc/frontend/entrypoint.sh /entrypoint.sh
COPY docker/inc/frontend/etc/nginx/ /etc/nginx/
COPY --from=production /opt/zaaksysteem /opt/zaaksysteem/
ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]

# Frontend developement layer build, does install npm
FROM node:boron as npm-development
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        libfontconfig1-dev xz-utils gnupg2 ca-certificates \
        vim-tiny less sudo \
    && apt-get install --no-install-recommends --no-install-suggests -y nginx \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && npm config set ca "" \
    && npm config set loglevel=warn \
    && npm config set progress=false

COPY dev-bin/generate_dev_certs.sh /tmp/
COPY --from=development /etc/nginx /etc/nginx
COPY --from=development /opt/zaaksysteem /opt/zaaksysteem/
COPY client/package.json /opt/zaaksysteem/client/package.json
COPY --from=frontend-build /opt/zaaksysteem/root /opt/zaaksysteem/root
COPY --from=frontend-build /opt/zaaksysteem/frontend /opt/zaaksysteem/frontend
COPY --from=client-build /opt/zaaksysteem/client /opt/zaaksysteem/client

COPY dev-bin /opt/zaaksysteem/dev-bin
COPY docker/inc/frontend/entrypoint.sh /entrypoint.sh

RUN cd /opt/zaaksysteem/client \
    && npm install --no-optional

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
