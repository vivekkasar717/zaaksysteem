version: '3.6'
services:
    backend:
        build:
            context: "."
            dockerfile: "docker/Dockerfile.backend"
        depends_on:
            - "smtpd"
            - "virusscanner"
            - "redis"
            - "database"
            - "converter"
            - "message-queue"
            - "minio"
            - "swift"
            - "statsd"
        networks:
            - "default"
        environment:
            - "CATALYST_DEBUG=1"
            - "CLAMAV_SERVER=clamav"
            - "CONNECTION_PER_INSTANCE_LIMIT=10"
            - "DOCUMENT_CONVERTER_URL=http://converter:5032"
            - "VIRUS_SCAN_SERVICE_HOST=virusscanner:5000"
            - "VIRUS_SCAN_SERVICE_TEMPLATE=http://{+virus_scan_service_host}/filestore"
            - "PM_MAX_REQUESTS=10"
            - "SMTP_PORT=1025"
            - "SMTP_SERVER=smtpd"
            - "ZAAKSYSTEEM_HOME=/opt/zaaksysteem"
            - "ZS_DISABLE_STUF_PRELOAD=1"
            - "ZS_NO_XSRF_TOKEN_CHECK=1"
            - "ZS_NO_CLIENT_CERT_CHECK=1"
            - "ZS_NUM_THREADS=3"
            - "ZS_JSON_PRETTY=1"
            - "DBIC_TRACE=0"
            - "MAX_PROCESS_SIZE=15%"
            - "ZAAKSYSTEEM_BUNDLE_VERSION=dev"
            - "ZS_FEATURE_FLAG_QUEUE_CLEANUP=1"
            - "ZS_FEATURE_FLAG_SPLIT_BRAIN=1"
            - "ZS_FEATURE_FLAG_TRANSACTION_CLEANUP=1"
        expose:
            - "9083"
        user: "root"
        command:
            - "/opt/zaaksysteem/script/zaaksysteem_dev_server.sh"
        volumes:
            - "./db/testbase:/opt/testbase"
            - "./bin:/opt/zaaksysteem/bin"
            - "./dev-bin:/opt/zaaksysteem/dev-bin"
            - "./etc:/etc/zaaksysteem"
            - "./lib:/opt/zaaksysteem/lib"
            - "./root:/opt/zaaksysteem/root"
            - "./script:/opt/zaaksysteem/script"
            - "./share:/opt/zaaksysteem/share"
            - "./t:/opt/zaaksysteem/t"
            - "./xt:/opt/zaaksysteem/xt"
            - "filestore:/opt/filestore"
        logging:
            driver: "json-file"
            options:
                max-size: "200k"
                max-file: "10"
    api2csv:
        build:
            context: "."
            dockerfile: "docker/Dockerfile.api2csv"
        expose:
            - "1030"
        networks:
            - "default"
        volumes:
            - "./etc:/etc/zaaksysteem"
    frontend:
        build:
            context: "."
            dockerfile: "docker/Dockerfile.frontend"
            target: "development"
        ports:
            - "443:443"
        depends_on:
            - "backend"
            - "api2csv"
            - "swaggerui"
            - "httpd-admin"
            - "httpd-case-management"
            - "httpd-communication"
            - "httpd-document"
            - "minio"
            - "swift"
        networks:
            default:
                aliases:
                    - "dev.zaaksysteem.nl"
                    - "testbase.dev.zaaksysteem.nl"
                    - "testbase-instance.dev.zaaksysteem.nl"
        environment:
            - "RUN_BUILD_HELPERS=1"
            - "CONNECTION_PER_INSTANCE_LIMIT=10"
            - 'SWIFT_REDIRECT_URL=\$$2:\/\/\$$3:\$$4\/\$$5'
        volumes:
            - "./dev-bin:/opt/zaaksysteem/dev-bin"
            - "./frontend/zaaksysteem:/opt/zaaksysteem/frontend/zaaksysteem"
            - "./frontend/zaaksysteem/angular-index.js:/opt/zaaksysteem/frontend/zaaksysteem/angular-index.js"
            - "./frontend/zaaksysteem/gulpfile.js:/opt/zaaksysteem/frontend/zaaksysteem/gulpfile.js"
            - "./frontend/zaaksysteem/index.js:/opt/zaaksysteem/frontend/zaaksysteem/index.js"
            - "./frontend/zaaksysteem/webpack.config.js:/opt/zaaksysteem/frontend/zaaksysteem/webpack.config.js"
            - "./client/src:/opt/zaaksysteem/client/src"
            - "./client/test:/opt/zaaksysteem/client/test"
            - "./client/webpack:/opt/zaaksysteem/client/webpack"
            - "./client/package.json:/opt/zaaksysteem/client/package.json"
            - "./client/jest.config.js:/opt/zaaksysteem/client/jest.config.js"
            - "./root/images:/opt/zaaksysteem/root/images"
            - "./share/apidocs:/opt/zaaksysteem/apidocs"
            - "certificates:/etc/nginx/ssl"
            - "filestore:/opt/filestore"
    frontend-mono:
        image: registry.gitlab.com/zaaksysteem/zaaksysteem-frontend-mono:latest
        environment:
            - DOCKER=true
        depends_on:
          - frontend
    httpd-admin:
        image: "registry.gitlab.com/minty-python/zsnl-admin-http:latest"
        volumes:
            - "./etc/customer.d:/etc/zaaksysteem/customer.d"
            - "./etc/minty_config.conf:/opt/zsnl_admin_http/package.conf"
        command:
            - "pserve"
            - "--server-name"
            - "docker"
            - "development.ini"
        expose:
            - "9084"
        depends_on:
            - "redis"
            - "database"
            - "message-queue"
            - "statsd"
    httpd-case-management:
        image: "registry.gitlab.com/minty-python/zsnl_case_management_http:latest"
        volumes:
            - "./etc/customer.d:/etc/zaaksysteem/customer.d"
            - "./etc/minty_config.conf:/opt/zsnl_case_management_http/package.conf"
        command:
            - "pserve"
            - "--server-name"
            - "docker"
            - "development.ini"
        expose:
            - "9085"
        depends_on:
            - "redis"
            - "database"
            - "message-queue"
            - "statsd"
    httpd-document:
        image: "registry.gitlab.com/minty-python/zaaksysteem/zsnl-document-http:latest"
        volumes:
            - "./etc/customer.d:/etc/zaaksysteem/customer.d"
            - "./etc/minty_config.conf:/opt/zsnl_document_http/package.conf"
        command:
            - "pserve"
            - "--server-name"
            - "docker"
            - "development.ini"
        expose:
            - "9086"
        depends_on:
            - "redis"
            - "database"
            - "message-queue"
            - "swift"
            - "statsd"
    httpd-communication:
        image: "registry.gitlab.com/minty-python/zaaksysteem/zsnl-communication-http:latest"
        volumes:
            - "./etc/customer.d:/etc/zaaksysteem/customer.d"
            - "./etc/minty_config.conf:/opt/zsnl_communication_http/package.conf"
        command:
            - "pserve"
            - "--server-name"
            - "docker"
            - "development.ini"
        expose:
            - "9087"
        depends_on:
            - "redis"
            - "database"
            - "message-queue"
            - "statsd"
    amqp-consumer-minty:
        image: "registry.gitlab.com/minty-python/zsnl_amqp_consumers:latest"
        volumes:
            - "./etc/customer.d:/etc/zaaksysteem/customer.d"
            - "./etc/minty_config.conf:/opt/zsnl_amqp_consumers/config.conf"
        command:
            - "python"
            - "-m"
            - "zsnl_amqp_consumers"
        depends_on:
            - "message-queue"
            - "database"
            - "statsd"
    minio:
        image: minio/minio
        expose:
            - "9000"
        ports:
            - "9000:9000"
        command:
            - "server"
            - "/data"
        environment:
            - "MINIO_ACCESS_KEY=AKIAIOSFODNN7EXAMPLE"
            - "MINIO_SECRET_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
        volumes:
            - "minio-data:/data"
        networks:
            default:
                aliases:
                    - "dev.minio"
    swift:
        build:
            context: "."
            dockerfile: "docker/Dockerfile.swift"
        networks:
            - "default"
        expose:
            - "5000"
            - "8080"
            - "35357"
        volumes:
            - "swift-data:/mnt/sdb1"
        command:
            - "/swift/bin/launch.sh"
        entrypoint:
            - "/swift/bin/entrypoint.sh"
    clamav:
        image: "quay.io/ukhomeofficedigital/clamav:latest"
        networks:
            - "default"
        volumes:
            - "./etc/clamav/clamd.conf:/usr/local/etc/clamd.conf"
    virusscanner:
        image: "registry.gitlab.com/zaaksysteem/zaaksysteem-virus_scanner-service:latest"
        environment:
            - "CLAMAV_SERVER=clamav"
        expose:
            - "5000"
        depends_on:
            - "clamav"
        networks:
            - "default"
        volumes:
            - "./etc/virus_scanner-service.conf:/etc/zaaksysteem-virus_scanner-service/zaaksysteem-virus_scanner-service.conf"
            - "certificates:/etc/nginx/ssl"
    message-queue:
        image: "rabbitmq:3-management-alpine"
        expose:
            - "5672"
        ports:
            - "15672:15672"
        networks:
            - "default"
        volumes:
            - "rabbitmq-data:/var/lib/rabbitmq"
    database:
        image: "postgres:9.6"
        ports:
            - "5432:5432"
        networks:
            - "default"
        environment:
            - "POSTGRES_USER=zaaksysteem"
            - "POSTGRES_PASSWORD=zaaksysteem123"
            - "POSTGRES_DB=zaaksysteem"
        volumes:
            - "dbdata:/var/lib/postgresql/data"
            - "./tmp:/var/tmp"
            - "./db:/opt/zaaksysteem/db"
            - "./db/initial:/docker-entrypoint-initdb.d"
            - "./db/template.sql:/docker-entrypoint-initdb.d/00_zaaksysteem.sql"
    redis:
        image: "redis:5-alpine"
        expose:
            - "6379"
        networks:
            - "default"
    queue-runner:
        image: "registry.gitlab.com/zaaksysteem/queue-runner:latest"
        environment:
            - "REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca.crt"
        depends_on:
            - "frontend"
            - "message-queue"
        networks:
            - "default"
        volumes:
            - "certificates:/etc/ssl/certs"
        command:
            - "message-queue"
            - "5672"
            - "amqp://message-queue:5672"
            - "localhost"
            - "1234"
    converter:
        image: "registry.gitlab.com/zaaksysteem/converter-service:latest"
        expose:
            - "5032"
    smtpd:
        image: mailhog/mailhog
        expose:
            - "1025"
        ports:
          - "8025:8025"
        networks:
            - "default"
    swaggerui:
        image: "swaggerapi/swagger-ui"
        networks:
            - "default"
        environment:
            - "BASE_URL=/apidocs"
            - "API_URL=/doc/api/v1/swagger/index.yaml"
    statsd:
        image: "graphiteapp/docker-graphite-statsd"
        ports:
            - "82:80"
        expose:
            - "8125"
volumes:
    dbdata:
    rabbitmq-data:
    minio-data:
    swift-data:
    filestore:
    certificates:
networks:
    default:
