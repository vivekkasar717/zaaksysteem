<div class="stap-tekst">
    <p class="subnote subnote-stap-beschrijving">
        Controleer uw contactgegevens: Telefoonnummer, mobiele nummer en
        e-mailadres. Dit zijn gegevens die geen deel uitmaken van de
        Basisregistratie Personen (BRP), maar wel toegevoegde waarde kunnen hebben
        bij het behandelen van de zaak.
    </p>
    [% IF aanvrager.messages.briefadres %]
    <p class="subnote subnote-stap-beschrijving">
        Let op: U maakt gebruik van uw briefadres. Mochten onderstaande
        gegevens niet correct zijn, gelieve contact op te nemen met uw gemeente.
    </p>
    [% END %]
    <div class="form-required-fields-explanation"> * = Verplicht veld </div>
</div>

[%- bsn = aanvrager.burgerservicenummer;
    IF bsn;
        bsn = bsn FILTER format('%09d');
    END;
%]

<div class="form aanvrager webformcontent">
   <div class="row">
        <div class="column large-5"><label class="titel">Burgerservicenummer</label></div>
        <div class="column large-7">[% bsn | html_entity %]</div>

    </div>
   <div class="row">
        <div class="column large-5"><label class="titel">Voornamen</label></div>
        <div class="column large-7">[% aanvrager.voornamen | html_entity %]</div>

    </div>
   <div class="row">
        <div class="column large-5"><label class="titel">Tussenvoegsel</label></div>
        <div class="column large-7">[% aanvrager.voorvoegsel | html_entity %]</div>

    </div>
   <div class="row">
        <div class="column large-5"><label class="titel">Achternaam</label></div>
        <div class="column large-7">[% aanvrager.achternaam | html_entity %]</div>

    </div>
    [% IF aanvrager.adellijke_titel %]
    <div class="row">
         <div class="column large-5"><label class="titel">Adellijke titel</label></div>
         <div class="column large-7">[% aanvrager.adellijke_titel | html_entity %]</div>

     </div>
    [% END %]
   <div class="row">
        <div class="column large-5"><label class="titel">Geboortedatum</label></div>
        <div class="column large-7">[% aanvrager.geboortedatum.clone().set_time_zone('Europe/Amsterdam').dmy %]</div>

    </div>
    [% IF aanvrager.landcode == 6030 %]
    <div class="row">
        <div class="column large-5"><label class="titel">Woonplaats</label></div>
        <div class="column large-7">[% aanvrager.woonplaats | html_entity %]</div>
    </div>
    <div class="row">
        <div class="column large-5"><label class="titel">Straatnaam</label></div>
        <div class="column large-7">[% aanvrager.straatnaam | html_entity %]</div>
    </div>
    <div class="row">
        <div class="column large-5"><label class="titel">Huisnummer</label></div>
        <div class="column large-7">[% aanvrager.huisnummer | html_entity %]</div>
    </div>
    <div class="row">
        <div class="column large-5"><label class="titel">Huisletter</label></div>
        <div class="column large-7">[% aanvrager.huisletter | html_entity %]</div>
    </div>
    <div class="row">
        <div class="column large-5"><label class="titel">Huisnummertoevoeging</label></div>
        <div class="column large-7">[% aanvrager.huisnummertoevoeging | html_entity %]</div>
    </div>
    <div class="row">
        <div class="column large-5"><label class="titel">Postcode</label></div>
        <div class="column large-7">[% aanvrager.postcode | html_entity %]</div>
    </div>
    [% ELSE %]
    <div class="row">
        <div class="column large-5"><label class="titel">Adresregel 1:</label></div>
        <div class="column large-7">[% aanvrager.adres_buitenland1 | html_entity %]</div>
    </div>
    <div class="row">
        <div class="column large-5"><label class="titel">Adresregel 2:</label></div>
        <div class="column large-7">[% aanvrager.adres_buitenland2 | html_entity %]</div>
    </div>
    <div class="row">
        <div class="column large-5"><label class="titel">Adresregel 3:</label></div>
        <div class="column large-7">[% aanvrager.adres_buitenland3 | html_entity %]</div>
    </div>
    [% END %]
    <div class="row">
        <div class="column large-5"><label class="titel" for="telefoonnummer">Telefoonnummer
            [%- IF zaaktype_node.contact_info_phone_required %]*[% END -%]
        </label></div>
        <div class="column large-7">
            <input
                id="telefoonnummer"
                type="text"
                name="npc-telefoonnummer"
                value="[% aanvrager.telefoonnummer %]"
                class="input_large"
            />
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-5"><label class="titel" for="mobiel">Telefoonnummer (mobiel)
            [%- IF zaaktype_node.contact_info_mobile_phone_required %]*[% END -%]
        </label></div>
        <div class="column large-7">
            <input
                id="mobiel"
                type="text"
                name="npc-mobiel"
                value="[% aanvrager.mobiel %]"
                class="input_large"
            />
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>
    
    
    <div class="row">
        <div class="column large-5"><label class="titel" for="emailadres">E-mailadres
            [%- IF zaaktype_node.contact_info_email_required %]*[% END -%]
        </label></div>
        <div class="column large-7">
            <input
                id="emailadres"
                type="text"
                name="npc-email"
                value="[% aanvrager.email %]"
                class="input_large"
            />
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>
</div>

<div class="form-required-fields-explanation"> * = Verplicht veld </div>

<!--div class="aanvullende-gegevens">
            Uw telefoonnummer, mobiele nummer en e-mailadres kunt u desgewenst
            aanvullen of aanpassen. Dit zijn gegevens die geen deel uitmaken
            van de Gemeentelijke Basisadministratie (GBA), maar wel
            toegevoegde waarde kunnen hebben bij het behandelen van de zaak.
</div-->

