/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsBreadcrumb', [ '$parse', function ( $parse ) {
			
			return {
				controller: [ '$element', '$scope', '$attrs', function ( $element, $scope, $attrs ) {
					
					var ctrl = this,
						parser = $parse($attrs.zsBreadcrumb),
						callback = $parse($attrs.zsBreadcrumbSelect);
					
					ctrl.select = function ( value ) {
						callback($scope, {
							'$value': value
						});
					};
					
					ctrl.isSelected = function ( value ) {
						return parser($scope) === value;
					};
					
					$element.addClass('breadcrumb');
					
					return ctrl;
					
				}]
			};
			
		}]);
	
})();
