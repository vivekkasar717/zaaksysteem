/*global fetch,define*/
(function ( ) {
	
	window.zsDefine('nl.mintlab.utils.dom.fromGlobalToLocal', function ( ) {
		
		var getViewportPosition = window.zsFetch('nl.mintlab.utils.dom.getViewportPosition');
		
		return function ( element, point ) {
			var docPos = getViewportPosition(element),
				x = point.x - docPos.x,
				y = point.y - docPos.y;
				
			return { x: x, y: y };
		};
	});
		
})();
