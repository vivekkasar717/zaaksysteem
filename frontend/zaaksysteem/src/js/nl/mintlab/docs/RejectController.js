/*global angular*/
angular
	.module('Zaaksysteem.docs')
	.controller('nl.mintlab.docs.RejectController', [
		'$scope',
		function ( $scope ) {
			$scope.reject = function ( entities ) {
				$scope.rejectFile(entities, $scope.rejection_reason, $scope.reject_to_queue);
				$scope.closePopup();
			};
		}
	]);
