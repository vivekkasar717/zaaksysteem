/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.sysin', [ 'Zaaksysteem.sysin.links', 'Zaaksysteem.sysin.transactions', 'Zaaksysteem.sysin.records', 'Zaaksysteem.sysin.datastore' ] );
	
})();