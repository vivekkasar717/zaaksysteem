/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.sysin.transactions')
		.controller('nl.mintlab.sysin.transactions.TransactionCrudController', [ '$scope', function ( $scope ) {
		
			$scope.$on('manualprocess', function ( /*evt*/ ) {
				$scope.reloadData();
			});
		
		}]);
	
})();