/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.SidebarController', [ '$scope', function ( $scope ) {
			
			$scope.caseId = null;
			
			$scope.reloadData = function ( ) {
			};
			
			$scope.init = function ( caseId, phaseId, closed, showChecklist ) {
				$scope.caseId = caseId;
				$scope.phaseId = phaseId;
				$scope.closed = closed;
				$scope.showChecklist = showChecklist;
			};
			
		}]);
	
})();
