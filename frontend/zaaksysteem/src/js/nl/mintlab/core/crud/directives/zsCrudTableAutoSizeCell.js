/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsCrudTableAutoSizeCell', [ function ( ) {
			
			return {
				require: '^zsCrudTableAutoSizeRow',
				link: function ( scope, element, attrs, zsCrudTableAutoSizeRow ) {
					
					zsCrudTableAutoSizeRow.register(element[0]);
					
					scope.$on('destroy', function ( ) {
						zsCrudTableAutoSizeRow.unregister(element[0]);
					});
				}
			};
			
		}]);
	
})();