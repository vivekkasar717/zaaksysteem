/*global angular,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.core')
		.factory('safeApply', [ function ( ) {
			
			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');
			
			return safeApply;
			
		}]);
	
})();
