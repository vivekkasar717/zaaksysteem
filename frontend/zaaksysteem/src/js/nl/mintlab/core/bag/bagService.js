/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.core.bag')
		.factory('bagService', [ '$parse', function ( $parse ) {
			
			var bagService = {},
				streetLabel = 'streetname + (number && (\' \' + number) || \'\' ) + \', \' + city',
				bagLabel = 'street + (number && (\' \' + number) || \'\' ) + \', \' + city';
							
			bagService.getStreetLabel = function ( ) {
				return streetLabel;
			};
			
			bagService.getLabel = function ( ) {
				return bagLabel;	
			};
			
			bagService.parseLabel = function ( obj ) {
				var tpl;
				if(obj.streetname) {
					tpl = streetLabel;
				} else {
					tpl = bagLabel;
				}
				return $parse(tpl)(obj);
			};
			
			return bagService;
			
		}]);
	
})();