/*global angular*/
(function () {
	"use strict";
	angular.module('Zaaksysteem.admin')
		.controller('nl.mintlab.plugins.WozTaxatieverslagController', ['$scope', '$http', '$sce', function ($scope, $http, $sce ) {

			$scope.init = function (id, owner, woz_object_id) {
				var url = '/plugins/woz/object/' + id + '?owner=' + owner + '&object_id=' + woz_object_id;

				$http({
					method: 'GET',
					url: url
				})
					.success(function ( response ) {
						$scope.woz_object = response;
						$scope.woz_object.Huisnummer = parseInt($scope.woz_object.Huisnummer, 10);
					});
					
				$http({
					method: 'GET',
					url: '/plugins/woz/settings'
				})
					.success(function ( response ) {
						$scope.settings = response;
					});
					
			};
			
			$scope.getTrustedHtml = function ( html ) {
				return $sce.trustAsHtml(html);	
			};
			
		}]);
}());