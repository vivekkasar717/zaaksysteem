/*global window*/
(function ( ) {
	
	var registered = {};
	var resolved = {};
	
	// define functions in the global namespace
	
	window.zsDefine = function ( alias, factory ) {
		if(registered[alias]) {
			throw new Error('cannot redefine alias ' + alias);
		}
		registered[alias] = factory;
	};
	
	window.zsFetch = function ( alias ) {
		if(!resolved[alias]) {
			if (!registered[alias]) {
				throw new Error('No Registration was found for ' + alias);
			}
			resolved[alias] = registered[alias]();
		}
		return resolved[alias];
	};
	
})();
