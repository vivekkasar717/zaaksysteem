// NB: devServer has its own stats configuration
module.exports = {
  assets: true,
  cached: false,
  cachedAssets: false,
  children: false,
  chunks: false,
  chunkModules: false,
  chunkOrigins: false,
  colors: true,
  depth: false,
  entrypoints: false,
  errors: true,
  errorDetails: true,
  hash: false,
  maxModules: 0,
  modules: false,
  moduleTrace: false,
  performance: true,
  providedExports: false,
  publicPath: false,
  reasons: false,
  source: false,
  timings: true,
  usedExports: false,
  version: true,
  warnings: true,
};
