/**
 * Get the keys from the dependencies object in /client/package.json
 */
const { dependencies } = require('../package');

const { keys } = Object;

const getClientDependencies = () => keys(dependencies);

module.exports = getClientDependencies;
