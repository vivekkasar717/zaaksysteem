const glob = require('glob');
const { cyan, green, magenta, red, yellow } = require('chalk');
const { join, resolve } = require('path');
const { dependencies } = require('../../package');
const getVendorImports = require('./getVendorImports');
const getModuleBaseName = require('./getModuleBaseName');

const { keys } = Object;
const { error, info } = console;

info(`Recursively resolving ${magenta('import')} statements; this might take a moment.`);

const globSync = globExpression =>
  glob
    .sync(globExpression, {
      cwd: resolve(process.cwd(), '..', 'client', 'src'),
    });

const views = [
  // static imports, lexically analized
  ...globSync('*/index.js'),
  // dynamic route requires (cf. intern/routing.js)
  ...globSync('intern/views/*/{route,index,controller}.js'),
  // legacy case tabs
  ...[
    'zsCaseDocumentView',
    'zsCaseTimelineView',
  ]
    .map(id => join('intern', 'views', 'case', id)),
];

const cache = [];
const apps = [];

info('Entry points:');

for (const file of views) {
  info(`- ${yellow(file)}`);
  apps.push(
    ...getVendorImports(file, cache)
      .vendors
      .filter(dependency =>
        !apps.includes(dependency)
      )
  );
}

apps.sort();

const npmModules = apps
  .reduce((result, moduleName) => {
    const moduleBaseName = getModuleBaseName(moduleName);

    if (!result.includes(moduleBaseName)) {
      result.push(moduleBaseName);
    }

    return result;
  }, []);

info(`${magenta(npmModules.length)} installed npm modules found:`);

for (const dependency of npmModules) {
  info(`- ${cyan(dependency)}`);
}

const dependencyNames = keys(dependencies);
const missingDependencies = [];

for (const dependency of npmModules) {
  const moduleName = getModuleBaseName(dependency);

  if (!dependencyNames.includes(moduleName)) {
    missingDependencies.push(moduleName);
  }
}

if (missingDependencies.length) {
  for (const missingDependency of missingDependencies) {
    error(`Missing ${yellow('npm')} dependency: ${red(missingDependency)}`);
  }

  process.exit(1);
}

info(green('OK.'));

module.exports = apps;
