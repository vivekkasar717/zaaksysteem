const { cyan, yellow } = require('chalk');
const { join, resolve } = require('path');
const fsExtra = require('fs-extra');
const { PUBLIC_PATH, ROOT, VENDOR } = require('./constants');
const catchError = require('./catchError');

try {
  fsExtra
    .copySync(
      resolve('webpack', 'distribution', VENDOR),
      join(ROOT, PUBLIC_PATH, VENDOR)
    );
} catch (error) {
  catchError(error, `Did you build the ${cyan('vendors')} bundle ($ ${yellow('npm run build-vendors')})?`);
}
