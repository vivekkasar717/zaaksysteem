import angular from 'angular';
import mutationServiceModule from '../../../shared/api/resource/mutationService';
import propCheck from '../../../shared/util/propCheck';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import configServiceModule from '../../shared/configService';
import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';

export default
	angular.module('Zaaksysteem.mor.caseCompleteActions', [
		mutationServiceModule,
		snackbarServiceModule,
		configServiceModule
	])
	.run([ 'snackbarService', 'mutationService', 'configService', ( snackbarService, mutationService, configService ) => {

		mutationService.register( {
			type: 'MOR_UPDATE_CASE',
			request: ( mutationData ) => {

				propCheck.throw(
					propCheck.shape({
						caseRef: propCheck.string,
						updateValues: propCheck.object
					}),
					mutationData
				);

				if (isEmpty(mutationData.updateValues.values)) {
					return true;
				}

				return {
					url: `/api/v1/case/${mutationData.caseRef}/update`,
					method: 'POST',
					data: mutationData.updateValues,
					headers: {
						'API-Interface-Id': configService.getInterfaceId()
					}
				};
			},
			reduce: ( data, mutationData ) => {

				return data.map(
					caseItem => {

						if (caseItem.reference === mutationData.caseRef) {

							return caseItem.merge({
								instance: {
									attributes: mutationData.updateValues.values
								}
							}, { deep: true });
						}

						return caseItem;

					}
				);

			},
			wait: ( mutationData, promise ) => {
				return snackbarService.wait('Zaak wordt opgeslagen', {
					promise,
					collapse: 0,
					catch: ( ) => 'Er ging iets mis bij opslaan van de waardes van de zaak. Neem contact op met uw beheerder.'
				});
			}

		});

		mutationService.register( {
			type: 'MOR_TRANSITION_CASE',
			request: ( mutationData ) => {

				propCheck.throw(
					propCheck.shape({
						caseRef: propCheck.string,
						result: propCheck.object
					}),
					mutationData
				);

				return {
					url: `/api/v1/case/${mutationData.caseRef}/transition`,
					method: 'POST',
					data: mutationData.result,
					headers: {
						'API-Interface-Id': configService.getInterfaceId()
					}
				};
			},
			reduce: ( data, mutationData ) => {
				return data.map(
					caseItem => {

						if (caseItem.reference === mutationData.caseRef) {

							return caseItem.merge({
								instance: {
									result: mutationData.result.values
								}
							}, { deep: true });
						}

						return caseItem;

					}
				);

			},
			wait: ( mutationData, promise ) => {

				return snackbarService.wait('Zaak wordt afgehandeld', {
					promise,
					collapse: 0,
					then: ( ) => 'Zaak is afgehandeld',
					catch: ( error ) => {

						let message,
							actions = configService.getSupportLink() ?
								[
									{
										type: 'link',
										label: 'Meld aan beheerder',
										link: configService.getSupportLink()
									}
								] : [];


						switch (get(error, 'data.result.instance.message')) {

							default:
							message = 'Er ging iets mis bij het afhandelen van de zaak. Neem contact op met uw beheerder voor meer informatie.';
							break;

							case 'No transition possible for this case, missing documents':
							message = 'Er staat een document voor deze zaak in de wachtrij. Of een verplicht document is niet toegevoegd. Deze moet geaccepteerd of toegevoegd worden voordat de zaak afgehandeld kan worden.';
							break;

							case 'No transition possible for this case, missing fields':
							message = 'Een verplicht veld voor deze zaak is niet ingevuld. Deze zaak kan niet afgehandeld worden voordat dit is ingevuld.';
							break;

							case 'No transition possible for this case, missing fields, documents':
							message = 'Er staat een document voor deze zaak in de wachtrij en een verplicht veld is niet ingevuld.';
							break;

							case 'No transition possible for this case, missing result':
							message = 'Er is een configuratieprobleem met dit zaaktype. Neem contact op met uw beheerder voor meer informatie.';
							break;

						}

						return {
							message,
							actions
						};

					}
				});
			}

		});

	}])
	.name;
