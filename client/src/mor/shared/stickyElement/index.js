import angular from 'angular';

export default
	angular.module('Zaaksysteem.mor.stickyElement', [
	])
		.directive('stickyNav', [ '$window', ( $window ) => {

			let stickyNavLink = ( scope, element ) => {

				let w = angular.element($window),
				size = element[0].clientHeight,
				top = 0;

				let toggleStickyNav = ( ) => {
					if (!element.hasClass('controls-fixed') && $window.pageYOffset > top + size) {

						element.addClass('controls-fixed');

					} else if (element.hasClass('controls-fixed') && $window.pageYOffset <= top + size) {

						element.removeClass('controls-fixed');
					}
				};

				scope.$watch( ( ) => {

					return element[0].getBoundingClientRect().top + $window.pageYOffset;

				}, (newValue, oldValue) => {

					if (newValue !== oldValue && !element.hasClass('controls-fixed')) {

						top = newValue;

					}
				});

				w.bind('resize', ( ) => {

					element.removeClass('controls-fixed');
					top = element[0].getBoundingClientRect().top + $window.pageYOffset;
					toggleStickyNav();

				});

				w.bind('scroll', toggleStickyNav);

			};

			return {
				scope: {},
				restrict: 'A',
				link: stickyNavLink
			};

		}
	])
	.name;
