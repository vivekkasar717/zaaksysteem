import angular from 'angular';
import template from './index.html';
import resourceModule from '../../shared/api/resource';
import composedReducerModule from './../../shared/api/resource/composedReducer';
import caseListViewModule from './caseListView';
import configServiceModule from './../shared/configService';
import savedSearchesServiceModule from './../../shared/ui/zsSpotEnlighter/savedSearchesService';
import snackbarServiceModule from './../../shared/ui/zsSnackbar/snackbarService';
import seamlessImmutable from 'seamless-immutable';
import map from 'lodash/map';
import appServiceModule from '../shared/appService';

export default {
	moduleName:
		angular.module('Zaaksysteem.mor.caseList', [
			resourceModule,
			caseListViewModule,
			configServiceModule,
			snackbarServiceModule,
			savedSearchesServiceModule,
			composedReducerModule,
			appServiceModule
		])
		.name,
	config: [
		{
			route: {
				url: '/zaken/:caseType',
				template,
				title: [ 'configService', ( configService ) => {

					return configService.getConfig().title;

				}],
				resolve: {
					caseGroups: [
						'$rootScope', '$q', '$stateParams', 'resource', 'composedReducer', 'configService', 'savedSearchesService', 'snackbarService', 'config', 'user',
						( $rootScope, $q, $stateParams, resource, composedReducer, configService, savedSearchesService, snackbarService, config, userResource ) => {

							let groups = [],
								reducer,
								scope = $rootScope;

							if ($stateParams.caseType === 'open') {
								
								groups = groups.concat(
									{
										id: 'open',
										label: 'In behandeling',
										resource: resource(
											( ) => {

												let ids = map(configService.getConfig().casetypes, 'casetype.values.casetype_id'),
													zql = ids.length ?
														`${savedSearchesService.getZql(
															{ id: 'mine' },
															{
																from: 'case',
																user: userResource.data()
															}
														)} AND (case.casetype.id IN ("${ids.join('","')}")) NUMERIC ORDER BY case.number ASC `
														: '';

												return ids.length ?
													{
														url: '/api/v1/case/',
														params: {
															zql
														}
													}
													: null;
											},
											{ scope }
										),
										empty: {
											icon: 'check-all',
											message: 'Je hebt geen zaken in behandeling. <br>Open een zaak uit de werkvoorraad om te beginnen.'
										}
									}
								);

								groups = groups.concat(
									{
										id: 'intake',
										label: 'Werkvoorraad',
										resource: resource(
											( ) => {

												let ids = map(configService.getConfig().casetypes, 'casetype.values.casetype_id'),
													zql = ids.length ?
														`${savedSearchesService.getZql(
															{ id: 'intake' },
															{
																from: 'case',
																user: userResource.data()
															}
														)} WHERE (case.casetype.id IN ("${ids.join('","')}"))`
														: '';

												zql += ' NUMERIC ORDER BY case.number ASC';

												return ids.length ?
													{
														url: '/api/v1/case/',
														params: {
															zql,
															rows_per_page: $stateParams.paging || 10
														}
													}
													: null;
											},
											{ scope }
										),
										empty: {
											icon: 'check-all',
											message: 'Je hebt geen zaken in je werkvoorraad.'
										}
									}
								);

							} else {

								groups = groups.concat(
									{
										id: 'intake',
										label: 'Afgehandeld',
										resource: resource(
											( ) => {

												let ids = map(configService.getConfig().casetypes, 'casetype.values.casetype_id');

												return ids.length ?
													{
														url: '/api/v1/case/',
														params: {
															zql: `SELECT {} FROM case WHERE (case.status = "resolved") AND (case.casetype.id IN ("${ids.join('","')}")) NUMERIC ORDER BY case.number DESC`,
															rows_per_page: $stateParams.paging || 10
														}
													}
													: null;
											},
											{ scope }
										).reduce(( requestOptions, cases ) => {
											return cases ? seamlessImmutable(cases) : [];
										}),
										empty: {
											icon: 'check-all',
											message: 'Er zijn geen afgehandelde zaken beschikbaar.'
										}
									}
								);
							}

							reducer = composedReducer({ scope: $rootScope }, ...groups.map(group => group.resource))
								.reduce( ( ) => {

									return seamlessImmutable(groups).map(
										group => {

											let pager = group.resource.source().result.instance.pager,
												count = pager.total_rows,
												showMore = false;

											if (count > pager.rows && pager.rows < 20) {
												count = `${pager.rows}+`;
												showMore = true;
											}

											if (count) {
												count = ` (${count})`;
											} else {
												count = '';
											}

											return group.merge({
												cases: group.resource.data(),
												label: `${group.label}${count}`,
												showMore
											});
										}
									);

								});

							return $q.all(groups.map(group => group.resource.asPromise()))
								.then(( ) => reducer)
								.catch(( err ) => {

									snackbarService.error('Er ging iets fout bij het ophalen van de zaken. Neem contact op met uw beheerder voor meer informatie.');

									return $q.reject(err);
								});

						}

					]
				},
				params: {
					paging: {
						squash: true,
						value: 10
					}
				},
				onExit: [ 'caseGroups', ( caseGroups ) => {

					caseGroups.destroy();

				}],
				controller: [ '$timeout', '$state', '$stateParams', '$scope', 'caseGroups', 'appService', ( $timeout, $state, $stateParams, $scope, caseGroups, appService ) => {

					$scope.caseGroups = caseGroups;

					$scope.handleLoadMore = ( ) => {
						$state.go($state.current.name, { paging: ($stateParams.paging || 10) + 10 }, { inherit: true });
					};

					appService.on('reload_case_groups', ( ) => {
						caseGroups.data().map( ( group ) => {
							group.resource.reload();
						});
					});

				}]
			},
			state: 'caseList'
		}
	]
};
