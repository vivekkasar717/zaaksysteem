export default ( values ) => {
	return values.related_subject_role !== 'anders' ?
		values.related_subject_role
		: values.related_subject_role_freeform;
};
