const doTwoRectanglesOverlap = ({ aLeft, aRight, aTop, aBottom }, { bLeft, bRight, bTop, bBottom }) =>
  aLeft > bRight ||
  bLeft > aRight ||
  aTop > bBottom ||
  bTop > aBottom;

const doesRectangleOverlapWithOthers = (rectangle, rectangles) =>
  rectangles.every(rect =>
    doTwoRectanglesOverlap(rectangle, rect)
  );

export const doWidgetsFit = widgets => {
  const validatedWidgets = [];

  return widgets.every(({ column, row, size_x, size_y }) => {
    const aLeft = column;
    const aRight = column + size_x - 1;
    const aTop = row;
    const aBottom = row + size_y - 1;

    const fits = doesRectangleOverlapWithOthers({ aLeft, aRight, aTop, aBottom }, validatedWidgets);
  
    validatedWidgets.push({
      bLeft: aLeft,
      bRight: aRight,
      bTop: aTop,
      bBottom: aBottom
    });
  
    return fits;
  });
};
