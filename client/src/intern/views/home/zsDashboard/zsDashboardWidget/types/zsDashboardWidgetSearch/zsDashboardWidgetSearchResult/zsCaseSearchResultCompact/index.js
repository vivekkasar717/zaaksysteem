import angular from 'angular';
import zsCaseStatusIconModule from './../../../../../../../../../shared/case/zsCaseStatusIcon';
import zsProgressBarModule from './../../../../../../../../../shared/ui/zsProgressBar';
import zsTruncateHtmlModule from './../../../../../../../../../shared/ui/zsTruncate/zsTruncateHtml';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsCaseSearchResultCompact', [
		zsCaseStatusIconModule,
		zsProgressBarModule,
		zsTruncateHtmlModule
	])
		.directive('zsCaseSearchResultCompact', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					item: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
