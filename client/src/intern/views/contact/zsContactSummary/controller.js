import first from 'lodash/first';
import getFullName from '../../../../shared/util/subject/getFullName';

export default class ContactSummaryController {
	constructor( scope, sce, composedReducer ) {
		const contactDetailsReducer = composedReducer({ scope }, this.subject, this.isCollapsed)
			.reduce(( subject, isCollapsed ) => {
				const { instance } = subject.instance.subject;
				const phone = instance.mobile_phone_number || instance.phone_number;
				const email = instance.email_address;
				const list = [];

				if (phone) {
					const label = 'Telefoonnummer';

					list.push({
						name: 'phone',
						label: (isCollapsed ? `${label}: ${phone}` : label),
						value: phone
					});
				}

				if (email) {
					const label = 'E-mailadres';

					list.push({
						name: 'email',
						label: (isCollapsed ? `${label}: ${email}` : label),
						value: sce.trustAsHtml(`<a href="mailto:${email}">${email}</a>`)
					});
				}

				return list;
			});

		this.getContactDetails = contactDetailsReducer.data;
	}

	get contact() {
		return this.subject();
	}

	get fullName() {
		return getFullName(this.contact);
	}

	get initial() {
		return first(this.fullName);
	}
}
