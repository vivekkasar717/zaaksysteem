import angular from 'angular';
import ContactInformationController from './controller';
import template from './template.html';
import './styles.scss';

const controller = [ '$sce', ContactInformationController ];

export default angular
	.module('zsContactInformationView', [])
	.component('zsContactInformationView', ({
		bindings: {
			subject: '&'
		},
		controller,
		template
	}))
	.name;
