import get from 'lodash/get';
import template from 'lodash/template';

export default ( error, locals = { description: 'deze pagina' } ) => {

	let status = get(error, 'status', 500),
		message,
		data = get(error, 'data.result', error);

	switch (status) {

		default:
		message = template('Er ging iets fout bij het laden van <%= description %>. Neem contact op met uw beheerder voor meer informatie.')(locals);
		break;

	}

	return {
		status,
		message,
		data
	};

};
