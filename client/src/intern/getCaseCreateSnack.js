import assign from 'lodash/fp/assign';
import propCheck from './../shared/util/propCheck';

export default ( $state, values, config = {} ) => {
	let actions = [];
	let message;

	propCheck.throw(
		propCheck.shape({
			caseNumber: propCheck.number.optional,
			assigneeResult: propCheck.string.optional,
			userIsAssignee: propCheck.bool.optional,
			status: propCheck.string.optional,
			$state: propCheck.object
		}),
		assign({ $state }, values)
	);

	// ZS-TODO: clarify and simplify the conditionals
	if (
		(values.caseNumber && $state.current.name.indexOf('case') === -1 )
		&& !values.userIsAssignee
	) {
		message = `Zaak geregistreerd onder zaaknummer ${values.caseNumber}`;

		if (values.assigneeResult === 'failed') {
			message += ', maar kon niet worden toegewezen aan een behandelaar.';
		}
	} else {
		message = values.status === 'open' ?
			'Zaak is door u in behandeling genomen'
			: 'Zaak is geregistreerd';

		if (values.caseNumber) {
			message += ` onder zaaknummer ${values.caseNumber}`;
		}
	}

	return assign({
		message,
		actions
	}, config);
};
