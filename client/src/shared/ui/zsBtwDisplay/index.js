import angular from 'angular';
import controller from './controller';
import template from './template.html';

controller.$inject = [ 'currencyFilter' ];

export default angular
	.module('zsBtwDisplay', [])
	.component('zsBtwDisplay', {
		bindings: {
			value: '&',
			btwType: '&'
		},
		controller,
		template
	})
	.name;
