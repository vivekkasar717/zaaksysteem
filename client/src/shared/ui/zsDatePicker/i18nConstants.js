/**
 * @type {string}
 */
export const previousMonth = 'Vorige maand';

/**
 * @type {string}
 */
export const nextMonth = 'Volgende maand';

/**
 * @type {Array<string>}
 */
export const months = [
	'Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni',
	'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'
];

/**
 * @type {Array<string>}
 */
export const weekdays = [
	'Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'
];

/**
 * @type {Array<string>}
 */
export const weekdaysShort = weekdays.map(day => day.substring(0, 2));

/**
 * @type {Object}
 * @property {previousMonth}
 * @property {nextMonth}
 * @property {months}
 * @property {weekdays}
 * @property {weekdaysShort}
 */
export default {
	previousMonth,
	nextMonth,
	months,
	weekdays,
	weekdaysShort
};
