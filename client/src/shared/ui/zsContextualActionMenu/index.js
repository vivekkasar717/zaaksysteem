import angular from 'angular';
import zsContextualActionMenuButtonModule from './zsContextualActionMenuButton';
import zsContextualActionFormModule from './zsContextualActionForm';
import vormInvokeModule from './../../vorm/vormInvoke';
import template from './template.html';
import contextualActionServiceModule from './contextualActionService';
import './addbutton.scss';

export default
	angular.module('zsContextualActionMenu', [
		zsContextualActionMenuButtonModule,
		zsContextualActionFormModule,
		vormInvokeModule,
		contextualActionServiceModule
	])
		.directive('zsContextualActionMenu', [ '$rootScope', '$compile', '$animate', '$document', 'contextualActionService', 'vormInvoke', ( $rootScope, $compile, $animate, $document, contextualActionService, vormInvoke ) => {

			return {
				restrict: 'E',
				template,
				scope: {},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this;

					let closeMenu = ( ) => {
						contextualActionService.closeAllActions();
					};

					ctrl.getOpenActions = ( ) => contextualActionService.getOpenActions();

					ctrl.handleClickOutside = ( ) => {

						if (ctrl.getOpenActions().length) {
							scope.$evalAsync(closeMenu);
						}

						return false;
					};

					ctrl.handleActionClose = ( action ) => {
						contextualActionService.closeAction(action);
					};

					ctrl.getOptions = ( ) => {
						let filtered = contextualActionService.getAvailableActions().filter(
							option => {

								return option.when === undefined || !!vormInvoke(option.when, { viewController: option.controller });
							}
						);

						return filtered;
					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
