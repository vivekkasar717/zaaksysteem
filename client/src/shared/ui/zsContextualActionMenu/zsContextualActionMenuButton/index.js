import angular from 'angular';
import controller from './../../zsDropdownMenu/controller';
import capabilitiesModule from './../../../util/capabilities';
import zsTooltipModule from './../../zsTooltip';
import template from './template.html';
import './styles.scss';

export default
	angular.module('shared.ui.zsContextualActionMenuButton', [
		zsTooltipModule,
		capabilitiesModule
	])
		.directive('zsContextualActionMenuButton', [
			'$document', '$animate', '$timeout', 'capabilities',
			( $document, $animate, $timeout, capabilities ) => {

				const TIMEOUT = 50,
					MAX_DELTA = 0;

				return {
					restrict: 'E',
					template,
					scope: {
						isActive: '&',
						actions: '&'
					},
					bindToController: true,
					controller: [ '$scope', '$element', function ( $scope, $element ) {

						let ctrl = this,
							style = {},
							prevPos,
							timeout,
							container = angular.element($element[0].querySelector('.event-wrapper'));

						let resetTimer = ( ) => {

							$timeout.cancel(timeout);

							timeout = $timeout(( ) => {

								timeout = null;

								ctrl.openMenu();

							}, TIMEOUT);

						};

						let handleKeyUp = ( event ) => {
							if (event.keyCode === 27 && ctrl.isMenuOpen()) {
								ctrl.closeMenu();
							}
						};

						if (!capabilities().touch) {

							ctrl.mode = ( ) => 'custom';

							container.bind('mouseenter', ( ) => {

								$scope.$evalAsync(resetTimer);
								
							});

							container.bind('mouseleave', ( ) => {

								$scope.$evalAsync(( ) => {

									if (timeout) {
										$timeout.cancel(timeout);
										timeout = null;
									} else {
										ctrl.closeMenu();
									}

								});

								prevPos = null;

							});

							container.bind('mousemove', ( event ) => {

								if (ctrl.isMenuOpen() || !timeout) {
									return;
								}

								let deltaX,
									deltaY,
									x = event.clientX,
									y = event.clientY;

								if (prevPos) {

									deltaX = Math.abs(prevPos.x - x);
									deltaY = Math.abs(prevPos.y - y);

									if (Math.max(deltaX, deltaY) > MAX_DELTA) {

										$scope.$evalAsync(resetTimer);

									}

								}

								prevPos = { x, y };

							});

							
							$document.bind('keyup', handleKeyUp);
							
						}

						ctrl.options = ( ) => {
							return ctrl.isMenuOpen() ? ctrl.actions() : null;
						};

						controller.call(this, $document, $scope, $element, $animate, capabilities());

						ctrl.buttonStyle = ( ) => style;

						ctrl.isActive({ $getter: ctrl.isMenuOpen });

						ctrl.titleOptions = {
							attachment: 'left middle',
							target: 'right middle',
							offset: { x: 10, y: 0 }
						};

					}],
					controllerAs: 'vm'
				};

		}])
		.name;
