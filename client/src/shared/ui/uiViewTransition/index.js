import angular from 'angular';
import some from 'lodash/some';
import getViewName from './../../util/route/getViewName';

export default
	angular.module('uiViewTransition', [

	])
		.factory('uiViewTransitionService', [ '$rootScope', '$interpolate', ( $rootScope, $interpolate ) => {

			let transitioning = {
					// default view
					null: false
				};

			let getTargetViewFromName = ( name ) => {
				let path = name.split('.'),
					parent = path[path.length - 2] || null;

				if (parent) {
					parent = parent.replace(/$@/, '');
				}

				return parent;
			};

			let handleStart = ( event, state ) => {
				
				let parent = getTargetViewFromName(state.name);

				transitioning[parent] = true;
			};

			let handleComplete = ( event, state ) => {

				let parent = getTargetViewFromName(state.name);

				delete transitioning[parent];
			};

			let getUiViewName = ( ...rest ) => getViewName($interpolate, ...rest);

			let isViewTransitioning = ( viewName ) => {

				if (viewName === '*') {
					return some(transitioning);
				}

				return transitioning[getTargetViewFromName(viewName)];
			};

			$rootScope.$on('$stateChangeStart', handleStart);

			$rootScope.$on('$stateChangeSuccess', handleComplete);
			$rootScope.$on('$stateChangeError', handleComplete);
			$rootScope.$on('$stateChangeCancel', handleComplete);

			return {
				getUiViewName,
				isViewTransitioning,
				getTargetViewFromName
			};

		}])
		.name;
