import angular from 'angular';

// ZS-TODO: this is a copy of /frontend/zaaksysteem/src/js/nl/mintlab/utils/filters/dateOrText.js
// Do **not** depend on more `/frontend` code in `/intern` (or the other way round).
export default angular
	.module('Zaaksysteem.dateOrTextFilter', [])
	.factory('dateOrTextFilter', ['$filter', function dateOrTextFilterFactory($filter) {
		const dateFilter = $filter('date');

		return function dateOrTextFilter(string) {
			const date = new Date(string);

			if (!isNaN(date.getTime())) {
				return dateFilter.apply(this, arguments);
			}

			return string;
		};
	}])
	.name;
