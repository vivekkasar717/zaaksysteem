import angular from 'angular';
import isArray from 'lodash/isArray';
import last from 'lodash/last';
 
export default
	angular.module('vorm.vormInvoke', [])
		.factory('vormInvoke', [ '$injector', '$parse', ( $injector, $parse ) => {
			
			let invoke = ( invokable, locals ) => {

				let value;
				
				if (!invokable) {
					return invokable;
				}
				
				if ((isArray(invokable) && typeof last(invokable) === 'function') || invokable.$inject !== undefined) {
					value = $injector.invoke(invokable, null, locals);
				} else if (typeof invokable === 'function') {
					value = invokable();
				} else {
					value = invokable;
				}
				
				return value;
			};
			
			let invoker = ( invokable, locals ) => invoke(invokable, locals);
			
			invoker.expr = ( invokable, locals, scope ) => {

				let value;
				
				if (typeof invokable === 'string') {
					value = $parse(invokable)(scope, locals);
				} else {
					value = invoke(invokable, locals);
				}
				return value;
			};
			
			return invoker;
			
		}])
		.name;
