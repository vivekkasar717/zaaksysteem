import controller from './AppointmentDisplayController';
import template from './template.html';

controller.$inject = ['$scope', '$http', 'resource', 'composedReducer', 'dateFilter', 'snackbarService'];

export default {
	bindings: {
		value: '&',
		onRemove: '&',
		provider: '&'
	},
	controller,
	template
};
