import triggerDownload from './triggerDownload';

describe('the triggerDownload function', () => {
  test('uses MSHTML implementation if it exists', () => {
    const originalMsSaveOrOpenBlob = window.navigator.msSaveOrOpenBlob;

    window.navigator.msSaveOrOpenBlob = jest.fn();
    triggerDownload('response', 'filename');

    expect(window.navigator.msSaveOrOpenBlob).toHaveBeenCalledTimes(1);

    window.navigator.msSaveOrOpenBlob = originalMsSaveOrOpenBlob;
  });

  test('does not persist DOM mutations', () => {
    const originalInnerHTML = document.body.innerHTML;

    triggerDownload('response', 'filename');

    expect(document.body.innerHTML).toBe(originalInnerHTML);
  });

  test('creates and revokes an object URL', () => {
    const originalURL = window.URL;

    window.URL.createObjectURL = jest.fn();
    window.URL.revokeObjectURL = jest.fn();

    triggerDownload('response', 'filename');

    expect(window.URL.createObjectURL).toHaveBeenCalledTimes(1);
    expect(window.URL.revokeObjectURL).toHaveBeenCalledTimes(1);

    window.URL = originalURL;
  });
});
