// from angular-ui-router
export default ( $interpolate, scope, element, attrs ) => {
	let name = $interpolate(attrs.uiView || attrs.name || '')(scope),
		inherited = element.inheritedData('$uiView');

	return name.indexOf('@') >= 0 ? name : (`${name}@${(inherited ? inherited.state.name : '')}`);
};
