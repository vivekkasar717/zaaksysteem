import { formatZipcode, NBSP } from './format';

describe('The `formatZipcode` function', () => {
	test('inserts a non breaking space if no space is present', () => {
		expect(formatZipcode('1234AA')).toBe(`1234${NBSP}AA`);
	});

	test('replaces a plain space character with a non breaking space', () => {
		expect(formatZipcode('1234 AA')).toBe(`1234${NBSP}AA`);
	});

	test('does not mutate already formatted data', () => {
		expect(formatZipcode(`1234${NBSP}AA`)).toBe(`1234${NBSP}AA`);
	});
});
