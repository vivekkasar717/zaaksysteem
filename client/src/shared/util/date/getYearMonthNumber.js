/**
 * Get a Number representation of year and month for easy comparison.
 *
 * @param {Date} date
 * @return {Number}
 */
const getYearMonthNumber = date => ((date.getFullYear() * 100) + (date.getMonth() + 1));

export default getYearMonthNumber;
