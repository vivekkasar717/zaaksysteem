import mapValues from 'lodash/mapValues';
import pickBy from 'lodash/pickBy';
export default ( instance ) => {

	return {
		validations:
			mapValues(
				pickBy(instance, ( value ) => value.validity !== 'valid'),
				( value ) => {

					return [
						{
							[value.validity]: value.message
						}
					];
				}
			),
		valid: false
	};

};
