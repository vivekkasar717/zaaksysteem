import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import appServiceModule from './../../shared/appService';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import resourceModule from '../../../shared/api/resource';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';

import controller from './MeetingListViewController';
import template from './template.html';
import './styles.scss';

export default angular
	.module('Zaaksysteem.meeting.meetingListView', [
		angularUiRouterModule,
		appServiceModule,
		composedReducerModule,
		resourceModule,
		snackbarServiceModule
	])
	.component('meetingListView', {
		bindings: {
			meetingResource: '&',
			appConfig: '&'
		},
		controller,
		template
	})
	.name;
