import first from 'lodash/head';
import mapValues from 'lodash/mapValues';

export default proposal => {
	let flattened = proposal.merge(
		{ instance:
			{ attributes:
				mapValues(proposal.instance.attributes, ( attrValue ) => {
					return first(attrValue);
				})
			}
		},
		{ deep: true }
	);

	return flattened;
};
