import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import template from './template.html';
import './styles.scss';

export default
	angular.module('Zaaksysteem.officeaddins.word.wordApp', [
		angularUiRouterModule
	])
		.directive('wordApp', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					appConfig: '&'
				}
			};

		}])
		.name;
