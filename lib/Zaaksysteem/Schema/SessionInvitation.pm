use utf8;
package Zaaksysteem::Schema::SessionInvitation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SessionInvitation

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<session_invitation>

=cut

__PACKAGE__->table("session_invitation");

=head1 ACCESSORS

=head2 id

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 subject_id

  data_type: 'uuid'
  is_foreign_key: 1
  is_nullable: 0
  size: 16

=head2 object_id

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 object_type

  data_type: 'text'
  is_nullable: 1

=head2 date_created

  data_type: 'timestamp'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}
  timezone: 'UTC'

=head2 date_expires

  data_type: 'timestamp'
  is_nullable: 0
  timezone: 'UTC'

=head2 token

  data_type: 'text'
  is_nullable: 0

=head2 action_path

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "subject_id",
  { data_type => "uuid", is_foreign_key => 1, is_nullable => 0, size => 16 },
  "object_id",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "object_type",
  { data_type => "text", is_nullable => 1 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
    timezone      => "UTC",
  },
  "date_expires",
  { data_type => "timestamp", is_nullable => 0, timezone => "UTC" },
  "token",
  { data_type => "text", is_nullable => 0 },
  "action_path",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 subject_id

Type: belongs_to

Related object: L<Zaaksysteem::Schema::Subject>

=cut

__PACKAGE__->belongs_to(
  "subject_id",
  "Zaaksysteem::Schema::Subject",
  { uuid => "subject_id" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-10-12 07:58:57
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:8iikzmdfMcamL1v4aNwfTw

__PACKAGE__->resultset_class('Zaaksysteem::Backend::Session::Invitation::ResultSet');

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
