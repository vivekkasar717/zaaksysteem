package Zaaksysteem::StUF::0301::NatuurlijkPersoon;

use Moose::Role;

=head1 NAME

Zaaksysteem::StUF::0301::NatuurlijkPersoon - Handling of Natuurlijk Personen below the stuf 0301 definition

=head1 SYNOPSIS


=head1 DESCRIPTION

StUF 0301 related calls for handling of Natuurlijk Personen, an improvement of Zaaksysteem::StUF.

=head1 ATTRIBUTES

See L<Zaaksysteem::XML::Compile::Instance> for a description of Required attributes

=cut

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

