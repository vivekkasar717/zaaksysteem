package Zaaksysteem::Controller::Zaaktype::Category;

use Moose;

use DateTime;

BEGIN { extends 'Zaaksysteem::Controller' }

my $CATEGORY_FIELDS = {
    'categorie'      => 'categorie',
    'eigenaar'       => 'eigenaar',
    'behandelaar'    => 'behandelaar',
};



sub index : Chained('/zaaktype/base'): PathPart('categorie'): Args(0) {
    my ($self, $c) = @_;

    ### Load categories
    $c->stash->{categorien} = $c->model('DB::ZaaktypeCategorie')->search(
        {
            deleted_on  => undef,
            pid         => undef,
        },
        {
            order_by    => 'categorie'
        }
    );

    $c->stash->{template} = 'zaaktype/categorie/index.tt';
}

sub zaaktypes : Chained('/zaaktype/base'): PathPart('categorie'): Args(1) {
    my ($self, $c, $id) = @_;

    ### Load categorie
    $c->stash->{categorie}  = $c->model('DB::ZaaktypeCategorie')->find($id);

    unless ($c->stash->{categorie}) {
        $c->res->redirect($c->uri_for('/zaaktype'));
        $c->detach;
    }

    $c->add_trail(
        {
            uri     => $c->uri_for('/zaaktype/categorie/' .  $c->stash->{categorie}->id),
            label   => 'Categorie: ' . $c->stash->{categorie}->categorie,
        }
    );

    ### Load zaaktypes with categorie
    ### XXX TODO, Move backward
    $c->stash->{zaaktypen}  = $c->stash->{categorie}->zaaktypes->search(
        {
            deleted => undef
        },
    );

    $c->stash->{template}   = 'zaaktype/index.tt';

}

sub edit : Chained('/zaaktype/base'): PathPart('categorie/edit'): Args() {
    my ($self, $c, $id) = @_;
    my ($categorie);

    $c->stash->{nowrapper} = 1;
    $c->stash->{template} = 'zaaktype/categorie/edit.tt';

    if (%{ $c->req->params } && $c->req->params->{action}) {

        my %args = map { $_ => $c->req->params->{ $CATEGORY_FIELDS->{ $_ } } }
            keys %{ $CATEGORY_FIELDS };

        $args{pid} = $c->req->params->{parent_id} || undef;

        if (!$id) {
            $categorie = $c->model('DB::ZaaktypeCategorie')->create(
                \%args
            );
        } else {
            $categorie = $c->model('DB::ZaaktypeCategorie')->find($id);
            $categorie->$_($c->req->params->{ $CATEGORY_FIELDS->{ $_ } })
                for keys %{ $CATEGORY_FIELDS };

            $categorie->update;
        }

        $c->push_flash_message(
            'Zaaktype categorie ' . $categorie->categorie
            . ' succesvol bijgewerkt'
        );
        $c->res->redirect( $c->uri_for('/zaaktype/categorie/' . $categorie->id) );
        $c->detach;
    } elsif ($id) {
        $c->stash->{categorie} = $c->model('DB::ZaaktypeCategorie')->find($id);
    }
}

sub delete : Chained('/zaaktype/base'): PathPart('categorie/delete'): Args(1) {
    my ($self, $c, $id) = @_;

    my $categorie   = $c->model('DB::ZaaktypeCategorie')->find($id);

    ### Find zaaktype
    my $zt_nodes    = $categorie->zaaktypes->search(
        {
            deleted => undef
        }
    );

    if (
        %{ $c->req->params } &&
        $c->req->params->{confirmed}
    ) {
        if (
            !$zt_nodes->count
        ) {
            $categorie->deleted_on(DateTime->now);
            if ($categorie->update) {
                $c->push_flash_message(
                    'Zaaktype categorie ' . $categorie->categorie
                    . ' succesvol verwijderd'
                );
            } else {
                $c->push_flash_message(
                    'Helaas kan deze Zaaktype categorie'
                    . ' om onbekende reden niet verwijderd worden.'
                );
            }
        }

        $c->res->redirect( $c->uri_for('/zaaktype') );
        $c->detach;
    }

    if ($zt_nodes->count) {
        $c->stash->{confirmation}->{message}    =
            'Helaas kan dit zaaktype niet worden verwijderd. Er zijn'
            . ' nog zaaktypen actief.';
    } else {
        $c->stash->{confirmation}->{message}    =
            'Weet u zeker dat u deze categorie wilt verwijderen?'
    }

    $c->stash->{confirmation}->{type}       = 'yesno';

    $c->stash->{confirmation}->{params}     = {
        'zaaktype_id'   => $id
    };

    $c->stash->{confirmation}->{uri}        =
        $c->uri_for(
            '/zaaktype/categorie/delete/' . $id
        );

    $c->forward('/page/confirmation');
    $c->detach;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 delete

TODO: Fix the POD

=cut

=head2 edit

TODO: Fix the POD

=cut

=head2 zaaktypes

TODO: Fix the POD

=head2 index

TODO: Fix the POD

=cut

