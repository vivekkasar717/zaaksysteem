package Zaaksysteem::Controller::API::Case::Results;

use Moose;

use Zaaksysteem::Constants qw[ZAAKSYSTEEM_OPTIONS];

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::Case::Results - Retrieve case result strings

=head1 SYNOPSIS

This package provides a single action that allows an API consumer to retrieve
all 'Resultaattypen', directly from the L<Zaaksysteem::Constants> definitons.

=head1 ACTIONS

=head2 results

This action returns a ZAPI hydrated list of all resulttypes Zaaksysteem
defines in L<Zaaksysteem::Constants>. In order to get the full listing do not
forget to set the C<zapi_no_pager> request parameter!

=head3 URL

C</api/case/results>

=head3 Example

    {
        "next" : "https://dev.poly1/api/case/results?zapi_page=2",
        "status_code" : "200",
        "prev" : null,
        "num_rows" : 54,
        "rows" : 10,
        "comment" : null,
        "at" : null,
        "result" : [
            "aangekocht",
            "aangehouden",
            "aangesteld",
            "aanvaard",
            "afgeboekt",
            "afgebroken",
            "afgehandeld",
            "afgesloten",
            "afgewezen",
            "akkoord"
        ]
    }

=cut

sub results : Chained('/api/case/caseless_base') : PathPart('results') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = [
        map { { label => ucfirst $_, value => $_ } } @{ ZAAKSYSTEEM_OPTIONS->{ RESULTAATTYPEN } }
    ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 ZAAKSYSTEEM_OPTIONS

TODO: Fix the POD

=cut

