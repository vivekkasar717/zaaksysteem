package Zaaksysteem::Controller::API::ExternKoppelprofiel;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

=head1 NAME

Zaaksysteem::Controller::API::ExternKoppelprofiel - Controller for public API calls

=head1 DESCRIPTION

=head1 ACTIONS

=head2 base

This I<base> action will validate the incoming call by authenticating the user
via HTTP-Digest, retrieving the referenced interface and checking the call
to be for the correct object type.

=cut

sub base : Chained('/'): PathPart('api/public'): CaptureArgs(2) {
    my ($self, $c, $interface_id, $type) = @_;

    if (!$interface_id || !$type) {
        throw('api/public/missing_parameters', "Missing parameters");
    }

    my $interface = $c->model('DB::Interface')->search_active({ id => $interface_id })->first;
    my $user;

    # People not need to know about other interfaces, so they can guess and whatnot
    if (!$interface || $interface->module ne 'externe_koppeling') {
        throw(
            "/api/public/interface/inactive_or_missing",
            'Extern koppelprofiel koppeling is niet beschikbaar of inactief'
        );
    }

    $user = $c->authenticate({ interface => $interface }, 'api');
    if (!$user) {
        throw(
            "/api/public/$interface_id/$type/authentication_failed",
            "Digest authentication failed, check your credentials!"
        );
    }

    unless(grep { $_->{ object_type } eq $type } @{ $interface->get_interface_config->{ objecttypes } }) {
        throw(
            "/api/public/$interface_id/$type/permission",
            "Not authorized to query '$type'"
        );
    }

    $c->stash->{ object_type } = $type;
}

=head2 list

This is the main action for external callers that want to retrieve objects
from Zaaksysteem.

=head3 URL

C</api/public/[INTERFACE_ID]/[OBJECT_TYPE]>

=head3 Response

The response will be wrapped in a L<Zaaksysteem::ZAPI::Response>
datastructure. The items serialize according to their own
L<OA|Zaaksysteem::Metarole::ObjectAttribute> definitions, via the
L<Zaaksysteem::Object/TO_JSON> infrastructure.

=cut

sub list : Chained('base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{ zapi } = $c->model('Object')->search($c->stash->{ object_type }, {});
}

=head2 get_by_uuid

This action consumes an additional URI path parameter, which will be
interpreted as a UUID. The response will be either a
L<Zaaksysteem::ZAPI::Response>-wrapped L<Zaaksysteem::Object> serialization,
or a HTTP 404 indicating the object does not exist.

Extra guards are put in place such that the object's type is also verified
to be allowed.

=head3 URL

C</api/public/[INTERFACE_ID]/[OBJECT_TYPE]/[OBJECT_UUID]>

=cut

sub get_by_uuid : Chained('base') : PathPart('') : Args(1) {
    my ($self, $c, $uuid) = @_;

    my $object = $c->model('Object')->retrieve(uuid => $uuid);

    if ($object->type ne $c->stash->{ object_type }) {
        $c->stash->{zapi} = [];
    }
    else {
        $c->stash->{zapi} = [ $object ];
    }

}

=head1 HELPER FUNCTIONS

=head2 throw

This helper function exists because throwing traditional exceptions does not
have the desired (C<die>) effect.

=cut

sub throw {
    die(sprintf"%s: %s\n", @_);
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
