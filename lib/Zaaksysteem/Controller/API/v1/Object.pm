package Zaaksysteem::Controller::API::v1::Object;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Object - APIv1 generic object retrieval

=head1 DESCRIPTION

This controller handles C<api/v1/[OBJECT_TYPE_NAME]/[UUID]> object requests.

=cut

use BTTW::Tools;
use Zaaksysteem::Object::Reference::Instance;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 instance_base

Reserves the C</api/v1/[OBJECT_TYPE_NAME]/[UUID]> routing namespace.

=cut

sub instance_base : Chained('/api/v1/base') : PathPart('') : CaptureArgs(2) {
    my ($self, $c, $type, $uuid) = @_;

    my $ref = Zaaksysteem::Object::Reference::Instance->new(
        type => $type,
        id   => $uuid,
    );

    my $object = $c->model('Object')->retrieve(ref => $ref);

    unless (defined $object) {
        throw('api/v1/object_not_found', sprintf(
            'No object of type "%s" found with id "%s" - have you checked your privilege?',
            $type,
            $uuid
        ));
    }

    $c->stash->{object} = $object
}

=head2 get

Retrieve a single object instance, given its type and UUID.

=head3 URL path

C</api/v1/[OBJECTTYPE_NAME]/[UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    $c->stash->{result} = $c->stash->{object};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
