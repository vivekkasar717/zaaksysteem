package Zaaksysteem::Object::Query::Expression::Field;

use Moose;

use BTTW::Tools;

with 'Zaaksysteem::Object::Query::Expression';

=head1 NAME

Zaaksysteem::Object::Query::Expression::Field - Object attribute reference
expression

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 name

Name of the field instances of this expression class reference.

=cut

has name => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Zaaksysteem::Object::Query::Expression/stringify>.

    # "field:my_field"
    qb_field('my_field')->stringify

=cut

sub stringify {
    my $self = shift;

    return sprintf('field:%s', $self->name);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
