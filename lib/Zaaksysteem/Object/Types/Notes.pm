package Zaaksysteem::Object::Types::Notes;
use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Object::Types::Notes

=cut

extends 'Zaaksysteem::Object';

with 'Zaaksysteem::Object::Roles::Security';

use Zaaksysteem::Types qw(NonEmptyStr);

=head1 DESCRIPTION

Here be dragons

=head1 ATTRIBUTES

=cut

=head2 content

Content of the note

=cut

has content => (
    is       => 'rw',
    isa      => 'Str',
    label    => 'Naam',
    traits   => [qw[OA]],
    required => 1
);

=head2 data

The creator/owner of this comment

=cut

has event_type => (
    is       => 'ro',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Notes type ID',
    required => 1,
);

=head2 description

The description of the event.

=cut

has description => (
    is     => 'rw',
    isa    => 'Str',
    traits => [qw(OA)],
    label  => 'Description of the event',
);

=head2 event

A reference to the actual even type

=cut

has event => (
    is     => 'ro',
    traits => [qw(OR)],
    label  => 'Notes object',
    embed  => 1,
);

=head2 created_by

Created by L<Zaaksysteem::Object::Types::Subject>

=cut

has created_by => (
    is     => 'ro',
    isa    => 'Zaaksysteem::Object::Types::Subject',
    traits => [qw(OA)],
    label  => 'Created by',
);

__PACKAGE__->meta->make_immutable;

__END__


=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
