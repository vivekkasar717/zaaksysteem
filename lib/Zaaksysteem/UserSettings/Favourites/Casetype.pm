package Zaaksysteem::UserSettings::Favourites::Casetype;

use Moose::Role;

use constant FAVTYPE => 'casetype';

before '_load_allowed_reference_types' => sub {
    my $self        = shift;

    push (@{ $self->_allowed_reference_types }, FAVTYPE);
};

around '_inflate_from_settings' => sub {
    my $method          = shift;
    my $self            = shift;
    my ($setting)       = shift;

    next unless ref $setting eq 'ARRAY';

    ### Let's add the proper labels to all settings
    my %casetype_ids;

    for (my $i = 0; $i < scalar(@$setting); $i++) {
        my $row = $setting->[$i];

        next unless ref $row eq 'HASH';
        next unless ($row->{reference_type} eq FAVTYPE);

        ### Grep all casetype IDS
        $casetype_ids{$row->{reference_id}} = $i;
    }

    my %map = map {
        $_->object_id => $_->uuid
    } $self->schema->resultset('ObjectData')->search(
        {
            'uuid'          => { -in => [ keys %casetype_ids ] },
            'object_class'  => FAVTYPE,
        },
        {
            select          => [qw/uuid object_id/],
            as              => [qw/uuid object_id/],
        }
    )->all;

    if (keys %map) {
        my @zaaktypen   = $self->schema->resultset('Zaaktype')->search(
            {
                'me.id'  => { -in => [ keys %map ] }
            },
            {
                prefetch    => 'zaaktype_node_id',
            }
        )->all;

        for my $zaaktype (@zaaktypen) {
            my $uuid        = $map{ $zaaktype->id };
            my $pointer     = $casetype_ids{ $uuid };

            next unless defined $pointer;
            $setting->[$pointer]->{label} = $zaaktype->zaaktype_node_id->titel;
        }
    }

    return $self->$method($setting, @_);
};

around '_prepare_row_params' => sub {
    my $method          = shift;
    my $self            = shift;
    my ($params, $row)  = @_;

    my $uuid            = ($row ? $row->reference_id : $params->{reference_id});

    if ($params->{reference_type} && $params->{reference_type} eq FAVTYPE) {
        my $object = $self->schema->resultset('ObjectData')->search(
            {
                'uuid'          => $uuid,
                'object_class'  => FAVTYPE,
            },
            {
                select          => [qw/uuid object_id/],
                as              => [qw/uuid object_id/],
            }
        )->first;

        if ($object) {
            my $zaaktype = $self->schema->resultset('Zaaktype')->search(
                {
                    'me.id'  => $object->object_id,
                },
                {
                    prefetch    => 'zaaktype_node_id',
                }
            )->first;

            $params->{label} = $zaaktype->zaaktype_node_id->titel if $zaaktype;
        }
    }

    $self->$method(@_);
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
