package Zaaksysteem::Zorginstituut::Roles::SendMessage;
use Moose::Role;

=head1 NAME

Zaaksysteem::Zorginstituut::Roles::SendMessage - A role for sending Zorginstituut messages

=head1 DESCRIPTION

Defines that users of this role must implement the following functions:

=over

=item _build_encoder

=item encode

Encode 

=back

=cut

use BTTW::Tools;

requires qw(
   encode
    _build_encoder
);

sig encode => '=> Str';

=head1 ATTTRIBUTES

=head2 encoder

The encoder used by this message. Consumers must implement C<_build_encoder>.

=cut

has encoder => (
    is       => 'ro',
    isa      => 'Any',
    builder  => '_build_encoder',
    lazy     => 1,
);

=head2 message_version

Message version, consumers must define this attribute

=cut

has message_version => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 message_version

Message sub version, consumers must define this attribute

=cut

has message_sub_version => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 case

A L<Zaaksysteem::Schema::Zaak> object, required.

=cut

has case => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema::Zaak',
    required => 1,
)
;
=head2 code

The message code, required.

=cut

has code => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 generate_headers

    my %headers = $self->generate_headers(
        # required
        berichtcode => '132',
        ontvanger => '1234',
        afzender => '2345',

        # optional
        date => DateTime->now(),
    );

Returns a hash in list context

=cut

define_profile generate_headers => (
    required => {
        ontvanger     => 'Int',
        afzender      => 'Int',
    },
    optional => { date => 'DateTime', },
    defaults => { date => sub { return DateTime->now() } },
);

sub generate_headers {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    return (
        Header => {
            Berichtspecificatie => {
                Code      => $self->code,
                Versie    => $self->message_version,
                SubVersie => $self->message_sub_version,
            },
            Afzender             => $opts->{afzender},
            Ontvanger            => $opts->{ontvanger},
            Berichtidentificatie => {
                Identificatie => $self->generate_identification,
                Dagtekening   => $opts->{date},
            },
        },
    );
}

=head2 generate_identification

Creates a new serial ID.

=cut

sub generate_identification {
    my $self = shift;

    return $self->schema->storage->dbh_do(
        sub {
            my ($storage, $dbh, @args) = @_;

            my $sth = $dbh->prepare(q{
                SELECT nextval('zorginstituut_identificatie_seq');
            });
            $sth->execute();
            my @result = $sth->fetchrow_array;
            return $result[0];
        }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
