package Zaaksysteem::Zaken::Concept;
use Moose;
use namespace::autoclean;

use BTTW::Tools;
use JSON::XS;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Zaken::Concept - A concept case model

=head1 DESCRIPTION

Create and update concept cases

=head1 SYNOPSIS

    # TODO: Fix me

=head1 ATTRIBUTES

=head2 concept_rs

The Zaaktype Relation resultset

=cut

has concept_rs => (
    is       => 'ro',
    isa      => 'Defined',
    required => 1,
);

=head2 json

JSON encoded/decoder for concept cases

=cut

has json => (
    is      => 'ro',
    isa     => 'JSON::XS',
    default => sub {
        return JSON::XS->new->allow_nonref->allow_blessed->convert_blessed;
    },
);

=head2 find_concept_case

Find the concept case from the resultset based on the casetype ID and returns the json data

=cut

sig find_concept_case => 'Int, Str';

sub find_concept_case {
    my ($self, $casetype_id, $betrokkene) = @_;

    my $case = $self->concept_rs->search_rs({
        zaaktype_id => $casetype_id,
        betrokkene  => $betrokkene,
    })->first;
    return $case if $case;
    return;
}

=head2 get_concept_case_data

Get the data from the concept case

=cut

sig get_concept_case_data => 'Int, Str';

sub get_concept_case_data {
    my $self = shift;

    my $concept = $self->find_concept_case(@_);

    return $self->json->decode($concept->json_string) if $concept;
    return;
}

=head2 delete_concept_case

Delete a concept case for a betrokkene

=cut

sig delete_concept_case => 'Int, Str';

sub delete_concept_case {
    my ($self, $casetype_id, $betrokkene) = @_;
    $self->concept_rs->search_rs({
        zaaktype_id => $casetype_id,
        betrokkene  => $betrokkene,
    })->delete;
    return;
}

=head2 submit_concept_case

Submit concept cases to the database

=cut

sig submit_concept_case => 'HashRef';

sub submit_concept_case {
    my ($self, $zaak_create) = @_;

    my $json = try {
        return $self->json->encode($zaak_create);
    }
    catch {
        $self->log->warn(sprintf("Error encoding '%s' to JSON: %s", dump_terse($zaak_create), $_));
        throw('case/concept/json_encoder', "JSON encoding error");
    };

    return $self->concept_rs->update_or_create({
        zaaktype_id     => $zaak_create->{zaaktype_id},
        betrokkene      => $zaak_create->{ztc_aanvrager_id},
        json_string     => $json,
        afronden        => 0,
        create_unixtime => DateTime->now()->epoch(),
    });
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
