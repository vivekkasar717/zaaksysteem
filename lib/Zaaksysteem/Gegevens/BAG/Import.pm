package Zaaksysteem::Gegevens::BAG::Import;

use Moose::Role;
use Data::Dumper;

use File::Spec::Functions qw/catfile/;

use File::Basename qw/basename/;

has 'customer_short_name' => (
    is      => 'rw',
    isa     => 'Str',
);

has 'import_home_directory' => (
    is      => 'rw',
    isa     => 'Str',
);


sub import_start {
    my $self        = shift;

    $self->_load_config_from_interface or return;

    my $import      = $self->_load_iterator;

    $import->run;
}

sub _load_iterator {
    my $self        = shift;

    die('Could not find import class') unless
        $self->config->{import_class};

    my $package     = __PACKAGE__ . '::'
        . $self->config->{import_class};

    eval "use $package";

    if ($@) {
        die('Error loading package: ' . $package . ':' . $@);
    }

    my $object      = $package->new(
        dbic    => $self->dbic,
        config  => $self->config,
        prod    => $self->prod,
        'log'   => $self->log,
    );

    return $object;
}

sub _load_config_from_interface {
    my $self        = shift;

    my $interface   = $self->dbic->resultset('Interface')->search_active({ module => 'importplugin' })->first;

    unless ($interface) {
        $self->log->error('Cannot find active interface: importplugin');
        return;
    }

    unless (
        $interface->get_interface_config->{bag_enabled} &&
        $interface->get_interface_config->{bag_module} &&
        $interface->get_interface_config->{bag_filename}
    ) {
        $self->log->error('Configuration for importplugin/bag not filled correctly');
        return;   
    }

    my $config = $self->{config} // {};

    $config->{import_class}   = $interface->get_interface_config->{bag_module};
    $config->{filename}       = catfile(
        $self->import_home_directory,
        $self->customer_short_name,
        'import',
        $interface->get_interface_config->{bag_filename}
    );

    $self->config($config);

    $self->log->info('Trying to load for BAG parsing: ' . $self->config->{filename});

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 import_start

TODO: Fix the POD

=cut

