=head1 NAME

Zaaksysteem::Manual::API::V1::Conversation - Creating a conversation item

=head1 Description

This API-document describes the usage of our JSON Conversation API. Via the Conversation API it is possible
to create and list conversations (called "contactmomenten" in dutch).

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/conversation

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head1 Mutate data

Mutations via our API will have to be send via the HTTP C<POST> method. The inputdata for these mutations
are one single JSON object containing the parameters. When no input is needed, make sure you send an empty
object (C< {} >)

=head2 create

   /api/v1/conversation/create

It is possible to create a conversation in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over

=item message

Type: Text

Returns the message of the contactmoment. Could also contain the contents of C<email_body>

=item type

Type: ENUM (C<note> or C<email>)

Returns the type of this contactmoment

=item medium

Type: ENUM (balie, telefoon, post, email, webformulier, behandelaar, sociale media)

The medium this contactmoment came in.

=item subject_id

Type: Internal Betrokkene Identifier

Returns the internal representation of a betrokkene in zaaksysteem

=back

B<Example call>

 curl --anyauth -H "Content-Type: application/json" --data @file_with_request_json --digest -u "username:password" https://localhost/api/v1/conversation/create

B<Request JSON JSON>

=begin javascript

{
   "medium" : "behandelaar",
   "message" : "Test message\nwith a break",
   "subject_id" : "betrokkene-natuurlijk_persoon-3",
   "type" : "note"
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-9329a8-435553",
   "development" : false,
   "result" : {
      "instance" : {
         "case_id" : null,
         "created_by" : "betrokkene-medewerker-1",
         "date_created" : "2015-12-01 14:33:46.043949",
         "email_bcc" : null,
         "email_body" : null,
         "email_cc" : null,
         "email_recipient" : null,
         "email_subject" : null,
         "medium" : "behandelaar",
         "message" : "Test message\nwith a break",
         "subject_id" : "betrokkene-natuurlijk_persoon-3",
         "type" : "note"
      },
      "reference" : "4be64660-c03d-4cee-b52c-e79f2cd72ff0",
      "type" : "contactmoment"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 Contactmoment

Most of the calls in this document return an instance of type C<contactmoment>. Below we provide more information about
the contents of this object.

B<Properties>

=over

=item case_id

Type: Num

A numeric identifier to the case id (table id) related to this contactmoment

=item created_by

Type: Internal Betrokkene Identifier

Returns the internal representation of a betrokkene in zaaksysteem

=item subject_id

Type: Internal Betrokkene Identifier

Returns the internal representation of a betrokkene in zaaksysteem

=item date_created

Type: Timestamp

Returns the date when this contactmoment was created

=item email_bcc

Type: List of emailaddresses

Returns a list of BCC emailadresses this contactmoment was sent to

=item email_bcc

Type: List of emailaddresses

Returns a list of BCC emailadresses this contactmoment was sent to

=item email_cc

Type: List of emailaddresses

Returns a list of CC emailadresses this contactmoment was sent to

=item email_recipient

Type: List of emailaddresses

Returns a list of emailadresses this contactmoment was sent to

=item email_subject

Type: Text

Subject of the e-mail

=item email_body

Type: Text

Returns the body of the e-mail, same as C<message>

=item message

Type: Text

Returns the message of the contactmoment. Could also contain the contents of C<email_body>

=item type

Type: ENUM (C<note> or C<email>)

Returns the type of this contactmoment

=item medium

Type: ENUM (balie, telefoon, post, email, webformulier, behandelaar, sociale media)

The medium this contactmoment came in.

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Subject>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
