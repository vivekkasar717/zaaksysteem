package Zaaksysteem::Transaction;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Transaction - Model for transactions (interface logs)

=head1 DESCRIPTION

This model handles interaction with transactions, transaction records and
mappnigs of transaction records to objects.

Note that this doesn't mean I<database> transactions. It's about transactions
between Zaaksysteem and other systems.

=head1 SYNOPSIS

    my $model = Zaaksysteem::Transaction->new(
        schema => $db,
        maximum_age_days => 365,
        maximum_rows => 365,
        dry => 1,
    );
    $model->clean_old_transactions();

    my $model = $c->model('Transaction');
    $model->clean_old_transactions()

=cut

=head1 ATTRIBUTES

=head2 schema

The database object (L<Zaaksysteem::Schema> instance) to use.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 maximum_age_days

The maximum age of the transaction in days. Defaults to 120.

=cut

has maximum_age_days => (
    is      => 'ro',
    isa     => 'Int',
    default => '120',
);

=head2 maximum_rows

The maximum rows deleted in one run. Defaults to 50

=cut

has maximum_rows => (
    is      => 'ro',
    isa     => 'Int',
    default => '50',
);

=head2 enabled

Enable the cleanup run method

=cut

has enabled => (
    is      => 'ro',
    isa     => 'Bool',
    lazy    => 1,
    builder => '_build_enabled',
);

sub _build_enabled {
    return $ENV{ZS_FEATURE_FLAG_TRANSACTION_CLEANUP} ? 1 : 0;
}

=head2 cleanup_old_transactions

Removes transactions older than the configured maximum age.

=cut

sub cleanup_old_transactions {
    my $self = shift;

    return unless $self->enabled;

    my $interval = sprintf(
        "(CURRENT_DATE - '%d days'::INTERVAL)",
        $self->maximum_age_days
    );

    $self->schema->txn_do(sub {
        my $transactions = $self->schema->resultset('Transaction')->search(
            {
                '-or' => [
                    { 'date_created::DATE' => { '<' => \$interval } },
                    { 'date_deleted' => { '!=' => undef }},
                ]
            },
            {
                columns => 'id',
                rows => $self->maximum_rows,
            }
        );
        $self->log->debug(sprintf("Deleting %d transactions", $transactions->count));
        $transactions->delete;
    });

    return;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
