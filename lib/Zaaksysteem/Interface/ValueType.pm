package Zaaksysteem::Interface::ValueType;

use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Interface::ValueType - Common API for L<Zaaksysteem::Object>
values.

=head1 DESCRIPTION

This interface specifies required methods for object value types.

=cut

use BTTW::Tools;
use Moose::Util::TypeConstraints qw[role_type];

=head1 REQUIRED INTERFACES

=head2 name

Requires a static name method.

=cut

requires qw[
    name
];

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
