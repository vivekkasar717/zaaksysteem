package Zaaksysteem::Search::ZQL::Literal::JPath;

use Moose;

use JSON::Path;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'JSON::Path' );

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;
    my %param = @_;

    $class->$orig(
        value => JSON::Path->new($param{ value })
    );
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

