package Zaaksysteem::Model::Zaken;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Zaken',
    constructor => 'new',
);


sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        'log'       => $c->log,
        'prod'      => (
            $c->config->{otap} eq 'prod'
                ? 1
                : undef
        ),
        'dbic'      => $c->model('DB'),
        'config'    => $c->config->{'Zaken'},
        z_betrokkene => $c->model('Betrokkene'),
        z_current_user => $c->user,
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 prepare_arguments

TODO: Fix the POD

=cut

