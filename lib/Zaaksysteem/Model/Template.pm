package Zaaksysteem::Model::Template;

use base 'Exporter';

use Zaaksysteem::Model::Template::ODF;
use Zaaksysteem::Model::Template::Plaintext;

our @EXPORT = qw[instantiate_template];

sub instantiate_template {
    my $thing = shift;

    if(eval { $thing->isa('OpenOffice::OODoc::Document') }) {
        return Zaaksysteem::Model::Template::ODF->new($thing);
    }

    return Zaaksysteem::Model::Template::Plaintext->new($thing);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 instantiate_template

TODO: Fix the POD

=cut

