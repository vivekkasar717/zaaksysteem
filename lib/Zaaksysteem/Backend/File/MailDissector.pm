package Zaaksysteem::Backend::File::MailDissector;

use Moose::Role;
use Zaaksysteem::OutlookMailParser;
use Zaaksysteem::Email;

=head2 handle_accepted

Outlook emails will be converted to MIME.

MIME mails will be dissected into parts, these parts
will be added to the case in a folder.

=cut

sub handle_accepted {
    my ($self, $arguments) = @_;

    $self->unpack_email();
}

=head2 unpack_email

Unpack an email message into a folder full of attachments.

=cut

sub unpack_email {
    my $self = shift;

    my $filestore = $self->filestore_id();
    my $name = $filestore->original_name;

    if ($name !~ m/\.(msg|eml)$/i) {
        return;
    }

    my $path = $filestore->get_path();

    my $outlook = Zaaksysteem::OutlookMailParser->new();
    my $msg     = $outlook->process_outlook_message(
        { path => "$path", original_name => $name });

    return if !$msg;

    my $email = Zaaksysteem::Email->new(
        message => $msg,
        schema  => $self->result_source->schema
    );

    $email->add_to_case(
        $self->case,
        accept     => 1,
        created_by => $self->created_by,
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
