package Zaaksysteem::Backend::Component::Searchable;

use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Component::Searchable - Role providing searchable methods to a Component

=head1 SYNOPSIS


=head1 DESCRIPTION

These Component Role provides extends TO_JSON to provide searchable data when it
exists

=head1 ATTRIBUTES

=head2 _searchable_object_id [required]

ISA: Integer

Defines the id which identifies this search result, when giving it to
C<< $rs->find >>


Define this in your component class when you would like to use searchable

    has '_searchable_object_id' => (
        is      => 'ro',
    );

=head2 _searchable_object_label [required]

ISA: String

 print $self->_searchable_object_label;

 # "Testzaaktype"

Defines the human readable label for this search, for displaying in search result

Define this in your component class when you would like to use searchable

    has '_searchable_object_label' => (
        is      => 'ro',
    );

=head2 _searchable_object_description [required]

ISA: String

 print $self->_searchable_object_description;

 # "Testzaaktype voor gebruik binnen unit tests"

Defines the human readable description for this search, for displaying in search result

Define this in your component class when you would like to use searchable

    has '_searchable_object_description' => (
        is      => 'ro',
    );

=head1 EXTENDS

=head2 TO_JSON

 {
    searchable_object_id            => 4232,
    searchable_object_type          => 'zaaktype',
    searchable_object_label         => 'Testzaaktype',
    searchable_object_description   => 'Dit zaaktype wordt gebruikt voor bla',
 }

=cut

before 'TO_JSON'    => sub {
    my $self    = shift;

    return unless (
        $self->can('_searchable_object_id') &&
        $self->can('_searchable_object_label') &&
        $self->can('object_type')
    );

    ### Required parameters
    $self->_json_data->{searchable_object_id}       = $self->_searchable_object_id;
    $self->_json_data->{searchable_object_label}    = $self->_searchable_object_label;
    $self->_json_data->{searchable_object_type}     = $self->object_type;

    ### Optional description
    $self->_json_data->{searchable_object_description}  = $self->_searchable_object_description
        if $self->can('_searchable_object_description');
};

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::API>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
