package Zaaksysteem::Backend::Sysin::Modules::AuthTwoFactor;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::AuthTwoFactor - Non-government authentication for PIP/Form

=head1 DESCRIPTION

This interface module calls itself C<auth_twofactor>, and handles creation of
accounts, and authentication by means of username, password and an SMS token.

=cut

use Authen::Passphrase;
use Crypt::OpenSSL::Random qw(random_pseudo_bytes);
use DateTime;
use LWP::UserAgent;
use Math::Base36 qw(encode_base36);
use XML::LibXML;
use Zaaksysteem::Environment;
use BTTW::Tools;
use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::Backend::Sysin::Auth::Alternative;

my $PASSWORD_RESET_MINIMUM_INTERVAL = 300; # 300 seconds = 5 minutes

my $INTERFACE_ID = 'auth_twofactor';

my @INTERFACE_CONFIG_FIELDS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_allow_new_accounts',
        type        => 'checkbox',
        required    => 0,
        label       => 'Account aanmaken toestaan',
        description => '',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_twofactor_type',
        type        => 'select',
        required    => 1,
        label       => 'Tweede authenticatie-factor',
        data => {
            options => [
                { label => "Code via SMS",  value => "sms" },
            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_enable_for',
        type     => 'select',
        required => 1,
        label => 'Gebruik deze authenticatiemethode voor',
        data => {
            options => [
                { label => "Geen",                     value => "" },
                { label => "Personen",                 value => "persons" },
                { label => "Organisaties",             value => "companies" },
                { label => "Personen en organisaties", value => "both" },
            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_login_message',
        type        => 'richtext',
        required    => 0,
        label       => 'Introductietekst bij inloggen',
        data => {
            features => 'bold italic underline link bullet ordered'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_registration_message',
        type        => 'richtext',
        required    => 0,
        label       => 'Registratietoelichting',
        data => {
            features => 'bold italic underline link bullet ordered'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password_lost_message',
        type        => 'richtext',
        required    => 0,
        label       => 'Toelichting vergeten wachtwoord',
        data => {
            features => 'bold italic underline link bullet ordered'
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password_reset_template',
        type        => 'spot-enlighter',
        required    => 1,
        description => 'Mogelijke magicstrings: [[username]], [[email]], [[mobile]]',
        label       => 'Sjabloon voor wachtwoord-reset-email',
        data => {
            restrict   => 'notification',
            placeholer => 'Type uw zoekterm',
            label      => 'label',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_account_confirmation_template',
        type        => 'spot-enlighter',
        required    => 1,
        description => 'Mogelijke magicstrings: [[username]], [[email]], [[mobile]]',
        label       => 'Sjabloon voor bevestigingsemail',
        data => {
            restrict    => 'notification',
            placeholder => 'Type uw zoekterm',
            label       => 'label',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_account_activation_template',
        type        => 'spot-enlighter',
        required    => 1,
        description => 'Mogelijke magicstrings: [[username]], [[email]], [[mobile]], [[activation_link]]',
        label       => 'Sjabloon voor het verzenden van de activatielink',
        data => {
            restrict    => 'notification',
            placeholder => 'Type uw zoekterm',
            label       => 'label',
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sms_sender',
        type        => 'text',
        required    => 1,
        label       => 'SMS Afzender',
        description => 'Afzender voor SMS-berichten (maximaal 11 tekens)',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sms_product_token',
        type        => 'text',
        required    => 1,
        label       => 'SMS Product token',
        description => 'Product-token (zoals opgegeven door CM)',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_sms_endpoint',
        type        => 'text',
        required    => 1,
        label       => 'SMS Resource URL',
        description => 'URL (zoals opgegeven door CM) die gebruikt wordt om SMS te sturen',
        default     => 'https://sgw01.cm.nl/gateway.ashx',
    ),
);

my %MODULE_SETTINGS = (
    name             => $INTERFACE_ID,
    label            => 'Alternatieve gebruikersauthenticatie voor PIP en form',
    essential        => 0,
    interface_config => \@INTERFACE_CONFIG_FIELDS,
    direction        => 'incoming',
    manual_type      => [],
    is_multiple      => 0,
    is_manual        => 0,
    retry_on_error   => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    trigger_definition => {
        "check_password" => {
            method => "check_password",
            update => 1,
        },
        "change_password" => {
            method => "change_password",
            update => 1,
        },
        "verify_second_factor" => {
            method => "verify_second_factor",
            update => 1,
        },
        "activate_account" => {
            method => "activate_account",
            update => 1,
        },
        "link_betrokkene_id" => {
            method => "link_betrokkene_id",
            update => 1,
        },
        "lost_password_initialize" => {
            method => "lost_password_initialize",
            update => 1,
        },
        "send_sms" => {
            method => "send_sms",
            update => 1,
        },
    },
);

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig(%MODULE_SETTINGS);
};

=head1 METHODS

=cut

sub _change_password {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;
    my $schema      = $interface->result_source->schema;

    my $ue = $self->_find_userentity($interface->id, $schema, $params->{username});

    if (   (not exists $params->{password})
        || (not exists $params->{password_check})
        || ($params->{password} ne $params->{password_check})
    ) {
        $self->log->trace("Passwords don't match.");
        throw(
            "twofactor/password_mismatch",
            "Passwords don't match",
            { fatal => 1 },
        );
    }

    my $subject = $ue->subject_id;

    my $email_id = $interface->jpath('$.password_reset_template.id');
    my $template = $schema->resultset('BibliotheekNotificaties')->find($email_id);

    my $email = $template->send_mail(
        {
            to => $subject->properties->{initial_email_address},
            ztt_context => {
                username => $params->{username},
                email    => $subject->properties->{initial_email_address},
                mobile   => $subject->properties->{initial_phone_number},
            },
            request_id => $Zaaksysteem::Environment::REQUEST_ID,
        }
    );

    $record->preview_string("Wachtwoordwijziging voor $params->{username}");
    $record->output($email->as_string);

    $self->log->trace("Passwords match. Updating to '$params->{password}'");
    $subject->update_password(
        $ue,
        { password => $params->{password} }
    );

    # Remove clear-text password
    delete $params->{password};
    delete $params->{password_check};
    $transaction->processor_params($params);

    return {
        success => 1,
    };
}

sub _get_betrokkene {
    my ($self, $ue) = @_;
    my $subject = $ue->subject_id;

    my $schema = $ue->result_source->schema;

    my %search_options;
    if ($subject->subject_type eq 'person') {
        $search_options{burgerservicenummer} = $subject->properties->{bsn};
    }
    else {
        $search_options{dossiernummer}    = $subject->properties->{kvknummer};
        $search_options{vestigingsnummer} = $subject->properties->{vestigingsnummer};
    }

    my $betrokkene_rs = $schema->betrokkene_model->search(
        {
            type => ($subject->subject_type eq 'person')
                ? 'natuurlijk_persoon'
                : 'bedrijf',
            intern => 0,
        },
        \%search_options,
    );

    if ($betrokkene_rs) {
        return $betrokkene_rs->next;
    }

    return;
}

=head2 send_sms

Send a text message

=cut

# This should be split out to its own interface
sub send_sms {
    my ($self, $params, $interface) = @_;

    my $transaction = $interface->process(
        {
            external_transaction_id => 'n/a',
            input_data => "SMS to $params->{phonenumber}",

            processor_params => {
                %$params,
                processor => '_send_sms',
            }
        }
    );

    return $transaction;
}

sub _get_model {
    my ($self, $opts) = @_;

    my $interface = $opts->{interface};
    my $schema    = $interface->result_source->schema;
    my $config    = $interface->get_interface_config;

    my $model = Zaaksysteem::Backend::Sysin::Auth::Alternative->new(
        schema            => $schema,
        interface         => $interface,
        sms_sender        => $interface->jpath('$.sms_sender'),
        sms_endpoint      => $interface->jpath('$.sms_endpoint'),
        sms_product_token => $interface->jpath('$.sms_product_token'),
    );
    return $model;

}

sub _send_sms {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    my $model = $interface->model;

    my $number = $model->assert_mobile_phone_number($params->{phonenumber});

    my $xml= $model->create_sms_xml(
        phone_number => $number,
        message      => $params->{message},
        reference    => $transaction->id,
    );

    $model->send_sms(
        xml    => $xml,
        record => $record,
    );

    return 1;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015& 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
