package Zaaksysteem::Backend::Sysin::Modules::SDUConnect::PDC;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use BTTW::Tools;

use XML::XPath;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw[
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient
];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::SDUConnect::PDC - PDC object
synchronisation via SDU Connect

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 INTERFACE_ID

C<Str[sdu_connect_pdc]> - Claim interface module name

=cut

use constant INTERFACE_ID => 'sdu_connect_pdc';

=head2 INTERFACE_CONFIG_FIELDS

C<ArrayRef[Zaaksysteem::ZAPI::Form::Field]> - Collection of form fields for
the interface's main fieldset

=cut

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_url',
        type => 'text',
        label => 'REST API URL',
        required => 1,
        description => 'URL van de SDU Connect REST API'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_account_id',
        type => 'text',
        label => 'Account ID',
        required => 1,
        description => 'Door SDU uitgegeven Account ID'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_collection_id',
        type => 'text',
        label => 'Collection ID',
        required => 1,
        description => 'Collection ID van de te synchroniseren collectie producten'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_version',
        label => 'Delta API versie',
        type => 'select',
        required => 1,
        default => '1.0',
        data => {
            options => [
                { value => '1.0', label => 'v1.0' }
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_last_sync',
        label => 'Laatste synchronisatie',
        description => 'Datum en tijd van de laatste synchronisatie',
        type => 'display',
        data => { template => "<[field.value | date:'dd-MM-yyyy HH:MM']>" }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_next_sync',
        label => 'Volgende synchronisatie',
        description => 'Datum en tijd van de volgende (geplande) synchronisatie',
        type => 'display',
        data => { template => "<[field.value | date:'dd-MM-yyyy HH:MM']>" }
    )
];

=head2 ATTRIBUTE_LIST

C<ArrayRef[HashRef]> - Hardcoded list of PDC data source fields

=cut

use constant ATTRIBUTE_LIST => [
    {
        slug => 'id',
        external_name => 'SDU Entry ID',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'name',
        external_name => 'Naam',
        attribute_type => 'magic_string',
        objecttype_attribute => 1,
    },
    {
        slug => 'description',
        external_name => 'Omschrijving',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'price',
        external_name => 'Prijs',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'approach',
        external_name => 'Aanpak',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'conditions',
        external_name => 'Voorwaarden',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'appeal',
        external_name => 'Bezwaar en beroep',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'law_rules',
        external_name => 'Wet- en regelgeving',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'contact',
        external_name => 'Contact',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'submission_addresses',
        external_name => 'Adressen',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'audience',
        external_name => 'Doelgroepen',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'keywords',
        external_name => 'Trefwoorden',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    }
];

=head2 MODULE_SETTINGS

C<HashRef> - Attribute key/value pairs for module instantiation

=cut

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'SDU Connect PDC',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'incoming',
    manual_type => [qw[file]],
    is_multiple => 1,
    is_manual => 1,
    allow_multiple_configurations => 1,
    is_objecttype_interface => 1,
    has_attributes => 1,
    test_interface => 1,
    attribute_list => ATTRIBUTE_LIST,

    active_state_callback => sub {
        return shift->install_scheduled_job(@_);
    },

    test_definition => {
        description => '',
        tests => [
            {
                id => 1,
                label => 'Test verbinding met SDU Connect',
                name => 'test_connection',
                method => 'test_connection',
                description => 'Test verbinding met de gespecifieerde API'
            },
            {
                id => 2,
                label => 'Test PDC delta verzoek',
                name => 'test_get_delta',
                method => 'test_get_delta',
                description => 'Test het ophalen van een PDC delta'
            }
        ]
    },

    trigger_definition => {
        delta_sync => {
            method => 'delta_sync',
            update => 1
        },

        create_object => {
            method => 'create_object',
            update => 1
        },

        update_object => {
            method => 'update_object',
            update => 1
        },

        delete_object => {
            method => 'delete_object',
            update => 1
        }
    }
};

=head1 ATTRIBUTES

=head1 METHODS

=cut

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

=head2 entry_type

Implements interface required by
L<Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient>.

Returns the hard-coded string C<pdc>.

=cut

sub entry_type { 'pdc' }

=head2 entry_nodeset

Implements interface required by
L<Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient>.

Returns a L<XML::XPath::NodeSet>.

=cut

sub entry_nodeset {
    my $self = shift;
    my $xpath = shift;

    return $xpath->find('/page/product');
}

=head2 entry_relations

Implements interface required by
L<Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient>.

Returns an empty list, SDU PDC items do not serialize with relations.

=cut

sub entry_relations { }

=head2 entry_id

=cut

sub entry_id {
    my $self = shift;
    my $xpath = shift;
    my $entry = shift;

    return "" . $xpath->findvalue('./@id', $entry);
}

=head2 entry_values

Implements interface required by
L<Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient>.

Returns a mapping of internal slugs to values, as derived from a PDC entry.

=cut

sub entry_values {
    my $self = shift;
    my $xpath = shift;
    my $entry = shift;

    my @law_rules;
    my $law_rules_text = '';
    my @submission_addresses;
    my $submission_addresses_text = '';

    for my $law_rule ($xpath->findnodes('./law_rules/law_rule', $entry)) {
        push @law_rules, sprintf(
            '<li><a href="%s">%s</a></li>',
            $xpath->find('./url', $law_rule),
            $xpath->find('./law_rule_name', $law_rule)
        );
    }

    if (scalar @law_rules) {
        $law_rules_text = sprintf(
            '<p><ul>%s</ul></p>',
            @law_rules
        );
    }

    for my $submission_address ($xpath->findnodes('./uitvoerende_instanties/instantie', $entry)) {
        push @submission_addresses, sprintf(
            '<li><a href="%s">%s</a></li>',
            $xpath->find('./url', $submission_address),
            $xpath->find('./naam', $submission_address)
        );
    }

    if (scalar @submission_addresses) {
        $submission_addresses_text = sprintf(
            '<p><ul>%s</ul></p>',
            join('', @submission_addresses)
        );
    }

    my @audiences = map {
        $_->string_value
    } $xpath->findnodes('./doelgroepen/doelgroep', $entry);

    my @path = (qw[
        .
        overheidproduct:scproduct
        overheidproduct:meta
        overheidproduct:owmsmantel
        dcterms:subject
    ]);

    my $subjects = $xpath->find(join('/', @path), $entry);

    my @keywords = map {
        $_->string_value
    } $subjects->get_nodelist;

    return {
        name => "" . $xpath->findvalue('./name', $entry),
        description => "" . $xpath->findvalue('./description', $entry),
        approach => "" . $xpath->findvalue('./procedure', $entry),
        conditions => "" . $xpath->findvalue('./conditions', $entry),
        appeal => "" . $xpath->findvalue('./appeal', $entry),
        contact => "" . $xpath->findvalue('./contact', $entry),
        price => "" . $xpath->findvalue('./costs', $entry),
        date_expires => "" . $xpath->findvalue('./date_expires', $entry),
        law_rules => $law_rules_text,
        submission_addresses => $submission_addresses_text,
        audience => join(', ', @audiences),
        keywords => join(' ', @keywords)
    };
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Sysin::Modules::SDUConnect>, L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
