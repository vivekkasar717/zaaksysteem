package Zaaksysteem::API::v1::Serializer::Reader::Subject;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::DispatchReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::Subject - Subject readers, for the subjects like person,company,medewerker

=head1 DESCRIPTION

This serializer encodes various Subject Objects into a valid APIv1 JSON structure.

=head1 OBJECT TYPES

The following object types will be converted via this reader:

=over

=item Person

=item Company

=back

=cut

sub dispatch_map {
    return (
        # 'Zaaksysteem::Model::DB::Bedrijf' => sub { __PACKAGE__->read_subject_from_row(@_) },
        # 'Zaaksysteem::Model::DB::Subject' => sub { __PACKAGE__->read_subject_from_row(@_) },
        # 'Zaaksysteem::Model::DB::NatuurlijkPersoon' => sub { __PACKAGE__->read_subject_from_row(@_) },
        # 'Zaaksysteem::Object::Types::Subject' => sub { __PACKAGE__->read_object_type_subject(@_) },
    );
}

=head2 read_object_type_subject

Will generate a object from type L<Zaaksysteem::Object::Types::Subject>

=cut

sub read_object_type_subject {
    my ($class, $serializer, $object, $opts) = @_;

    my @attrs = grep { $_->does('Zaaksysteem::Metarole::ObjectAttribute') }
                     $object->meta->get_all_attributes;

    my %instance_attributes = map { my $value = $_->get_value($object); $_->name => (blessed $value ? $serializer->read($value) : $value) } @attrs;

    return {
        type => $object->type,
        reference => $object->id,
        instance => {
            %instance_attributes,
            subject     => ($class->read_related_subject_object($serializer, $object->subject) || undef)
        }
    };
}

=head2 read_subject_from_row

Will convert a L<DBIx::Class::Row> of table C<Bedrijf>, C<Subject>, C<NatuurlijkPersoon> to a Subject Object

=cut

sub read_subject_from_row {
    my ($class, $serializer, $row)  = @_;

    return $class->read_object_type_subject($serializer, $row->as_object);
}

=head2 read_related_subject_object

Will convert a subject of type C<Person>, C<Employee> or C<Company> to an object.

=cut

sub read_related_subject_object {
    my ($class, $serializer, $object)  = @_;

    my $subject     = $serializer->read($object);

    $subject->{instance}->{$_} = $serializer->read($object->$_) for grep ({ $object->$_ } qw/address_correspondence address_residence/);

    return $subject;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
