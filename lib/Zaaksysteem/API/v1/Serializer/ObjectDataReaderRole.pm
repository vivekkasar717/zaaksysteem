package Zaaksysteem::API::v1::Serializer::ObjectDataReaderRole;
use Moose::Role;

use BTTW::Tools qw(sig);
use Zaaksysteem::Types qw(UUID);
use JSON;

=head1 NAME

Zaaksysteem::API::v1::Serializer::ObjectDataReaderRole - Convenience role for simple
reader implementations

=head1 SYNOPSIS

    package Zaaksysteem::API::v1::Serializer::Reader::MyReader;

    use Moose;

    with 'Zaaksysteem::API::v1::Serializer::ObjectDataReaderRole';

    has '+object_data_class' => ( default => 'some_class' );


=head1 DESCRIPTION

This role assists with building reader classes that handle one type
specifically.

The role requires that the methods C<class> and C<read> are implemented by
the consuming class.

The C<class> method must return a value that can be used for is-a predicate
checks.

The C<read> method must return a hashref that can be passed to an encoder.

This role should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 ATTRIBUTES

=head2 object_data_class

=cut

has object_data_class => (
    is        => 'ro',
    isa       => 'Str',
    required  => 1,
);

=head1 METHODS

=head2 class

Implements the interface required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub class { 'Zaaksysteem::Backend::Object::Data::Component' }

=head2 grok

Implements the interface required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    if ($object->object_class eq $class->object_data_class) {
        return sub { $class->read_object_data(@_) };
    }

    return;
}

=head2 read_object_data

Transforms an L<Zaaksysteem::Backend::Object::Data::Component> instance into
a plain C<HashRef> for serialization.

=cut

sig read_object_data => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_object_data {
    my ($class, $serializer, $object) = @_;

    return {
        type      => $object->object_class,
        reference => UUID->check($object->id) ? $object->id : undef,
        instance  => {
            id => $object->id,
            $class->_map_object_attributes($serializer, $object),
        }
    };
}

sub _parse_value {
    my $class      = shift;
    my $serializer = shift;
    my $value      = shift;

    return $serializer->read($value)
        if blessed($value) && !JSON::is_bool($value);

    return $value;
}

sub _map_object_attributes {
    my ($self, $serializer, $object, @attribute_list) = @_;

    if (@attribute_list) {
        return map {
            $_ => $self->_parse_value($serializer,
                $object->get_object_attribute($_)->value)
        } @attribute_list;
    }

    return map {
        $_->name => $self->_parse_value($serializer, $_->value);
    } grep { $_->name !~ /^object\./ && $_->name }
        @{ $object->object_attributes };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
