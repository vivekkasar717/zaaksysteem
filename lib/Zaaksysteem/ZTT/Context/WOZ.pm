package Zaaksysteem::ZTT::Context::WOZ;
use Moose;

with 'Zaaksysteem::ZTT::Context';

use File::Temp ();
use HTML::TreeBuilder;
use LWP::Simple ();
use Zaaksysteem::Constants qw(
    ZAAKSYSTEEM_BETROKKENE_SUB
);
use Zaaksysteem::ZTT::Element;

=head1 NAME

Zaaksysteem::ZTT::Context::WOZ - WOZ specific context for ZTT

=head1 SYNOPSIS

    use Zaaksysteem::ZTT::Context::WOZ;

    my $objects = $schema->resultset('WozObjects')->search({});
    my $context = Zaaksysteem::ZTT::Context::WOZ->new($objects);

    my $ztt = Zaaksysteem::ZTT->new->add_context($context);

=head1 ATTRIBUTES

=head2 woz_objects

A L<WozObjects|Zaaksysteem::DB::Resultset::WozObjects> resultset, used to
return the right bits of context to ZTT.

=cut

has woz_object => (
    is       => 'ro',
    required => 1,
);

=head2 settings

A hash containing the WOZ settings (woz_taxatieverslag_text, woz_object_text,
woz_waardebepaling_text, woz_kadastrale_percelen_text)

=cut

has settings => (
    is       => 'ro',
    required => 0,
    default => sub { {} },
);

=head2 betrokkene

The currently logged-in "betrokkene", so their details can be put in the report.

=cut

has betrokkene => (
    is       => 'ro',
    isa      => 'Maybe[Zaaksysteem::Betrokkene::Object]',
    required => 0,
);

=head2 _created_tempfiles

List of temporary files created (for images), so that they can be deleted when
the context is destroyed.

=cut

has _created_tempfiles => (
    is       => 'rw',
    isa      => 'ArrayRef',
    default  => sub { [] },
    traits   => ['Array'],
    init_arg => undef,
    handles => {
        add_tempfile => 'push',
    },
);

=head1 METHODS

=cut

sub _date_preprocessor {
    my $self = shift;
    my $date = shift;

    $date =~ s/^([0-9]{4})([0-9]{2})([0-9]{2})$/$3-$2-$1/;

    return $date;
}

my %magicstring_mapping = (
    straatnaam           => 'Straatnaam',
    huisnummer           => 'Huisnummer',
    huisletter           => 'Huisletter',
    huisnummertoevoeging => 'Huisnummertoevoeging',
    postcode             => 'Postcode',
    woonplaats           => 'Woonplaatsnaam',
    woz_objectnummer     => 'WOZ-objectnummer',
    waardepeildatum      => {
        preprocessor   => \&_date_preprocessor,
        value_location => 'Waardepeildatum',
    },
    toestandspeildatum => {
        preprocessor   => \&_date_preprocessor,
        value_location => ['onderbouwing_taxatie', 'Toestandspeildatum'],
    },
    vastgestelde_waarde => {
        preprocessor   => sub { my $self = shift; sprintf("%.0f", shift) },
        value_location => 'Vastgestelde waarde',
    },
    meegetaxeerde_oppervlakte => {
        preprocessor   => sub { my $self = shift; sprintf("%d", shift) },
        value_location => 'Meegetaxeerde oppervlakte gebouwd',
    },
    vastgoedobjectsoort => {
        preprocessor   => sub {
            my $self = shift;
            my $value = shift;

            return $self->woz_object->objectcodes()
                ->{ $value }
                ->{"Omschrijving Vastgoedobjectsoort"};
        },
        value_location => ['onderbouwing_taxatie', 'Soort-object-code'],
    },
);

my @derived_data = qw(
    bouwjaar
    totale_oppervlakte
    totale_waarde
    report_type
    report_type_basic
    monumentaanduiding
    partialobjectcodes
);


=head2 get_string_fetchers

Return an array of subroutine (references) that return a
L<Zaaksysteem::ZTT::Element> if they are given a recognised tag.

=cut

sub get_string_fetchers {
    my $self = shift;

    my @fetchers;

    # "Plain" JSON data, possibly with a simple transformation applied.
    push @fetchers, sub {
        my $tagname = shift->name;

        return if not exists $magicstring_mapping{ $tagname };

        my $preprocessor;
        my $value_location = $magicstring_mapping{ $tagname };
        if (ref $magicstring_mapping{ $tagname } eq 'HASH') {
            $preprocessor = $magicstring_mapping{ $tagname }{ preprocessor };
            $value_location = $magicstring_mapping{ $tagname }{ value_location };
        }

        my $value;
        if (ref $value_location eq 'ARRAY') {
            $value = $self->woz_object->object_data;

            for my $sub (@{ $value_location }) {
                $value = $value->{ $sub };
            }
        }
        else {
            $value = $self->woz_object->object_data->{ $value_location }
        }

        if($preprocessor) {
            $value = $preprocessor->($self, $value);
        }

        return Zaaksysteem::ZTT::Element->new(
            value => $value
        );
    };

    # Derived values
    push @fetchers, sub {
        my $tagname = shift->name;

        return unless grep { $tagname eq $_ } @derived_data;

        return Zaaksysteem::ZTT::Element->new(
            value => $self->woz_object->$tagname(),
        );
    };

    # Some magic values to make tables appear/disappear
    push @fetchers, sub {
        my $tagname = shift->name;

        if ($tagname eq 'has_hwk_type1') {
            my $has_type1 = grep {
                $_->{"Kapitalisatiefactor"} <= 0
            } @{ $self->woz_object->object_data->{onderbouwing_taxatie_onderdeel} };

            return Zaaksysteem::ZTT::Element->new(
                value => $has_type1 ? 1 : 0,
            );
        }
        elsif ($tagname eq 'has_hwk_type3') {
            my $has_type3 = grep {
                $_->{"Kapitalisatiefactor"} > 0
            } @{ $self->woz_object->object_data->{onderbouwing_taxatie_onderdeel} };

            return Zaaksysteem::ZTT::Element->new(
                value => $has_type3 ? 1 : 0,
            );
        }

        return;
    };

    push @fetchers, sub {
        my $tagname = shift->name;
        return unless $tagname eq 'taxatie_opbouw_extra';

        if (   $self->woz_object->report_type eq 'grond'
            || $self->woz_object->report_type eq 'huurwaardekapitalisatie'
        ) {
            return Zaaksysteem::ZTT::Element->new(
                value => "en kapitalisatie huurwaarde",
            );
        }
        elsif($self->woz_object->report_type eq 'incourant') {
            return Zaaksysteem::ZTT::Element->new(
                value => "en gecorrigeerde vervangingswaarde",
            );
        }

        return;
    };

    push @fetchers, sub {
        my $tagname = shift->name;
        return unless $tagname =~ /^ref_object(?<object_nr>[1-3])_(?<tag>.*)/;

        my $object_nr = $+{object_nr};
        my $sub_tag   = $+{tag};

        my $vergelijkbare_objecten = $self->woz_object->object_data->{vergelijkbare_objecten};
        return if not $vergelijkbare_objecten;
        return if (@$vergelijkbare_objecten < $object_nr);

        my $object = $vergelijkbare_objecten->[$object_nr - 1]; # Arrays are 0-based
        my $woz_object = $object->{woz_object};

        my ($value, $type);
        if ($sub_tag eq 'aanwezig')           { $value = 1; }
        elsif ($sub_tag eq 'vermelding')      { $value = $object->{"Indicatie vermelding op taxatieverslag"} }
        elsif ($sub_tag eq 'verkoopprijs')    { $value = $object->{'marktinformatie'}{'Transactieprijs'} + 0 }
        elsif ($sub_tag eq 'straatnaam')      { $value = $woz_object->{'Straatnaam'} }
        elsif ($sub_tag eq 'huisnummer')      { $value = $woz_object->{'Huisnummer'} }
        elsif ($sub_tag eq 'postcode')        { $value = $woz_object->{'Postcode'} }
        elsif ($sub_tag eq 'woonplaats')      { $value = $woz_object->{'Woonplaatsnaam'} }
        elsif ($sub_tag eq 'woz_waarde')      { $value = $woz_object->{'Vastgestelde waarde'} + 0 }
        elsif ($sub_tag eq 'waardepeildatum') { $value = $self->_date_preprocessor($woz_object->{'Waardepeildatum'}) }
        elsif ($sub_tag eq 'verkoopdatum')    { $value = $self->_date_preprocessor($object->{'marktinformatie'}{'Datum transactie'}) }
        elsif ($sub_tag eq 'vastgoedobjectsoort') {
            $value = $self->woz_object->objectcodes()
                ->{$woz_object->{'onderbouwing_taxatie'}{'Type-aanduiding'}}
                ->{"Omschrijving Vastgoedobjectsoort"};
        }
        elsif ($sub_tag eq 'foto') {
            $value = $self->_download_photo($object->{"WOZ-objectnummer referentie-object"});
            $type = 'image' if defined $value;
        }
        else {
            # Unknown tag
            return;
        }

        return Zaaksysteem::ZTT::Element->new(
            value => $value,
            $type ? (type => $type) : (),
        );
    };

    push @fetchers, sub {
        my $tagname = shift->name;
        return unless $tagname =~ /^betrokkene_(?<part>.*)$/;
        return unless $self->betrokkene;

        my $part = $+{part};

        my $value = ZAAKSYSTEEM_BETROKKENE_SUB->($self->betrokkene, $part) || '';

        return Zaaksysteem::ZTT::Element->new(
            value => $value,
        );
    };

    push @fetchers, sub {
        my $tagname = shift->name;

        my ($value, $type);
        if ($tagname eq 'object_foto') {
            $value = $self->_download_photo($self->woz_object->object_id);
            $type = 'image' if defined $value;
        }
        else {
            return;
        }

        return Zaaksysteem::ZTT::Element->new(
            value => $value,
            $type ? (type => $type) : (),
        );
    };


    # Settings
    push @fetchers, sub {
        my $tagname = shift->name;
        return unless exists $self->settings->{$tagname};

        my $builder = HTML::TreeBuilder->new;
        $builder->implicit_body_p_tag(1);
        $builder->p_strict(1);
        $builder->no_space_compacting(1);

        my $tree = $builder->parse($self->settings->{$tagname});
        $builder->eof;

        return Zaaksysteem::ZTT::Element->new(
            type  => 'richtext',
            value => [ $tree->find(qw[p ul ol]) ],
        );
    };

    return @fetchers;
}

=head2 get_context_iterators

Returns a hashref containing context generators for iterating blocks.

=cut

sub get_context_iterators {
    my $self = shift;

    my %iterators = (
        taxatie_opbouw_incourant => sub { $self->_taxatie_opbouw },
        taxatie_opbouw_agrarisch => sub { $self->_taxatie_opbouw },
        taxatie_opbouw_woning    => sub { $self->_taxatie_opbouw },
        taxatie_opbouw_grond     => sub { $self->_taxatie_opbouw },
        # "Type 1", the one line rows
        taxatie_opbouw_huurwaardekapitalisatie       => sub { $self->_taxatie_opbouw('type1') },
        # "Type 3", the three-line rows (Kapitalisatiefactor > 0)
        taxatie_opbouw_huurwaardekapitalisatie_type3 => sub { $self->_taxatie_opbouw('type3') },
        kadastrale_percelen => sub { $self->_kadastrale_percelen },
    );

    for my $ref_object (qw(1 2 3)) {
        $iterators{"ref_object${ref_object}_onderbouwing"} = sub {
            $self->_ref_object_onderbouwing($ref_object);
        };
    }

    return \%iterators;
}

sub _taxatie_opbouw {
    my $self = shift;
    my $hwk_part = shift || '';

    my @rv;
    for my $onderdeel (@{ $self->woz_object->object_data->{onderbouwing_taxatie_onderdeel} }) {
        next if ($hwk_part eq 'type1' && $onderdeel->{'Kapitalisatiefactor'} > 0);
        next if ($hwk_part eq 'type3' && $onderdeel->{'Kapitalisatiefactor'} <= 0);

        my %onderdeel_ctx = (
            omschrijving => $self->woz_object->partialobjectcodes
                ->{ $onderdeel->{'Code onderdeel WOZ-object'} }
                ->{'Omschrijving code onderdeel'},

            bouwjaar     => $onderdeel->{Bouwjaar},
            oppervlakte  => $onderdeel->{Oppervlakte} + 0,
            inhoud       => $onderdeel->{Inhoud} + 0,
            waarde       => $onderdeel->{"Bepaalde waarde onderdeel"} + 0,

            waarde_per_eenheid   => $onderdeel->{"Waarde per stuk/eenheid"} + 0,
        );

        if ($onderdeel->{"Kapitalisatiefactor"} > 0) {
            $onderdeel_ctx{huurwaarde_per_m2}    = $onderdeel->{"Huurwaarde per vierkante meter"} + 0;
            $onderdeel_ctx{kapitalisatie_factor} = sprintf("%.2f", $onderdeel->{"Kapitalisatiefactor"} * 0.1);
            $onderdeel_ctx{huurwaarde}           = $onderdeel->{"Huurwaarde"} + 0;
        }

        if ($onderdeel->{Restwaarde} > 0) {
            $onderdeel_ctx{levensduur_verwacht} = $onderdeel->{'Verwachte levensduur'} + 0;
            $onderdeel_ctx{restwaarde}          = $onderdeel->{Restwaarde} + 0;
            $onderdeel_ctx{vervangingskosten}   = $onderdeel->{'Vervangingskosten per kubieke meter/ stuk/'} + 0;
            $onderdeel_ctx{vervangingswaarde}   = $onderdeel->{'Ongecorrigeerde vervangingswaarde'} + 0;
            $onderdeel_ctx{verouderings_factor} = $onderdeel->{'Factor voor technische veroudering'} / 1000;
            $onderdeel_ctx{verouderings_reden}  = "";

            if (   $onderdeel->{'Factor voor technische veroudering'} > 0
                && $onderdeel->{'Factor voor technische veroudering'} < 1000
            ) {
                $onderdeel_ctx{technische_veroudering} .= "technische veroudering",
            }
            if (   $onderdeel->{'Factor voor functionele veroudering'} > 0
                && $onderdeel->{'Factor voor functionele veroudering'} < 1000
            ) {
                $onderdeel_ctx{functionele_veroudering} .= "functionele veroudering",
            }
        }

        push @rv, \%onderdeel_ctx;
    }

    return \@rv;
}

sub _kadastrale_percelen {
    my $self = shift;

    my @rv;
    for my $perceel (@{ $self->woz_object->object_data->{kadastrale_identificatie} }) {
        push @rv, {
            gemeentecode         => $perceel->{"Kadastrale gemeentecode"},
            sectie               => $perceel->{"Sectie"},
            perceelnummer        => $perceel->{"Perceelnummer"} + 0,
            perceel_index_letter => $perceel->{"Perceel-index-letter"},
            perceel_index_nummer => $perceel->{"Perceel-index-nummer"} + 0,
            perceel_oppervlakte  => $perceel->{"Toegekende oppervlakte"} + 0,
        };
    }
    return \@rv;
}

sub _ref_object_onderbouwing {
    my $self = shift;
    my ($object_nr) = @_;

    my $vergelijkbare_objecten = $self->woz_object->object_data->{"vergelijkbare_objecten"};
    return [] if not $vergelijkbare_objecten;
    return [] if (@$vergelijkbare_objecten < $object_nr);

    my $object = $vergelijkbare_objecten->[$object_nr - 1]; # Arrays are 0-based
    my $woz_object = $object->{"woz_object"};

    my @rv;
    for my $onderdeel (@{ $woz_object->{"onderbouwing_taxatie_onderdeel"} }) {
        push @rv, {
            onderdeel   => $self->woz_object->partialobjectcodes
                ->{ $onderdeel->{'Code onderdeel WOZ-object'} }
                ->{'Omschrijving code onderdeel'},
            bouwjaar    => $onderdeel->{"Bouwjaar"},
            oppervlakte => $onderdeel->{"Oppervlakte"} + 0,
            inhoud      => $onderdeel->{"Inhoud"} + 0,
        };
    }

    return \@rv;
}

sub _download_photo {
    my $self = shift;
    my ($object_id) = @_;

    my $base_url = $self->settings->{woz_photo_base_url};
    return unless $base_url;

    my $fh = File::Temp->new(
        TEMPLATE => "wozimgXXXXX",
        SUFFIX   => ".jpg"
    );
    $self->add_tempfile($fh);

    (my $url = $base_url) =~ s/\[\[woz_object_id\]\]/$object_id/;

    my $content = LWP::Simple::get($url);
    return unless defined $content;

    print $fh $content;
    $fh->flush();

    return $fh->filename;
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
