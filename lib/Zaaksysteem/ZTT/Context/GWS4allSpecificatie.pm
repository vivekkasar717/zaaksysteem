package Zaaksysteem::ZTT::Context::GWS4allSpecificatie;
use Moose;

extends 'Zaaksysteem::ZTT::Context::GWS4all';

use String::CamelCase qw(decamelize camelize);

=head1 ATTRIBUTES

=head2 specificatie

The specific row from the "" list in the GWS4all data structure.

=head2 client

The "Client" part of the GWS4all XML data structure..

=cut

has specificatie => (
    is  => 'ro',
    isa => 'HashRef',
);

=head1 METHODS

=head2 get_string_fetchers

Return an array of subroutine (references) that return a
L<Zaaksysteem::ZTT::Element> if they are given a recognised tag.

=cut

around get_string_fetchers => sub {
    my $orig = shift;
    my $self = shift;

    my @fetchers = $self->$orig(@_);
    push @fetchers, sub {
        my $tagname = shift->name;
        return if $tagname !~ /^specificatie_(?<tag>.*)$/;

        my $tag = $+{tag};

        my $specificatie = $self->specificatie->{Dossierhistorie}[0];

        my $value;

        for my $key (qw(
            Betrekkingsperiode
            Regeling
            Iban
            Bic
            Dossiernummer
        )) {
            if ($tag eq decamelize($key)) {
                return Zaaksysteem::ZTT::Element->new(value => $specificatie->{ $key });
            }
        }

        for my $key (qw(
            GekorteInkomsten
            InkomstenNaVrijlating
            InkomstenVrijlating
            OpgegevenInkomsten
            TeVerrekenenInkomsten
            UitbetaaldBedrag
            VakantiegeldOverInkomsten
        )) {
            if ($tag eq decamelize($key)) {
                return Zaaksysteem::ZTT::Element->new(
                    value => $self->_flatten_bedrag( $specificatie->{ $key } )
                );
            }
        }

        return;
    };
    push @fetchers, sub {
        my $tagname = shift->name;
        return if $tagname !~ /^uitkeringsinstantie_(?<tag>.*)/;

        my $tag = $+{tag};

        my $instantie = $self->specificatie->{Uitkeringsinstantie};
        return unless $instantie;

        if (exists $instantie->{ camelize($tag) }) {
            return Zaaksysteem::ZTT::Element->new(
                value => $instantie->{ camelize($tag) },
            );
        }

        return;
    };
    push @fetchers, sub {
        my $tagname = shift->name;
        return if $tagname !~ /^overige_bijstandspartij_(?<tag>.*)/;

        my $tag = $+{tag};

        # TODO: Support >1 extra party
        my $partij = $self->specificatie->{Dossierhistorie}[0]{OverigeBijstandspartij}[0];
        return unless $partij;

        if (exists $partij->{ camelize($tag) }) {
            if ($tag eq 'achternaam') {
                return Zaaksysteem::ZTT::Element->new(
                    value => $self->_get_last_name($partij)
                );
            }
            else {
                return Zaaksysteem::ZTT::Element->new(
                    value => $partij->{ camelize($tag) },
                );
            }
        }

        return;
    };

    return @fetchers;
};

=head2 get_context_iterators

Returns a hashref containing context generators for iterating blocks.

=cut

sub get_context_iterators {
    my $self = shift;

    return {
        componenthistorie => sub {
            my @rv;

            my $dossier = $self->specificatie->{Dossierhistorie}[0]{Componenthistorie};

            for my $line (@$dossier) {
                my $bedrag = $self->_flatten_bedrag($line->{Bedrag});

                my %columns = (
                    kolom1_bedrag => '',
                    kolom2_bedrag => '',
                    kolom3_bedrag => '',
                    omschrijving  => $line->{Omschrijving},
                );

                my $active_column = sprintf(
                    "kolom%d_bedrag",
                    $line->{IndicatieKolom},
                );
                $columns{ $active_column } = $bedrag;

                push @rv, \%columns;
            }

            return \@rv;
        },
    };
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
