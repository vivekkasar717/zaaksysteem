package Zaaksysteem::CLI::Knife::Object;

use Moose::Role;
use Zaaksysteem::CLI::Knife::Action;
use JSON;

my $knife = 'object';

=head1 NAME

Zaaksysteem::CLI::Knife::Object - Object related commandline actions

=head1 SYNOPSIS

    See USAGE from commandline output zsknife

=cut

register_knife      $knife => (
    description => "Object related functions"
);

register_category   'attr' => (
    knife           => $knife,
    description     => "Attribute actions"
);

register_action     'update' => (
    knife           => $knife,
    category        => 'attr',
    description     => 'Update attribute',
    run             => sub {
        my $self    = shift;
        my ($uuid, @params)  = @_;

        $self->show_usage_for_actions("uuid not correct\nUsage: attr update [uuid] key1=value1 key2=value2")
            unless ($uuid && $uuid =~ /^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/i);

        my $object  = $self->objectmodel->retrieve(uuid => $uuid);

        my $attrs   = $self->get_knife_params;

        for my $key (keys %$attrs) {
            my $value = $attrs->{$key};

            if ($value =~ /^\{.*\}$/) {
                ## Assume JSON
                $value = JSON::from_json($value);
            }

            $self->log->debug("Changing attribute $key into '$value'");
            $object->$key($value);   
        }

        $self->objectmodel->save(object => $object);
    }
);

register_action     'search' => (
    knife           => $knife,
    category        => 'attr',
    description     => 'Search uuids',
    run             => sub {
        my $self    = shift;
        my ($objectclass, $attr, @params)  = @_;

        $self->show_usage_for_actions("objectclass not given\nUsage: attr search objectclass attribute searchkey=searchvalue") unless $objectclass;
        $self->show_usage_for_actions("attr not given\nUsage: attr search objectclass attribute searchkey=searchvalue") unless $attr;

        my %search;
        for my $param (@params) {
            my ($key, $value) = split('=', $param);
            $search{$key} = $value;
        }

        my $objects  = $self->objectmodel->search_rs($objectclass, \%search);

        while (my $object = $objects->next) {
            print $object->$attr . "\n";
        }
    }
);

1;

=head1 SEE ALSO

L<Zaaksysteem::CLI::Knife>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

