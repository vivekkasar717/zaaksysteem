package Zaaksysteem::DB::Component::Logging::Scheduler::Run;

use Moose::Role;

use HTML::Strip;
use JSON;

has 'raw_description' => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self    = shift;

        my $task        = $self
                        ->result_source
                        ->schema
                        ->resultset('ScheduledJobs')
                        ->find($self->data->{task_id});

        my $task_list   = $task->result_source->resultset->task_list;

        my $task_def    = $task_list->{ $task->task };

        return $task_def->{description}->($task);
    }
);

sub onderwerp {
    my $self        = shift;

    return 'Wijziging via PIP: ' . $self->raw_description->{short};
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    my $stripper = HTML::Strip->new;

    $data->{ content } = $stripper->parse($self->raw_description->{long});

    $data->{ expanded } = JSON::false;

    return $data;
};

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

