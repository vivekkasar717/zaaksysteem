package Zaaksysteem::DB::Component::ScheduledJobs;


use Data::Dumper;
use DateTime;
use Moose;
use Moose::Util qw/apply_all_roles/;

extends 'DBIx::Class::Row';



sub scheduled_for_utc {
    shift->scheduled_for->set_time_zone('UTC');
}


sub new {
    shift->next::method(@_)->apply_roles;
}


=head2 apply_roles

When expanding this behaviour we can add a more sophisticated system
for finding the suited role. The naming conventions clash a bit, and
it's not worth writing an algorithm for one case.

=cut

sub apply_roles {
    my ($self) = @_;

    if ($self->task eq 'case/update_kenmerk') {
        apply_all_roles($self, __PACKAGE__ . '::Case::UpdateField');
    }
    return $self;
}


sub reject {
    my $self            = shift;

    $self->handle_rejection;

    $self->deleted(DateTime->now());
    $self->update;

}


sub handle_rejection {
    #nop, just here to prevent errors.
}


=head2 description

Default behaviour for displaying values

=cut

sub description {
    my ($self, $values) = @_;

    return @$values;
}


sub format_created_by {
    my ($self) = @_;

    my $created_by = $self->parameters->{created_by} or return 'Betrokkene';

    my $subject = $self->result_source->schema->betrokkene_model->get({}, $created_by);

    return 'Onbekende betrokkene' unless $subject;

    return $subject->naam;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 format_created_by

TODO: Fix the POD

=cut

=head2 handle_rejection

TODO: Fix the POD

=cut

=head2 new

TODO: Fix the POD

=cut

=head2 reject

TODO: Fix the POD

=cut

=head2 scheduled_for_utc

TODO: Fix the POD

=cut

