package Zaaksysteem::DB::Component::ZaaktypeAuthorisation;

use strict;
use warnings;

use base qw/DBIx::Class::Row/;
use Zaaksysteem::Object::Types::Casetype::Authorisation;

sub security_identity {
    my $self = shift;

    return (
        position => sprintf('%s|%s', $self->ou_id, $self->role_id)
    );
}

sub as_index {
    my $self = shift;
    return join(':', $self->ou_id, $self->role_id, $self->confidential);
}

sub right {
    my $self = shift;

    my $right = $self->recht;
    return 'manage' if $right eq 'zaak_beheer';
    return 'write'  if $right eq 'zaak_edit';
    return 'read'   if $right eq 'zaak_read';
    return 'search' if $right eq 'zaak_search';

}
sub as_object {
    my $self = shift;

    my $role  = $self->result_source->schema->resultset('Roles')->find($self->role_id);
    my $group = $self->result_source->schema->resultset('Groups')->find($self->ou_id);

    if (!$role or !$group) {
        # Die?
        return;
    }

    return Zaaksysteem::Object::Types::Casetype::Authorisation->new(
        right        => $self->right,
        confidential => $self->confidential,

        role  => $role->as_object,
        group => $group->as_object,
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 security_identity

TODO: Fix the POD

=cut

