package Zaaksysteem::DB::Component::ZaakOnafgerond;

=head1 NAME

Zaaksysteem::DB::Component::ZaakOnafgerond

=head1 DESCRIPTION

A component to load onto L<Zaaksysteem::Schema::ZaakOnafgerond>, providing
convenience methods and additional attributes.

=head1 SYNOPSIS

    package Zaaksysteem::Schema::ZaakOnafgerond;
    
    __PACKAGE__->load_components(
        "+Zaaksysteem::DB::Component::ZaakOnafgerond",
        __PACKAGE__->load_components()
    );

and later

    my $concept_case = $schema->resultset('ZaakOnafgerond')->new(
        ...
    );
    
    ...

=cut

use Moose;

use DateTime;

extends 'Zaaksysteem::Backend::Component';

with 'MooseX::Log::Log4perl';

=head1 ATTRIBUTES

=cut

=head2 created

This attribute holds a lazy-loaded L<DateTime> object that has the same time as
the C<create_unixtime> column.

Unfortunatly, that column has been defined as an 'integer' in the tabel called
C<zaak_onafgerond>.

    $concept_case->created
        ->set_time_zone('Europe/Amsterdam')
        ->set_locale('nl_NL')
        ->strftime('%e %B %Y, %H:%M uur');
    # 6 augustus 2018, 15:34 uur

=cut

has 'created' => (
    is => 'ro',
    isa => 'DateTime',
    lazy => 1,
    builder => '_created',
    init_arg => undef,
);

sub _created {
    my $self = shift;
    return DateTime->from_epoch( epoch => $self->create_unixtime )
}

=head1 METHODS

=cut

=head2 zaak_gegevens

Returns a perl data structure, based on the JSON data in C<json_string>.

    my $data = $concept_case->zaak_gegevens;
    say $data->{confidentiality}; # public

=cut

sub zaak_gegevens {
    my $self = shift;

    my $json = JSON::XS->new->allow_nonref->allow_blessed->convert_blessed;

    return $json->decode($self->json_string)
}

1; #__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
