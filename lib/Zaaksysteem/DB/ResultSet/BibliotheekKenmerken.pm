package Zaaksysteem::DB::ResultSet::BibliotheekKenmerken;

use Moose;

extends 'Zaaksysteem::Backend::ResultSet';

use constant MAGIC_STRING_DEFAULT => 'doc_variable';

sub generate_magic_string {
    my ($self, $suggestion) = @_;
    my $suggestion_ok       = 0;
    my $suggestion_counter  = 0;

    $suggestion             = lc($suggestion);

    ### Replace suggestion
    $suggestion             =~ s/ /_/g;
    $suggestion             =~  s/[^\w0-9_]//g;

    if (!$suggestion) {
        $suggestion = MAGIC_STRING_DEFAULT . ++$suggestion_counter;
    }

    ### Search database for given suggestion
    while (!$suggestion_ok) {
        my $rv = $self->search({magic_string => $suggestion});

        if (!$rv->count) {
            $suggestion_ok  = 1;
        } else {
            if ($suggestion_counter > 0) {
                $suggestion =~ s/$suggestion_counter$//;
            }
            $suggestion     .= ++$suggestion_counter;
        }
    }

    return $suggestion;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 generate_magic_string

TODO: Fix the POD

=cut

