package Zaaksysteem::Test::Search::ObjectQueryResultSetShim;

use Zaaksysteem::Test;

use SQL::Abstract;

use Zaaksysteem::Search::ObjectQueryResultSetShim;
use Zaaksysteem::Object::Query;

sub test_query_unfolding {
    my $sql = SQL::Abstract->new;

    is_deeply _unfold(qb_eq('my_field', 'my_value')), {
        my_field => { '=' => \[ '?::text', 'my_value' ] }
    }, 'simple equality unfolds';

    is_deeply _unfold(qb_eq(qb_lit('string', 'foo'), 'bar')), \[
        '( ?::text = ?::text )',
        'foo',
        'bar'
    ], 'value-only equality query unfolds';

    is_deeply _unfold(qb_eq('foo_field', qb_field('bar_field'))), {
        foo_field => { '=' => \[ 'bar_field' ] }
    }, 'field-only equality query unfolds';

    my $and_query = qb_and(
        qb_eq('my_field', 'value'),
        qb_ne('other_field', 'value')
    );

    is_deeply _unfold($and_query), {
        -and => [
            { my_field => { '=' => \[ '?::text', 'value' ] } },
            { other_field => { '!=' => \[ '?::text', 'value' ] } }
        ]
    }, 'and query with eq/ne sub-expressions unfold';

    is_deeply _unfold(qb_in(qb_lit('string', 'a'), [qw[a b c d e f]])), \[
        '( ?::text IN ( ?::text, ?::text, ?::text, ?::text, ?::text, ?::text ) )',
        qw[a a b c d e f]
    ], 'in query unfolds';
}

sub _unfold {
    return Zaaksysteem::Search::ObjectQueryResultSetShim::_unfold_query_expr(@_);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
