package Zaaksysteem::Test::Payment::Ogone;
use Moose;
extends "Zaaksysteem::Test::Moose";

use Test::Mock::One;
use Zaaksysteem::Test;
use Zaaksysteem::Payment::Ogone;

=head1 NAME

Zaaksysteem::Test::Payment::Ogone - Test Zaaksysteem::Payment::Ogone

=head1 DESCRIPTION

Test the Ogone payment model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Payment::Ogone

=cut

sub test_start_payment {
    my $og = Zaaksysteem::Payment::Ogone->new(
        baseurl                    => 'https://example.com/',
        shapass                    => 'sha-in',
        shapassout                 => 'sha-out',
        hash_algorithm             => 'sha512',
        order_description_template => 'This is a description [[aap]]',
    );

    isa_ok($og, 'Zaaksysteem::Payment::Ogone');

    my $mockzaak = Test::Mock::One->new(
        id => 42,
        get_string_fetchers => sub {
            sub { 
                if (${ $_[0]->expression } eq 'aap') {
                    Zaaksysteem::ZTT::Element->new(value => 'noot');
                }
                else {
                    Zaaksysteem::ZTT::Element->new(value => 'fout');
                }
            },
        },
        get_context_iterators => sub { return {} },
        'X-Mock-ISA' => 'Zaaksysteem::Schema::Zaak',
    );

    my $clock_handle = stop_clock(1000000000);

    $og->start_payment(
        amount => "13.37",
        zaak   => $mockzaak,
    );

    is($og->com, "This is a description noot", "Description template executed and output put into 'com' field");
    is($og->orderid, '1000000000z42', 'Order-id calculated correctly');

    is(
        $og->shasign,
        "D78D6B2DCCD07C76FD82D578347FC6A2E5DC093DCC108552905CF57627F23FCAFC2DBB50701EB53C43902F30A63AA7B3AAABB02DA1639AD6C10D5D41547DFBB6",
        "SHA-signature is correct"
    );
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
