package Zaaksysteem::Test::Filestore::Engine::Swift;
use strict;
use warnings;

=head1 NAME

Zaaksysteem::Test::Filestore::Engine::Swift - Test methods

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Filestore::Engine::Swift;

=head1 METHODS

=cut

use Zaaksysteem::Filestore::Engine::Swift;


use Zaaksysteem::Test;

use Test::MockModule;
use Test::MockObject;

=head2 erase

A simple check that we do call an external erase method for this specific store
and that we do call it with the right arguments, a lowercase UUID. Besides that
it checks that we do log what we are doing.

=cut

my $test_obj;

sub test_erase {

    my %mock;

    $mock{obj}{'Net::OpenStack::Swift'}{'new'}
        = _mock_obj__Net_OpenStack_Swift__new();
    $mock{obj}{'Log::Log4perl'}{'new'}
        = _mock_obj__Log_Log4perl__new();

    $mock{obj}{'Net::OpenStack::Swift'}{'new'}->clear();
    $mock{obj}{'Log::Log4perl'}{'new'}->clear();

    my $UUID = '976D6037-4336-4BB1-A4A3-34A6C5A23199'; # implicit testing lowercase

    lives_ok {
        $test_obj = Zaaksysteem::Filestore::Engine::Swift->new(
            name           => 'Swift',
            os_username    => 'test_username',
            os_password    => 'test_password',
            os_auth_url    => 'test_auth_url',
            os_tenant_name => 'test_tenant_name',
    #       os_timeout     => 30, # default
            storage_bucket => 'test_bucket',
            swift          => $mock{obj}{'Net::OpenStack::Swift'}{'new'},
            logger         => $mock{obj}{'Log::Log4perl'}{'new'},
        );
    } "Created 'Zaaksysteem::Filestore::Engine::Swift' object";

    lives_ok {
        $test_obj->erase($UUID);
    } "... and could call `erase` method";

    $mock{obj}{'Log::Log4perl'}{'new'}->called_ok( 'info',
        "... did log an INFO"
    );

    cmp_deeply (
        [$mock{obj}{'Log::Log4perl'}{'new'}->call_args(1)] => [
            $mock{obj}{'Log::Log4perl'}{'new'}, #self
            re( qr|Erasing object '([-a-f0-9]{36})' from bucket '(.+)'| )
        ],
        "... ... with the right message"
    );

    $mock{obj}{'Net::OpenStack::Swift'}{'new'}->called_ok( 'delete_object',
        "... did call the external erase method"
    );

    cmp_deeply (
        [$mock{obj}{'Net::OpenStack::Swift'}{'new'}->call_args(1)] => [
            $mock{obj}{'Net::OpenStack::Swift'}{'new'}, #self
            container_name => 'test_bucket',
            object_name    => re ( qr|^([-a-f0-9]{36})$| ), # lowercase
        ],
        "... ... with the right arguments"
    );

}

# done testing - mocks follow

sub _mock_obj__Net_OpenStack_Swift__new {
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_isa('Net::OpenStack::Swift');
    $mock_obj->set_always( delete_object => 'my_headers' );
    return $mock_obj;
}

sub _mock_obj__Log_Log4perl__new {
    my $mock_obj = Test::MockObject->new();
    $mock_obj->set_always( info => undef );
    return $mock_obj;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
