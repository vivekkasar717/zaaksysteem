package Zaaksysteem::TestFor::General::Case::file;
use base qw(Test::Class);

use TestSetup;

sub zs_case_case_documents : Tests {

    $zs->zs_transaction_ok(
        sub {
            my $document_kenmerk = $zs->create_bibliotheek_kenmerk_ok(value_type => 'file');

            my $zt = $zs->create_zaaktype_predefined_ok;

            my $node     = $zt->zaaktype_node_id;
            my ($status) = $node->zaaktype_statussen;
            my $ztk      = $zs->create_zaaktype_kenmerk_ok(
                status              => $status,
                bibliotheek_kenmerk => $document_kenmerk,
            );

            my $case = $zs->create_case_ok(zaaktype => $zt);

            my $file = $zs->create_file_ok(
                db_params => {
                    accepted => 1,
                    case     => $case,
                },
            );

            my $rs = $case->case_documents;
            is($rs->count, 0, "Got no case document");

            my $case_document = $zs->create_file_ok(
                db_params => {
                    accepted => 1,
                    case     => $case,
                },
                case_document_ids => [ $ztk->id ],
            );

            is($rs->count, 1, "Got one case document");
            is($rs->first->id, $case_document->id, "And we have the file we are looking for");

            {
                my $case_document = $zs->create_file_ok(
                    db_params => {
                        accepted => 0,
                        case     => $case,
                    },
                    case_document_ids => [ $ztk->id ],
                );

                is($rs->count, 1, "Got one case document");
                isnt($rs->first->id, $case_document->id, "Unaccepted file is not a case document yet");
            }

        },
        "case->case_documents",
    );

}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
