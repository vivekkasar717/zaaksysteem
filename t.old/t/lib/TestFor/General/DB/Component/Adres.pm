package TestFor::General::DB::Component::Adres;
use base qw(Test::Class);
use TestSetup;

sub adres_change : Tests {
    $zs->txn_ok(
        sub {
            my $np = $zs->create_natuurlijk_persoon_ok();

            my $adres = $np->adres_id;
            # Call the base method - The one in the subclass will reset the
            # search term to something it calculates itself.
            $np->DBIx::Class::Row::update({ search_term => 'empty'});

            $adres->woonplaats('Foobar');
            $adres->update;

            $np->discard_changes;
            like(
                $np->search_term,
                qr/Foobar/,
                "Search term of linked natuurlijk_persoon record is updated when address is updated"
            );
        }
    );
}

1;

__END__

=head1 NAME

TestFor::General::DB::Component::Zaaktype - A zaaktype tester

=head1 DESCRIPTION

Testing zaaktypes

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
