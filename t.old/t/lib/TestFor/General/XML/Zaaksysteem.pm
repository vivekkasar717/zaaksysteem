package TestFor::General::XML::Zaaksysteem;
use base qw(ZSTest);

use TestSetup;

use Zaaksysteem::XML::Zaaksysteem::Serializer;

sub _get_dt {
    return DateTime->new(
        year      => 1955,
        month     => 11,
        day       => 12,
        hour      => 22,
        minute    => 04,
        second    => 0,
        time_zone => 'America/Los_Angeles',
    );
}

sub case_to_xml : Tests {
    $zs->txn_ok(
        sub {
            my $case = $zs->create_case_ok();

            my $s = Zaaksysteem::XML::Zaaksysteem::Serializer->new(
                schema => $zs->schema,
            );

            my $xml = $s->case_to_xml($case);
            ok($xml, "We have got XML");

            $case->update({deleted => DateTime->now() });

            throws_ok(
                sub {
                    $s->case_to_xml($case);
                },
                qr#XML/Serializer/Case/Nonexistent#,
                "Destroyed cases cannot be exported to XML"
            );

        },
        'Case to XML'
    );
}

sub catalogue_to_xml : Tests {
    $zs->txn_ok(
        sub {

            my $snowman = "\x{2603}";
            $zs->create_bibliotheek_kenmerk_ok();
            $zs->create_bibliotheek_kenmerk_ok(values => [ { value => 'foo' } ]);
            $zs->create_bibliotheek_kenmerk_ok(magic_string => $snowman, label => $snowman, value => $snowman);
            $zs->create_bibliotheek_kenmerk_ok(type_multiple => 1);
            $zs->create_bibliotheek_kenmerk_ok(type_multiple => 1, values => [ { value => 'bar' }, { value => 'baz' } ]);
            $zs->create_bibliotheek_kenmerk_ok(system => 1);

            my $s = Zaaksysteem::XML::Zaaksysteem::Serializer->new(
                schema => $zs->schema,
            );

            my $xml = $s->catalogue_to_xml();
            ok($xml, "We have got XML");

        },
        'Catalogue to XML'
    );
}

sub xml_zaaksysteem : Tests {
    $zs->txn_ok(
        sub {

            my $s = Zaaksysteem::XML::Zaaksysteem::Serializer->new(
                schema => $zs->schema,
            );

            my $instance = $s->xml;

            {
                my $xml = $instance->case_attributes(
                    'writer',
                    {
                        Header => {
                            Timestamp => _get_dt,
                            Identifier =>
                                '0417caf4-a0a4-425b-afb0-df1bb5a48f6e',
                        },
                        Case => {
                            UUID      => 'cee517db-4b40-4085-bc42-e60f73239d64',
                            Attribute => [
                                {
                                    magicstring => 'foo',
                                    value       => 'bar',
                                },
                                {
                                    magicstring => 'baz',
                                    value       => '',
                                },
                            ]
                        },
                    }
                );

                my $want = $zs->slurp(
                    qw(t data xml zaaksysteem case_attribute_multiple.xml));
                is($xml, $want, "XML is correct for multiple attributes");
            }

            {
                my $xml = $instance->case_attributes(
                    'writer',
                    {
                        Header => {
                            Timestamp  => _get_dt,
                            Identifier => '0417caf4-a0a4-425b-afb0-df1bb5a48f6e',
                        },
                        Case => {
                            UUID      => 'cee517db-4b40-4085-bc42-e60f73239d64',
                        },
                    }
                );
                my $want = $zs->slurp(qw(t data xml zaaksysteem case_attribute_none.xml));
                is($xml, $want, "XML is correct for no attributes, without attribute in hash");
            }

            {
                my $xml = $instance->case_attributes(
                    'writer',
                    {
                        Header => {
                            Timestamp  => _get_dt,
                            Identifier => '0417caf4-a0a4-425b-afb0-df1bb5a48f6e',
                        },
                        Case => {
                            UUID      => 'cee517db-4b40-4085-bc42-e60f73239d64',
                            Attribute => [],
                        },
                    }
                );
                my $want = $zs->slurp(qw(t data xml zaaksysteem case_attribute_none.xml));
                is($xml, $want, "XML is correct for no attributes");
            }

            {
                my $xml = $instance->case_attributes(
                    'writer',
                    {
                        Header => {
                            Timestamp  => _get_dt,
                            Identifier => '0417caf4-a0a4-425b-afb0-df1bb5a48f6e',
                        },
                        Case => {
                           UUID      => 'cee517db-4b40-4085-bc42-e60f73239d64',
                            Attribute => [
                                {
                                    type        => 'foo',
                                    magicstring => 'foo',
                                    value       => 'bar',
                                },
                            ],
                       },
                    }
                );
                my $want = $zs->slurp(qw(t data xml zaaksysteem case_attribute_one.xml));
                is($xml, $want, "XML is correct for one attribute: in array");
            }
            {
                my $xml = $instance->case_attributes(
                    'writer',
                    {
                        Header => {
                            Timestamp  => _get_dt,
                            Identifier => '0417caf4-a0a4-425b-afb0-df1bb5a48f6e',
                        },
                        Case => {
                            UUID      => 'cee517db-4b40-4085-bc42-e60f73239d64',
                            Attribute => {
                                magicstring => 'foo',
                                value       => 'bar',
                            },
                        },
                    }
                );
                my $want = $zs->slurp(qw(t data xml zaaksysteem case_attribute_one.xml));
                is($xml, $want, "XML is correct for one attribute: no array");
            }

        },
        "TO_XML"
    );

}


1;

__END__

=head1 NAME

TestFor::General::XML::Zaaksysteem - Tests Zaaksysteem's own XML

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
