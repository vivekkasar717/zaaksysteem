package TestFor::General::XML::Generator;
use base 'ZSTest';

use Cwd qw(realpath);
use File::Basename;
use File::Spec::Functions qw(catdir catfile);
use TestSetup;

BEGIN {
    package XXX::XML::Generator {
        use Moose;
        with 'Zaaksysteem::XML::Generator';

        sub path_prefix { return 'path_prefix' }
        sub name { return 'name' }
    }
}

sub test_xml_generator : Tests {
    my $x = XXX::XML::Generator->new();

    my $base_dir = catdir(
        dirname(__FILE__), # Generator.pm
        '..',              # XML/
        '..',              # General/
        '..',              # TestFor/
        '..',              # lib/
        '..',              # t/
    );
    my $dir = catdir($base_dir, "share", "xml-templates");

    is(
        realpath($x->template_root),
        realpath($dir),
        "Template root found correctly"
    );

    my $template_dir = realpath(catdir($dir, "path_prefix"));

    is(
        $x->template_path,
        $template_dir,
        "Template path (with specific suffix) found correctly"
    );

    no warnings 'redefine';
    local *XXX::XML::Generator::template_path = sub {
        # This is where build_generator_methods will look for XML files
        return realpath(catdir($base_dir, qw(t data xml templates))); 
    };
    
    $x->build_generator_methods();

    ok( $x->can('tryout1'), 'XML file is found and mapped onto a method');
    ok(!$x->can('tryout2'), 'Directory is ignored, even with .xml suffix');
    ok(!$x->can('tryout3'), 'File without .xml suffix is ignored');

    is(
        $x->tryout1('writer', { template_var => 'foo'}),
        "VAR1: foo\n",
        'The "tryout1" template is processed correctly'
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
