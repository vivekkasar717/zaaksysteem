package TestFor::General::Betrokkene::Object;
use base 'ZSTest';

use TestSetup;
use Zaaksysteem::Betrokkene;
use Zaaksysteem::Constants qw(
    ZAAKSYSTEEM_GM_AUTHENTICATEDBY_BEHANDELAAR
);

sub test_has_valid_address : Tests {
    my $betrokkene_model = Zaaksysteem::Betrokkene->new(
        'dbic'      => $schema,
    );

    {
        my $np = $zs->create_natuurlijk_persoon_ok();

        my $np_obj = $betrokkene_model->get({}, 'betrokkene-natuurlijk_persoon-' . $np->id);

        ok($np_obj->has_valid_address, "Fresh betrokkene (natuurlijk persoon) has valid address");

        $np_obj->straatnaam(undef);

        ok( !$np_obj->has_valid_address, "No street = no valid address");

        $np_obj->adres_buitenland1('foobar');

        ok($np_obj->has_valid_address, "adres_buitenland1 value implies valid address");
    }

    {
        my $bedrijf = $zs->create_bedrijf_ok();

        my $bedrijf_obj = $betrokkene_model->get({}, 'betrokkene-bedrijf-' . $bedrijf->id);

        ok($bedrijf_obj->has_valid_address, "Fresh betrokkene (bedrijf) has valid address");

        $bedrijf_obj->straatnaam(undef);

        ok( !$bedrijf_obj->has_valid_address, "No street = no valid address");

        $bedrijf_obj->vestiging_adres_buitenland1('foobar');

        ok($bedrijf_obj->has_valid_address, "vestiging_adres_buitenland1 value implies valid address");
    }
}

sub create_bedrijf_duplicate_check : Tests {
    my $zbob = Zaaksysteem::Betrokkene::Object::Bedrijf->new();

    my $mock_row = Test::MockObject->new();
    $mock_row->mock(id => sub { return 42 });

    my $mock_row2 = Test::MockObject->new();
    $mock_row2->mock(id => sub { return 666 });

    my $mock_rs = Test::MockObject->new();
    $mock_rs->mock(search => sub {
        return $mock_rs;
    });
    $mock_rs->mock(first => sub {
        return $mock_row;
    });
    $mock_rs->mock(create => sub {
        return $mock_row2;
    });

    my $mock_schema = Test::MockObject->new();
    $mock_schema->mock(resultset => sub {
        return $mock_rs;
    });

    my $created = $zbob->_create_extern(
        {
            dbic => $mock_schema,
        },
        undef,
        {
            bedrijf => {
                vestiging_landcode => 6030,
                vestigingsnummer => 123,
                dossiernummer    => 123,
            },
        },
    );
    is($created, 42, "Creating a company that exists leads to the existing one being returned");

    $created = $zbob->_create_extern(
        {
            dbic => $mock_schema,
        },
        undef,
        {
            bedrijf => {
                vestiging_landcode => 1234,
                vestigingsnummer => 123,
                dossiernummer    => 123,
            },
        },
    );
    is($created, 666, "Duplicate check is skipped when company isn't Dutch");

    undef $mock_row;
    $created = $zbob->_create_extern(
        {
            dbic => $mock_schema,
        },
        undef,
        {
            bedrijf => {
                vestiging_landcode => 6030,
                vestigingsnummer => 123,
                dossiernummer    => 123,
            },
        },
    );
    is($created, 666, "Creating a company that doesn't exist yet leads to one being created");
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
