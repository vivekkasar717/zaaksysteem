package TestFor::General::Xential;
use base qw(ZSTest);

use TestSetup;

use File::Spec::Functions;
use IO::All;
use Data::UUID;

use constant RESPONSE_XML_PATH => catfile(qw(/ vagrant t data xml xential));

sub setup : Test(startup) {
    my $self = shift;

    $self->{xential_tests} = {
        # Info service
        getTemplates => {
            call     => 'get_templates',
            endpoint => 'https://10.44.0.11/Foo',
            transport_hook =>
                _mock_response('InfoService', 'getTemplatesResponse.xml'),
            expect => {
                'getTemplates' => [
                    {
                        'createdTimeStamp'    => '1439304044178',
                        'lastEditedTimeStamp' => '1439304044189',
                        'objectTypeId'        => 'templategroup',
                        'properties'          => {
                            'entry' => [
                                {
                                    'key'   => 'name',
                                    'value' => { 'item' => ['Templates'] }
                                },
                                {
                                    'key'   => 'description',
                                    'value' => { 'item' => ['null'] }
                                }
                            ]
                        },
                        'uuid'    => '6c47397f-6716-4942-b63f-254192422685',
                        'version' => -1
                    }
                ]
            },
        },
        getVersion => {
            call     => 'get_version',
            endpoint => 'https://10.44.0.11/Foo',
            transport_hook =>
                _mock_response('InfoService', 'getVersionResponse.xml'),
            expect => { return => 'Zaaksysteem Testsuite version' },
            normalized_expect => 'Zaaksysteem Testsuite version',
        },
        getVersionInfo => {
            call     => 'get_version_info',
            endpoint => 'https://10.44.0.11/Foo',
            transport_hook =>
                _mock_response('InfoService', 'getVersionInfoResponse.xml'),
            expect => {
                versionInfo => {
                    codeVersion => 42,
                    major       => 42,
                    minor       => 666,
                    revision    => 9,
                },
            }
        },
        getRegistrationInfo => {
            call     => 'get_registration_info',
            endpoint => 'https://10.44.0.11/Foo',
            transport_hook =>
                _mock_response('InfoService', 'getRegistrationInfoResponse.xml'),
            params => {
                foo => 'bar',
            },
            expect => {
            },
        },

        # Context service
        listBases => {
            call     => 'list_bases',
            endpoint => 'https://10.44.0.11/Foo',
            transport_hook =>
                _mock_response('ContextService', 'listBasesResponse.xml'),
            expect => {
                listBases => [
                    'zs_testsuite',
                ],
            },
        },
        createContext => {
            call     => 'create_context',
            endpoint => 'https://10.44.0.11/Foo',
            transport_hook =>
                _mock_response('ContextService', 'createContextResponse.xml'),
            params => {
                baseId   => 'foo',
                userName => 'bar',
                password => 'baz',
            },
            expect => {
                createContext => {
                    domain     => 'zaaksysteem.testsuite.xential.nl',
                    XSessionId => 'c43d4749-a392-4c36-8d37-6175ac250a69',
                }
            },
        },

        # Build service
        preparePreregisteredWithOptions => {
            call   => 'prepare_preregistered_with_options',
            params => {

                # Our XML
                xml => '<foo></foo>',

                # Template by UUID
                selectionOption => { selectionByUuid => $zs->generate_uuid, },

                # Storage option, NONE is not stored on disk (probably
                # testing), on other platforms, use Zaaksysteem
                storageOption => {
                    fixed      => 1,
                    target     => 'NONE',
                    properties => { entry => [] },
                },

                # This is where we want to get our answer
                statusUpdateOption => {
                    events => {
                        data    => 'foo',
                        eventId => 'AFTERBUILD',
                        headers => { entry => [], },
                        method  => 'GET',
                        url     => 'foo',
                    },
                },
            },
            endpoint       => 'https://10.44.0.11/Foo',
            transport_hook => _mock_response(
                'BuildService', 'preparePreregisteredWithOptionsResponse.xml'
            ),
            expect => {
                preparePreregisteredResult => {
                    startDocumentUrl => 'https://start.document.url.xential.nl',
                    ticketUuid       => '8fe02991-f524-4ab4-8578-7d2eecda91b2',
                    xentialRegistrationUuid =>
                        '8fe02991-f524-4ab4-8578-7d2eecda91b2',
                },
            }
        },
        startDocument => {
            call   => 'start_document',
            params => {
                ticketUuid => $zs->generate_uuid,
                documentTitle =>
                    Zaaksysteem::TestUtils::generate_random_string(),
            },
            endpoint => 'https://10.44.0.11/Foo',
            transport_hook =>
                _mock_response('BuildService', 'startDocumentResponse.xml'),
            expect => {
                startDocumentResult => {
                    documentId => 42,
                    resumeDocumentUrl =>
                        'https://resume.document.url.xential.nl',
                    status => 'VALID',
                },
            },
        },
        buildDocument => {
            call   => 'build_document',
            params => {
                documentId => $zs->generate_uuid,
                priority   => 'NORMAL',
            },
            endpoint => 'https://10.44.0.11/Foo',
            transport_hook =>
                _mock_response('BuildService', 'buildDocumentResponse.xml'),
            expect => { buildResult => { id => 42 }, },
        },
        getBuildStatus => {
            call   => 'get_build_status',
            params => {
                documentId => $zs->generate_uuid,
                priority   => 'NORMAL',
            },
            endpoint => 'https://10.44.0.11/Foo',
            transport_hook =>
                _mock_response('BuildService', 'getBuildStatusResponse.xml'),
            expect => { buildStatus => { status => 'DONE' }, },
        },
    };
}

sub xential_api_infoservice : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {

            my $xential = _create_xential();
            $self->test_xential_via_testsuite($xential, 'getVersion');
            $self->test_xential_via_testsuite($xential, 'getVersionInfo');

            $self->test_xential_via_testsuite($xential, 'getTemplates');

            #$self->test_xential_via_testsuite($xential, 'getRegistrationInfo');

        },
        "InfoService"
    );
}

sub xential_api_contextservice : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {

            my $xential = _create_xential();
            $self->test_xential_via_testsuite($xential, 'listBases');
            $self->test_xential_via_testsuite($xential, 'createContext');

        },
        "ContextService"
    );
}

sub xential_api_buildservice : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {

            my $xential = _create_xential();

            # Flow:
            # prepare_preregistered_with_options
            # start_document
            # build_document

            $self->test_xential_via_testsuite($xential, 'preparePreregisteredWithOptions');
            $self->test_xential_via_testsuite($xential, 'startDocument');
            $self->test_xential_via_testsuite($xential, 'buildDocument');

            $self->test_xential_via_testsuite($xential, 'getBuildStatus');

        },
        "BuildService"
    );

}

sub xential_full_build_process : Tests {
    my $self = shift;

    $zs->txn_ok(
        sub {

            my $xential = _create_xential();

            my $case = $zs->create_case_ok();

            my $prep_res = $self->{xential_tests}{preparePreregisteredWithOptions}{expect};
            my $start_res   = $self->{xential_tests}{startDocument}{expect};
            my $build_res   = $self->{xential_tests}{buildDocument}{expect};
            my $context_res = $self->{xential_tests}{createContext}{expect};

            no warnings qw(redefine once);
            local *Zaaksysteem::Xential::create_context = sub {
                my $self = shift;
                return $context_res;
            };

            local *Zaaksysteem::Xential::prepare_preregistered_with_options = sub {
                my $self = shift;
                return $prep_res;
            };

            local *Zaaksysteem::Xential::start_document = sub {
                my $self = shift;
                return $start_res;
            };

            local *Zaaksysteem::Xential::build_document = sub {
                my $self = shift;
                return $build_res;
            };
            use warnings;

            my $uuid  = $zs->generate_uuid;
            my $title = Zaaksysteem::TestUtils::generate_random_string();

            my $t = $zs->create_transaction_ok();
            $t->discard_changes;

            my $want = {
                case          => $case->id,
                template_uuid => $uuid,
                document      => {
                    id => $start_res->{startDocumentResult}{documentId},
                    title => $title,
                    ticket_uuid => $prep_res->{preparePreregisteredResult}{ticketUuid},
                    xential_uuid => $prep_res->{preparePreregisteredResult}{xentialRegistrationUuid},
                    start_url => $prep_res->{preparePreregisteredResult}{startDocumentUrl},
                    resume_url => $start_res->{startDocumentResult}{resumeDocumentUrl},
                },
                status   => 'done',
                build_id => $build_res->{buildResult}{id},
                context  => $context_res->{createContext},
                transaction_uuid => $t->uuid,
            };


            _compare_build(
                $xential->create_document_from_template(
                    case           => $case,
                    template_uuid  => $uuid,
                    document_title => $title,
                    transaction    => $t,
                ),
                $want,
                "Silent build succeeded"
            );

            ### Complex build

            no warnings qw(redefine once);
            local *Zaaksysteem::Xential::start_document = sub {
                my $self = shift;
                my %start_res = %{$start_res};
                $start_res{startDocumentResult}{status} = 'VALID_UNANSWERED';
                return \%start_res;
            };
            use warnings;

            my $doc = $want->{document};
            $doc = {
                id    => undef,
                title => $title,
                ticket_uuid => $prep_res->{preparePreregisteredResult}{ticketUuid},
                xential_uuid => $prep_res->{preparePreregisteredResult}{xentialRegistrationUuid},
                start_url => $prep_res->{preparePreregisteredResult}{startDocumentUrl},
                resume_url => $start_res->{startDocumentResult}{resumeDocumentUrl},
            };

            $want->{document} = $doc;
            $want->{build_id} = undef;
            $want->{status}   = 'complex_build';

            _compare_build(
                $xential->create_document_from_template(
                    case           => $case,
                    template_uuid  => $uuid,
                    document_title => $title,
                    transaction    => $t,
                ),
                $want,
                "Complex build succeeded"
            );

            ### Invalid build: means resumable
            no warnings qw(redefine once);
            local *Zaaksysteem::Xential::start_document = sub {
                my $self = shift;
                my %start_res = %{$start_res};
                $start_res{startDocumentResult}{status} = 'INVALID';
                return \%start_res;
            };
            use warnings;

            _compare_build(
                $xential->create_document_from_template(
                    case           => $case,
                    template_uuid  => $uuid,
                    document_title => $title,
                    transaction    => $t,
                ),
                $want,
                "Build is invalid"
            );

        },
        'Full cycle build tests',
    );
}

sub _compare_build {
    my ($got, $want, $msg) = @_;
    if (!is_deeply($got, $want, $msg)) {
        local $Data::Dumper::Sortkeys = 1 ;
        diag explain { got => $got, want => $want };
    }
}

sub xential_live_tests : Tests {
    my $self = shift;
    if (!$ENV{ZS_DEVELOPER_XENTIAL}) {
        $self->builder->skip(
            "ZS_DEVELOPER_XENTIAL is not set, skipping tests",
        );
        return;
    }

    # Geen userinput: LoremIpsum,89d11f78-d781-4630-b1f0-885747387537
    # Met Invoer: Routebeschrijving, 2c5319cc-4f3b-42ed-9a3b-d9fdd0e7c6fe

    $zs->txn_ok(
        sub {

            my $xential = _create_xential(
                base_host               => $ENV{ZS_DEVELOPER_XENTIAL},
                contextservice_base_id  => 'base_zs_nl',
                contextservice_username => 'zsnl',
                contextservice_password => 'zsnl',
            );

            my $case = $zs->create_case_ok();

            my @tests = (
                {
                    testname => 'Silent build via Zaaksysteem testsuite',
                    uuid     => '89d11f78-d781-4630-b1f0-885747387537',
                    status   => 'done',
                    xml      => '<foo>bar</foo>'
                },
                {
                    testname => 'Complex build via Zaaksysteem testsuite',
                    uuid     => '2c5319cc-4f3b-42ed-9a3b-d9fdd0e7c6fe',
                    status   => 'complex_build',
                    xml      => '<foo>bar</foo>'
                },
            );

            my $res;
            foreach (@tests) {
                $res = $xential->create_document_from_template(
                    case           => $case,
                    document_title => $_->{testname},
                    template_uuid  => $_->{uuid},
                    $_->{xml} ? (xml => $_->{xml}) : (),
                );
                if (!is($res->{status}, $_->{status}, $_->{testname})) {
                    local $Data::Dumper::Sortkeys = 1 ;
                    diag explain $res;
                }
            }
        },
        "Live tests",
    );

}

sub test_xential_via_testsuite {
    my $self = shift;
    my ($xential, $orig) = @_;

    my $test = $self->{xential_tests}{$orig};

    if (!$test) {
        return fail("Call $orig is not implemented in the testsuite");
    }
    my $call = $test->{call};

    _test_response(
        call   => $call,
        params => $test->{params} // {},

        endpoint       => $test->{endpoint},
        transport_hook => $test->{transport_hook},
        expect         => $test->{expect},
    );

    my $res;
    if (exists $test->{params}) {
        $res = $xential->$call($test->{params},
            { transport_hook => $test->{transport_hook} });
    }
    else {
        $res = $xential->$call({ transport_hook => $test->{transport_hook} });
    }

    if (exists $test->{normalized_expect}) {
        is($res, $test->{normalized_expect}, "Normalized data is correct");
    }
    else {
        if (!is_deeply($res, $test->{expect}, "Actual call to ZS::Xential->$call works")) {
            local $Data::Dumper::Sortkeys = 1;
            diag explain { got => $res, want => $test->{expect} };
        }
    }
}

sub _mock_response {
    my ($service, $name) = @_;

    return sub {
        return HTTP::Response->new(
            200,
            'Mocking the answers',
            ['Content-Type' => 'text/xml'],
            $zs->slurp(RESPONSE_XML_PATH, lc($service), $name),
        );
        }
}

sub _test_response {
    my %options = @_;

    my $call   = delete $options{call};
    my $params = delete $options{params};
    my $expect = { parameters => delete $options{expect} };
    return _answer(
        $zs->xential->call($call, $params, \%options,),
        call   => $call,
        expect => $expect,
    );
}

sub _answer {
    my ($answer, $trace, %opts) = @_;

    if ($opts{expect}) {
        my $ok = is_deeply($answer, $opts{expect},
            "$opts{call} returns correct response");
        if (!$ok || $trace->error) {
            if ($trace->error) {
                diag $trace->error;
            }
            local $Data::Dumper::Sortkeys = 1;
            diag explain { got => $answer, want => $opts{expect} };
        }
        return $ok;
    }

    if ($trace->error) {
        diag explain [$answer, $trace];
    }
    else {
        diag explain [$answer];
    }

    return 1;
}

sub _create_xential {
    my %opts = @_;

    $opts{base_host} //= 'https://10.44.0.11/foo/bar';

    my $interface = $zs->create_xential_interface_ok(%opts);
    my $xential   = Zaaksysteem::Xential->new_from_interface($interface);

    isa_ok($xential,            'Zaaksysteem::Xential');
    isa_ok($xential->interface, 'Zaaksysteem::Model::DB::Interface');
    return $xential;
}

1;

__END__

=head1 NAME

TestFor::General::Xential - A Zaaksysteem Xential tester

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
