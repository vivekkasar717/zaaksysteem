#!perl

use warnings;
use strict;

use lib 't/inc';
use TestSetup;

use Zaaksysteem::Object::Model;

initialize_test_globals_ok;

my $model = Zaaksysteem::Object::Model->new(schema => $zs->schema);

$zs->zs_transaction_ok(sub {
    my $object_data = $zs->create_object_data_ok;
    my $object = $model->inflate_from_row($object_data);

    lives_ok { $object->actions } 'object->actions call lives';

    is_deeply(
        $object->actions,
        [qw(read write manage)],
        'object->actions has read, write and manage default'
    );
}, 'plain object allows action retrieval');

$zs->zs_transaction_ok(sub {
    my $case = $zs->create_case_ok;
    my $objects = $model->rs;

    my $object_data = $objects->find({
        object_class => 'case',
        object_id => $case->id
    });

    my $object = $model->inflate_from_row($object_data);

    lives_ok
        { $object->actions }
        'object->actions is callable for case objects';

    is ref $object->actions, 'ARRAY',
        'object->actions for case returns arrayref';
}, '');

zs_done_testing;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

