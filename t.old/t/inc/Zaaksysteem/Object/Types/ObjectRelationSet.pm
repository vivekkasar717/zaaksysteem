package Zaaksysteem::Object::Types::ObjectRelationSet;

use Moose;

extends 'Zaaksysteem::Object';

has object_set => (
    is => 'rw',
    traits => [qw[OR]],
    isa_set => 1,
    label => 'Set of relations',
    type => 'object_relation_set', # refers to __PACKAGE__
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
