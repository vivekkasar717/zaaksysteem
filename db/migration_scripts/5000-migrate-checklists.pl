#!/usr/bin/perl

use Moose;
use Data::Dumper;

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../../lib";

use Catalyst qw/ConfigLoader/;

use Catalyst::Log;
use Catalyst::Model::DBIC::Schema;

my $log = Catalyst::Log->new;

error("USAGE: $0 [dsn] [user] [password] [commit]") unless @ARGV && scalar(@ARGV) >= 3;

my ($dsn, $user, $password, $commit) = @ARGV;
my $dbic = database($dsn, $user, $password);
my %answers;

$dbic->txn_do(sub {
        fill_checklists($dbic);
        fill_case_lists($dbic);
});

$log->info('All done.');
$log->_flush;

sub database {
    my ($dsn, $user, $password) = @_;

    Catalyst::Model::DBIC::Schema->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => {
            dsn => $dsn,
            pg_enable_utf8 => 1,
            user => $user,
            password => $password
        }
    );

    Catalyst::Model::DBIC::Schema->new->schema;
}

sub error {
    $log->error(shift);
    $log->_flush;

    exit;
}

sub fill_checklists {
    my $dbic = shift;

    my $questions = $dbic->resultset('ChecklistVraag');
    my $checklist_items = $dbic->resultset('ChecklistItem');
    my $casetype_checklist_items = $dbic->resultset('ZaaktypeStatusChecklistItem');

    while(my $question = $questions->next) {
        my $answer = $question->checklist_antwoords->search({}, { order_by => { -desc => 'id' } })->first;

        $log->info(sprintf(
            'Handling checklist `%s` for casetype `%s` on `%s`',
            $question->vraag,
            $question->zaaktype_node_id->titel,
            $question->zaaktype_status_id->naam
        ));

        my $case_item = $casetype_checklist_items->new_result({
            label => $question->vraag,
            casetype_status_id => $question->zaaktype_status_id->id
        });

        $case_item->insert;

        if($answer) {
            $answers{ $case_item->id } = $answer;
        }

        $log->_flush;
    }
}

sub fill_case_lists {
    my $dbic = shift;

    my $cases = $dbic->resultset('Zaak');

    while(my $case = $cases->next) {
        my $stati = $case->zaaktype_node_id->zaaktype_statuses;

        $log->info('Generating checklists for case ' . $case->id);

        while(my $status = $stati->next) {
            next if $status->status == 1;

            $log->info('  Checklist for status ' . $status->naam);

            my $checklist = $dbic->resultset('Checklist')->new_result({
                case_id => $case->id,
                case_milestone => $status->status
            });

            $checklist->insert;

            my $questions = $dbic->resultset('ZaaktypeStatusChecklistItem')->search(
                { casetype_status_id => $status->id },
                { order_by => 'id' }
            );

            my $sequence = 1;

            while(my $question = $questions->next) {
                $log->info('    Injecting question ' . $question->label);

                my $item = $dbic->resultset('ChecklistItem')->new_result({
                    checklist_id => $checklist->id,
                    label => $question->label,
                    state => exists $answers{ $question->id } ? 'true' : 'false',
                    deprecated_answer => exists $answers{ $question->id } ? $answers{ $question->id }->antwoord : undef,
                    sequence => $sequence++,
                    user_defined => 0
                });

                $item->insert;
            }
        }
    }
}

sub find_or_create_checklist {
    my ($dbic, $case_id, $milestone) = @_;

    my $checklists = $dbic->resultset('Checklist');

    my $checklist = $checklists->search({ case_id => $case_id, case_milestone => $milestone})->first;

    unless($checklist) {
        $checklist = $checklists->new_result({
            case_id => $case_id,
            case_milestone => $milestone
        });

        $checklist->insert;
    }

    return $checklist;
}
