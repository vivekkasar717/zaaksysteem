BEGIN;

    -- NOTICE : DO NOT RUN THIS DB UPGRADE *BEFORE* ENSURING COMMIT
    -- 2b96becce46f6e096dbf27381a80fb01cad4d238 (MINTY-1940) IS RUNNING,
    -- OR FACE THE CONSEQUENCES OF BULK CASE UPDATES AGAIN

	UPDATE object_data SET
		index_hstore = index_hstore || ('"next_run" => "' || TO_CHAR(NOW(), 'YYYY-MM-DD"T"HH24:MI:SS') || '"')::hstore,
		properties = jsonb_set(
			properties::jsonb,
			'{values,next_run,value,__DateTime__}'::text[],
			('"' || TO_CHAR(NOW(), 'YYYY-MM-DD"T"HH24:MI:SS') || '"')::jsonb
		)
	WHERE object_class = 'scheduled_job'
	  AND properties::jsonb->'values'->'job'->>'value' = 'CaseUrgencyUpdater';

COMMIT;
