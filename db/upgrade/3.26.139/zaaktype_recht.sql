
BEGIN;
    ALTER TABLE zaaktype_authorisation ALTER COLUMN recht SET NOT NULL;
    ALTER TABLE zaaktype_authorisation ADD CONSTRAINT zaaktype_authorisation_recht
        CHECK (recht = ANY (ARRAY[
            'zaak_beheer'::text,
            'zaak_edit'::text,
            'zaak_read'::text,
            'zaak_search'::text
           ]))
    ;

COMMIT;
