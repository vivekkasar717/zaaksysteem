BEGIN;

INSERT INTO interface
        ( "name"
        , "active"
        , "max_retries"
        , "interface_config"
        , "multiple"
        , "module"
        )
    VALUES 
        ( 'Authenticatie voor externe applicaties'
        , TRUE
        , 10
        , '{"session_invitation_version":"v1"}'
        , FALSE
        , 'authtoken'
        );

COMMIT;
