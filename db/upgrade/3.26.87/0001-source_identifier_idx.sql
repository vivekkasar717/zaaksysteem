BEGIN;

    CREATE UNIQUE INDEX ON user_entity(source_interface_id, source_identifier);

COMMIT;
