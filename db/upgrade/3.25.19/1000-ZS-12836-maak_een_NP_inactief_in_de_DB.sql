BEGIN;

    ALTER TABLE natuurlijk_persoon ADD COLUMN active BOOLEAN DEFAULT true NOT NULL;
    UPDATE natuurlijk_persoon SET active = false WHERE deleted_on IS NOT NULL;

COMMIT;
