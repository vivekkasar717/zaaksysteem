BEGIN;

UPDATE object_data SET index_hstore = index_hstore || hstore('case.casetype', class_uuid::text) WHERE object_class = 'case';

COMMIT;
