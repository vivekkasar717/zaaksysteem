BEGIN;
    /* db/upgrade/3.25.0/1000-uuids_in_subject_tables.sql */

ALTER TABLE bedrijf ADD COLUMN uuid UUID UNIQUE DEFAULT uuid_generate_v4();
ALTER TABLE natuurlijk_persoon ADD COLUMN uuid UUID UNIQUE DEFAULT uuid_generate_v4();

    /* db/upgrade/3.25.0/1000-zs_queue.sql */

    CREATE TABLE queue (
        id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
        object_id UUID REFERENCES object_data(uuid),
        status TEXT NOT NULL DEFAULT 'pending' CHECK ( status IN ('pending', 'running', 'finished', 'failed') ),
        type TEXT NOT NULL,
        label TEXT NOT NULL,
        data TEXT NOT NULL DEFAULT '{}',
        date_created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
        date_started TIMESTAMP WITHOUT TIME ZONE,
        date_finished TIMESTAMP WITHOUT TIME ZONE
    );

    /* db/upgrade/3.25.0/1001-scheduled_jobs_uuid.sql */

    ALTER TABLE scheduled_jobs ADD COLUMN uuid UUID UNIQUE DEFAULT uuid_generate_v4();
    ALTER TABLE scheduled_jobs ADD FOREIGN KEY (case_id) REFERENCES zaak(id);

    /* db/upgrade/3.25.0/1002-disable_dashboard_modification.sql */

    INSERT INTO config(parameter, value, advanced) VALUES('disable_dashboard_customization', '0', TRUE);

    /* db/upgrade/3.25.0/1002-multi_thumbnails.sql */

CREATE TABLE file_thumbnail (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    file_id INTEGER NOT NULL REFERENCES file(id),
    filestore_id INTEGER NOT NULL REFERENCES filestore(id),
    max_width INTEGER NOT NULL,
    max_height INTEGER NOT NULL,
    date_generated TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);

    /* db/upgrade/3.25.0/1002-zs_queue_waiting.sql */

ALTER TABLE queue DROP CONSTRAINT "queue_status_check";
ALTER TABLE queue ADD CHECK (status IN ('pending', 'running', 'finished', 'failed', 'waiting'));

    /* db/upgrade/3.25.0/1003-contactmoment-add_uuid.sql */

ALTER TABLE contactmoment ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
CREATE INDEX contactmoment_uuid_idx ON contactmoment (uuid);

    /* db/upgrade/3.25.0/1003-zs_queue_misc.sql */

ALTER TABLE queue ALTER COLUMN date_created SET DEFAULT statement_timestamp();
ALTER TABLE queue ADD COLUMN parent_id UUID REFERENCES queue(id);

    /* db/upgrade/3.25.0/1004-remove_na.sql */

    DELETE FROM zaaktype_betrokkenen WHERE betrokkene_type IN (
        'niet_natuurlijk_persoon_na'
    );

    /* db/upgrade/3.25.0/2000-object_data_queue_relation.sql */

    ALTER TABLE queue DROP CONSTRAINT IF EXISTS queue_object_id_fkey;
    ALTER TABLE queue ADD CONSTRAINT queue_object_id_fkey
        FOREIGN KEY (object_id) REFERENCES object_data(uuid)
        ON DELETE CASCADE DEFERRABLE;

    /* db/upgrade/3.25.0/2001-object_relation_tablefix.sql */

ALTER TABLE object_relation ADD COLUMN object_id UUID REFERENCES object_data(uuid);

    /* db/upgrade/3.25.0/3000-object_relation_cascades_on_delete.sql */

ALTER TABLE object_relation DROP CONSTRAINT object_relation_object_id_fkey;
ALTER TABLE object_relation DROP CONSTRAINT object_relation_object_uuid_fkey;

ALTER TABLE object_relation ADD CONSTRAINT object_relation_object_id_fkey FOREIGN KEY (object_id) REFERENCES object_data(uuid) ON DELETE CASCADE DEFERRABLE;
ALTER TABLE object_relation ADD CONSTRAINT object_relation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid);

COMMIT;
