BEGIN;

    -- Original data b0rking happened because Zaaksysteem stored Europe/Amsterdam
    -- timezones in a timezone-less field. Fix this by interpreting the data
    -- in reverse. Assume the TZ is UTC, travel to EU/AMS TZ, strip the timezone
    -- and save the resulting datestamp.
    UPDATE natuurlijk_persoon
    SET geboortedatum = geboortedatum AT TIME ZONE 'UTC' AT TIME ZONE 'Europe/Amsterdam'
    -- This is our main assumption, if any time-part of the timestamp is non-zero
    -- categorize the dob as EU/AMS timestamp in TZ-less field.
    WHERE EXTRACT(HOUR FROM geboortedatum) + EXTRACT(MINUTE FROM geboortedatum) + EXTRACT(SECOND FROM geboortedatum) > 0;

    UPDATE gm_natuurlijk_persoon
    SET geboortedatum = geboortedatum AT TIME ZONE 'UTC' AT TIME ZONE 'Europe/Amsterdam'
    WHERE EXTRACT(HOUR FROM geboortedatum) + EXTRACT(MINUTE FROM geboortedatum) + EXTRACT(SECOND FROM geboortedatum) > 0;

    -- Would prefer to change the type, but existing queries will (presumably)
    -- break due to lack of explicit casting in certain contexts.
    -- Other reason not to change the type; if we b0rk up time_zone
    -- interpretation in the future, we have no way of knowing, whereas with
    -- a full timestamp type we can at least fix broken data.
    -- ALTER TABLE natuurlijk_persoon ALTER COLUMN geboortedatum TYPE DATE;

COMMIT;
