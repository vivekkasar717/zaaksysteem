BEGIN;

    DELETE FROM config WHERE parameter IN ('filestore_location', 'tmp_location');

COMMIT;
