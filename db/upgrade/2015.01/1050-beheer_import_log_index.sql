BEGIN;
    CREATE INDEX beheer_import_importtype_idx ON beheer_import(importtype);
    CREATE INDEX beheer_import_created_idx ON beheer_import(created);
COMMIT;
