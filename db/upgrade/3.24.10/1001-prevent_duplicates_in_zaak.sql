BEGIN;

ALTER TABLE zaak ADD COLUMN duplicate_prevention_token uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4();

COMMIT;
